<?php
/**
 * Customer processing order email
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates/Emails
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly ?>

<?php include( MBWPE_TPL_PATH.'/settings.php' ); ?>

<?php do_action('woocommerce_email_header', $email_heading, $email ); ?>

<h2><?php _e( "Insurance Cover Policy", 'woocommerce' ); ?></h2>
<style>
</style>
<?php //do_action( 'woocommerce_email_before_order_table', $order, $sent_to_admin, $plain_text, $email ); ?>

		<?php  $ins_pdf_imageset1 = get_option( 'ins_pdf_image');
		echo $ins_pdf_imageset1;
		?>

<?php do_action( 'woocommerce_email_footer', $email ); ?>

<?php include( MBWPE_TPL_PATH.'/treatments.php' ); ?>