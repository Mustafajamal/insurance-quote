<?php
/**
 * The template for displaying the footer for coming soon page.
 *
 * Contains the closing of the id=main div and all content after
 *
 * @package Barrel
 */
?>
<?php 
$barrel_theme_options = barrel_get_theme_options();

?>
<?php wp_footer(); ?>
</body>
</html>