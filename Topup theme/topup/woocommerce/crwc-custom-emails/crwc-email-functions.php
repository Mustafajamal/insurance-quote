<?php

add_filter( 'woocommerce_email_classes', 'crwc_custom_woocommerce_emails' );

function crwc_custom_woocommerce_emails( $email_classes ) {

	//* Custom welcome email to customer when purchasing online training program

	$upload_dir = wp_upload_dir();
	
	include_once( get_template_directory() . '/woocommerce/crwc-custom-emails/crwc-email-functions.php');
	$email_classes['CRWC_Welcome_Email'] = new CRWC_Welcome_Email(); // add to the list of email classes that WooCommerce loads


	return $email_classes;
	
}