<?php
function showForm2($atts) {

$args = array(
'post_type' => 'stern_taxi_car_type',
'nopaging' => true,
'order'     => 'ASC',
'orderby' => 'meta_value',
'meta_key' => '_stern_taxi_car_type_organizedBy'
);

$allTypeCars = get_posts( $args );
$args = array(
'post_type' => 'stern_listAddress',
'nopaging' => true,
'meta_query' => array(

array(
'key'     => 'typeListAddress',
'value'   => 'destination',
'compare' => '=',
),

array(
'key'     => 'isActive',
'value'   => 'true',
'compare' => '=',
),
),
);

$allListAddressesDestination = get_posts( $args );
$backgroundColor= get_option('stern_taxi_fare_bcolor');
if($backgroundColor!="")
{
$backgroundColor='background-color:'.stern_taxi_fare_hex2rgba($backgroundColor,get_option('stern_taxi_fare_bTransparency'));
}
global $woocommerce;
$currency_symbol = get_woocommerce_currency_symbol();
if (isBootstrapSelectEnabale()=="true")
{
$class = "selectpicker show-tick";
} else {
$class = "form-control";
}

$class_full_row12 = (get_option('stern_taxi_fare_full_row') != "true") ? "class='col-lg-12 col-md-12 col-sm-12 col-xs-12'" : "class='col-lg-12 col-md-12 col-sm-12 col-xs-12'";

$class_full_row6 = (get_option('stern_taxi_fare_full_row') != "true") ? "class='col-lg-6 col-md-6 col-sm-12 col-xs-12'" : "class='col-lg-12 col-md-12 col-sm-12 col-xs-12'";

$class_full_row3 = (get_option('stern_taxi_fare_full_row') != "true") ? "class='col-lg-3 col-md-3 col-sm-12 col-xs-12'" : "class='col-lg-12 col-md-12 col-sm-12 col-xs-12'";

$classMain = (get_option('stern_taxi_fare_form_full_row') != "true") ? "class='col-xs-12 col-sm-6 col-lg-6'" : "class='col-xs-12 col-sm-12 col-lg-12'";
?>

<div class="container1 row">
<div class="col col-lg-9 stern-taxi-fare">

<?php

$form_setVehicleInfo = get_option( 'vehicle_dataTpl' );
$coverage_dataTpl = get_option( 'frontend_dataTpl' );

foreach ( $coverage_dataTpl as $data_dataTpl ) {
$veh_cat_2[] = $data_dataTpl[4];
$tpl_cylenders[] = $data_dataTpl[6];
}

foreach ( $form_setVehicleInfo as $data_vbrand ) {
$name_arr[] = $data_vbrand[0];
$medel_year[] = $data_vbrand[1];
$modelno_submodel[] = $data_vbrand[2];
$vehicle_cat[] = $data_vbrand[3];
}

$vehicle_brand = array_unique($name_arr);
$veh_medel_year = array_unique($medel_year);
$veh_modelno = array_unique($modelno_submodel);
$veh_vehicle_cat = array_unique($vehicle_cat);
//$veh_sub_model = array_unique($sub_model);
$vehicle_cat_2 = array_unique($veh_cat_2);
//$repair_condition = array_unique($repair_con);
$tpl_cylenders_arr = array_unique($tpl_cylenders);
?>

<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700" rel="stylesheet">
<link href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css" rel="stylesheet">

<div style="@display:none;" id="bread_crums_custom">
<div class="steps clearfix">
<ul id="tablist11">
<li class="first"> <a id="goto_form" ><span class="number">Vehicle Details</span></a> </li>
<li class="second"> <a id="goto_table"><span class="number">Insurance Quote</span></a> </li>
<!--<li class="second"> <a id="goto_checkout"><span class="number">2 Goto Checkout</span></a> </li> -->
</ul>
</div>
</div>


<div id="form_main_div">

<div class="smart-wrap">
<div id="XXstep1" class="smart-forms smart-container wrap-1">
<div class="form-body smart-steps steps-progress stp-three">

<form  id="smart-form" method="post">
<h2>Vehicle Information</h2>
<fieldset>

<!-- STEP 1 HTML -->
<div class="frm-row">
<div class="section colm colm6"><p class="step1_labels">Your Vehicle Brand Name is</p></div>
<div class="section colm colm6">
<label class="field select">
<select class="calculate1 percentage" id="vehicle_brand" name="vehicle_brand"  required>
<option value="" >--- Select Auto Brand Name ---</option>
<?php
foreach ( $vehicle_brand as $data_vbrand ) { ?>
<option class="options" value="<?php echo $data_vbrand;?>"><?php echo $data_vbrand;?></option>
<?php } ?>
</select><i class="fa fa-chevron-down arrow1"></i><b class="tooltip tip-right-top"><em>Please Select Your Vehicle Brand</em></b>
</label>
</div>
</div>

<div id="dor_row_m" class="frm-row form_row-space">
<div class="section colm colm6"><p class="step1_labels">Your Vehicle Manufacture Year is</p></div>
<div class="section colm colm6"><div class="form-group">
<label class="field select">
<select class="calculate1 percentage" id="model_year" name="model_year"  required>
<option value=""> --- Model Year --- </option>
<?php
foreach ( $veh_medel_year as $data_medel_year ) { ?>
<option value="<?php echo $data_medel_year;?>"><?php echo $data_medel_year;?></option>
<?php } ?>
</select><i class="fa fa-chevron-down arrow1"></i><b class="tooltip tip-right-top"><em>Please! Select Your Car Manufacture / Make Year.</em></b>
</label>
</div>
</div>
</div>

<div class="frm-row form_row-space">
<div class="section colm colm6"><p class="step1_labels">Your Vehicle Model is</p></div>
<div class="section colm colm6"><div class="form-group">
<label class="field select">
<select class="calculate percentage modeloption" id="vehicle_model_detail" name="vec_model_num"  required>
<option value=""> --- Model Details --- </option>
<?php
//$data_unique_mno = array_unique($data_setfd);
//foreach ( $veh_modelno as $data_modelNo ) { ?>
<!-- <option data-price="5.00" value="<?php// echo $data_modelNo;?>"><?php //echo $data_modelNo;?></option> -->
<?php // } ?>
</select><i class="fa fa-chevron-down arrow1"></i><b class="tooltip tip-right-top"><em>Select Your Vehicle Model</em></b>
</label>
</div>
</div>
</div>

<div class="frm-row" id="veh_PolicyType_id">
<div class="section colm colm6"><p class="step1_labels">Your Vehicle is in Personal or Commercial Use</p></div>
<div class="section colm colm6">
<label class="modern-switch morph-switch">
<input type="radio" name="ptype_check" id="fr3" value="Yes" checked>
<span class="switch-toggle"></span>
<span class="switch-label">Personal Use</span><b class="tooltip tip-right-top"><em>My Vehicle is in Personal Use</em></b>
</label>
<label class="modern-switch morph-switch">
<input type="radio" name="ptype_check" id="fr4" value="No">
<span class="switch-toggle"></span>
<span class="switch-label">Commercial Use</span><b class="tooltip tip-left"><em>My Vehicle is as Commercial Use</em></b>
</label>
</div><!-- end section -->
</div>

<div id="dor_row_m" class="frm-row form_row-space">
<div class="section colm colm6">
<p class="step1_labels">Your Vehicle Cylenders or Capacity is</p>
</div>

<div class="section colm colm6"><div class="form-group">
<label class="field select">
<select class="calculate1 percentage" id="veh_cylenders" name="veh_cylenders"  required>
<option value=""> --- Select Cylenders --- </option>
<?php
foreach ( $tpl_cylenders_arr as $tpl_cylenders_show ) { ?>
<option value="<?php echo $tpl_cylenders_show;?>"><?php echo $tpl_cylenders_show;?></option>
<?php } ?>
</select><i class="fa fa-chevron-down arrow1"></i><b class="tooltip tip-right-top"><em>Please! Select Your Vehicle Cylenders or Capacity</em></b>
</label>
</div>
</div>
</div>

<!--
<div id="dor_row" class="frm-row form_row-space">
<div class="section colm colm6">
<p class="step2_labels">Vehicle First Registration Date is</p>
</div>

<div class="section colm colm6"><div class="form-group">
<span id="phone1">
<label class="field select">
<select class="calculate percentage ignore" id="vehicle_reg_date1" name="vehicle_reg_date"  required>
<option value="" >Date</option>
<option value="1">1</option>
<option value="2">2</option>
<option value="3">3</option>
<option value="4">4</option>
<option value="5">5</option>
<option value="6">6</option>
<option value="7">7</option>
<option value="8">8</option>
<option value="9">9</option>
<option value="10">10</option>
<option value="11">11</option>
<option value="12">12</option>
<option value="13">13</option>
<option value="14">14</option>
<option value="15">15</option>
<option value="16">16</option>
<option value="17">17</option>
<option value="18">18</option>
<option value="19">19</option>
<option value="20">20</option>
<option value="21">21</option>
<option value="22">22</option>
<option value="23">23</option>
<option value="24">24</option>
<option value="25">25</option>
<option value="26">26</option>
<option value="27">27</option>
<option value="28">28</option>
<option value="29">29</option>
<option value="30">30</option>
<option value="31">31</option>
</select><i class="fa fa-chevron-down arrow1"></i><b class="tooltip tip-right-top"><em> Hey buddy! iam a top left tooltip.</em></b>
</label>
</span>

<span id="month">
<label class="field select">
<select class="calculate percentage ignore" id="vehicle_reg_date1=2" name="dor_month"  required>
<option value="" >Month</option>
<option value="1">January</option>
<option value="2">February</option>
<option value="3">March</option>
<option value="4">April</option>
<option value="5">May</option>
<option value="6">June</option>
<option value="7">July</option>
<option value="8">August</option>
<option value="9">September</option>
<option value="10" >October</option>
<option value="11">November</option>
<option value="12">December</option>
</select><i class="fa fa-chevron-down arrow1"></i><b class="tooltip tip-right-top"><em> Hey buddy! iam a top left tooltip.</em></b>

</label>

</span>

<span id="phone1">
<label class="field select">
<select class="calculate percentage" id="vehicle_reg_date3" name="vehicle_reg_date3"  required>
<option value="" >Year</option>
<option value="2018">2018</option>
<option value="2017" >2017</option>
<option value="2016">2016</option>
<option value="2015">2015</option>
<option value="2014">2014</option>
<option value="2013">2013</option>
<option value="2012">2012</option>
<option value="2011">2011</option>
<option value="2010">2010</option>
<option value="2009">2009</option>
<option value="2008">2008</option>
</select><i class="fa fa-chevron-down arrow1"></i><b class="tooltip tip-right-top"><em> Hey buddy! iam a top left tooltip.</em></b>
</label>
</span>
</div>
</div>
</div>
-->

<div class="frm-row" id="pa_cover_driver_id">
<div class="section colm colm6">
<p class="step1_labels">I Need Personal Accident Cover for Driver</p>
</div>

<div class="section colm colm3 percentageX">
<label class="switch switch-round block option">
<input type="radio" class="need_per_acc_cover_driver" name="per_accidentcover_chk" id="fr23" value="Yes" >
<span class="switch-label" data-on="YES" data-off="YES"></span>
<span>  </span>
<b class="tooltip"><em>Yes! I Need Personal Accident Cover for Driver</em></b>
</label>
</div>

<div class="section colm colm3">
<label class="switch switch-round block option">
<input type="radio" class="need_per_acc_cover_driver" name="per_accidentcover_chk" id="fr24" value="No" checked>
<span class="switch-label" data-on="NO" data-off="NO"></span>
<span>  </span>
<b class="tooltip"><em>NO! i Don't Need Accident Cover for Driver</em></b>
</label>
</div>
</div>

<div class="frm-row form_row-space" id="pa_cover_passangers_id">
<div class="section colm colm6"><p class="step2_labels">I Need Personal Accident Cover for Passengers</p></div>
<div class="section colm colm6">
<label class="field select">
<select class="calculate percentage" id="pa_cover_pass" name="pa_cover_pass"  required>
<option class="option" value="0">--- Please Choose Passengers ---</option>
<option class="option" value="1">For 1 Person</option>
<option class="option" value="2">For 2 Persons</option>
<option class="option" value="3">For 3 Persons</option>
<option class="option" value="4">For 4 Persons</option>
<option class="option" value="5">For 5 Persons</option>
<option class="option" value="6">For 6 Persons</option>
<option class="option" value="7">For 7 Persons</option>
</select><i class="fa fa-chevron-down arrow1"></i><b class="tooltip tip-right-top"><em>Please! Select Number of Passengers, For Accident Cover</em></b>
</label>
</div>
</div>


<div class="frm-row form_row-space">
<div class="section colm colm6">
<p class="step2_labels">Your Vehicle Registration City is</p>
</div>

<div class="section colm colm6">
<label class="field select">
<select class="calculate percentage" id="cityof_reg" name="cityof_reg"  required>
<option class="option" value="">--- Select City ---</option>
<option class="option" value="abu-dahbi">Abu Dahbi</option>
<option class="option" value="abudahbi">Dubai</option>
<option class="option" value="jada">Sharjah</option>
<option class="option" value="jada">Al Ain</option>
<option class="option" value="jada">Ajman</option>
<option class="option" value="jada">Ras Al Khaimah</option>
<option class="option" value="jada">Fujairah</option>
<option class="option" value="jada">Umm al-Quwain</option>
</select><i class="fa fa-chevron-down arrow1"></i><b class="tooltip tip-right-top"><em>Select! Vehicle Registration City</em></b>
</label>
</div>
</div>

<div class="frm-row form_row-space">
<div class="section colm colm6"><p class="step2_labels">When would you like to Start Policy?</p></div>
<div class="section colm colm6"><div class="form-group">
<span id="phone1">
<label class="field select">
<select class="calculate percentage" id="vec_policy_date1" name="vec_policy_date"  required>
<option value=""> Date </option>
<option value="1">1</option>
<option value="2">2</option>
<option value="3">3</option>
<option value="4">4</option>
<option value="5">5</option>
<option value="6">6</option>
<option value="7">7</option>
<option value="8">8</option>
<option value="9">9</option>
<option value="10">10</option>
<option value="11">11</option>
<option value="12">12</option>
<option value="13">13</option>
<option value="14">14</option>
<option value="15">15</option>
<option value="16">16</option>
<option value="17">17</option>
<option value="18">18</option>
<option value="19">19</option>
<option value="20">20</option>
<option value="21">21</option>
<option value="22">22</option>
<option value="23">23</option>
<option value="24">24</option>
<option value="25">25</option>
<option value="26">26</option>
<option value="27">27</option>
<option value="28">28</option>
<option value="29">29</option>
<option value="30">30</option>
<option value="31">31</option>
</select><i class="fa fa-chevron-down arrow1"></i><b class="tooltip tip-right-top"><em>Select! Policy Starting Date</em></b>
</label>
</span>

<span id="month">
<label class="field select">
<select class="calculate percentage" id="vec_policy_date2" name="vec_policy_month2"  required>
<option value=""> Month </option>
<option value="1">January</option>
<option value="2">February</option>
<option value="3">March</option>
<option value="4">April</option>
<option value="5">May</option>
<option value="6">June</option>
<option value="7">July</option>
<option value="8">August</option>
<option value="9">September</option>
<option value="10">October</option>
<option value="11">November</option>
<option value="12">December</option>
</select><i class="fa fa-chevron-down arrow1"></i><b class="tooltip tip-right-top"><em>Select! Policy Starting Month</em></b>
</label>
</span>

<span id="phone1">
<label class="field select">
<select class="calculate percentage" id="vec_policy_date3" name="vec_policy_date2"  required>
<option value=""> Year </option>
<option value="2018">2018</option>
</select><i class="fa fa-chevron-down arrow1"></i><b class="tooltip tip-right-top"><em>Select! Policy Starting Year 2018</em></b>
</label>
</span>
</div>
</div>
</div>

</fieldset>
<h2>Driver Information</h2>
<fieldset>

<!-- STEP 3 HTML MARKUP GOES HERE-->

<div class="frm-row">
<div class="section colm colm6"><p class="step2_labels">Your Full Name is</p></div>
<div class="section colm colm6">
<label for="firstName" class="field prepend-icon">
<input type="text" name="firstName" placeholder="Your Full Name" class="gui-input required percentage" id="firstname" value=""  required>
<b class="tooltip tip-right-top"><em>Please! Fill Your Full Name</em></b>
<span class="field-icon"><i class="fa fa-user"></i></span>
</label>
</div>
</div>


<div class="frm-row">
<div class="section colm colm6"><p class="step2_labels">Your Contact Number is</p></div>
<div class="section colm colm2">
<label class="field select">
<select class="calculate percentage" id="contact_num1" name="contact_num1"  required>
<option  value="" selected> M Code </option>
<option  value="050"> 050 </option>
<option  value="052"> 052 </option>
<option  value="053"> 053 </option>
<option  value="054"> 054 </option>
<option  value="055"> 055 </option>
<option  value="056"> 056 </option>
<option  value="058"> 058 </option>
</select><i class="fa fa-chevron-down arrow1"></i><b class="tooltip tip-right-top"><em>Your Mobile Code</em></b>
</label>
</div>

<div class="section colm colm4">
<label class="field prepend-icon">
<input type="text" name="contact_num2" placeholder="e.g. 8234567" class="gui-input percentage" id="contact_num2" value=""  required>
<b class="tooltip tip-right-top"><em>Please Enter a Valid Phone Number</em></b>
<span class="field-icon"><i class="fa fa-phone-square"></i></span>
</label>
</div>
</div>


<div class="frm-row">
<div class="section colm colm6">
<p class="step1_labels">Your Email Address is</p>
</div>
<div class="section colm colm6">
<label for="email_address" class="field prepend-icon">
<input type="email" placeholder="Email Address" class="gui-input required percentage" id="email_address" name="email_address" />
<b class="tooltip tip-right-top"><em>Write Email address..then! we will send you policy documents.</em></b>
<span class="field-icon"><i class="fa fa-envelope"></i></span>
</label>
</div>
</div>


<div class="frm-row">
<div class="section colm colm6">
<p class="step2_labels">Your Nationality is</p>
</div>
<div class="section colm colm6">

<label class="field select">
<select class="calculate percentage" id="nationality" name="packaging"  required>
<option value="">--- My Nationality is ---</option>

<option value="afghan">Afghan</option>
<option value="albanian">Albanian</option>
<option value="algerian">Algerian</option>
<option value="american">American</option>
<option value="andorran">Andorran</option>
<option value="angolan">Angolan</option>
<option value="antiguans">Antiguans</option>
<option value="argentinean">Argentinean</option>
<option value="armenian">Armenian</option>
<option value="australian">Australian</option>
<option value="austrian">Austrian</option>
<option value="azerbaijani">Azerbaijani</option>
<option value="bahamian">Bahamian</option>
<option value="bahraini">Bahraini</option>
<option value="bangladeshi">Bangladeshi</option>
<option value="barbadian">Barbadian</option>
<option value="barbudans">Barbudans</option>
<option value="batswana">Batswana</option>
<option value="belarusian">Belarusian</option>
<option value="belgian">Belgian</option>
<option value="belizean">Belizean</option>
<option value="beninese">Beninese</option>
<option value="bhutanese">Bhutanese</option>
<option value="bolivian">Bolivian</option>
<option value="bosnian">Bosnian</option>
<option value="brazilian">Brazilian</option>
<option value="british">British</option>
<option value="bruneian">Bruneian</option>
<option value="bulgarian">Bulgarian</option>
<option value="burkinabe">Burkinabe</option>
<option value="burmese">Burmese</option>
<option value="burundian">Burundian</option>
<option value="cambodian">Cambodian</option>
<option value="cameroonian">Cameroonian</option>
<option value="canadian">Canadian</option>
<option value="cape verdean">Cape Verdean</option>
<option value="central african">Central African</option>
<option value="chadian">Chadian</option>
<option value="chilean">Chilean</option>
<option value="chinese">Chinese</option>
<option value="colombian">Colombian</option>
<option value="comoran">Comoran</option>
<option value="congolese">Congolese</option>
<option value="costa rican">Costa Rican</option>
<option value="croatian">Croatian</option>
<option value="cuban">Cuban</option>
<option value="cypriot">Cypriot</option>
<option value="czech">Czech</option>
<option value="danish">Danish</option>
<option value="djibouti">Djibouti</option>
<option value="dominican">Dominican</option>
<option value="dutch">Dutch</option>
<option value="east timorese">East Timorese</option>
<option value="ecuadorean">Ecuadorean</option>
<option value="egyptian">Egyptian</option>
<option value="equatorial guinean">Equatorial Guinean</option>
<option value="eritrean">Eritrean</option>
<option value="estonian">Estonian</option>
<option value="ethiopian">Ethiopian</option>
<option value="Emirati">Emirati</option>
<option value="emirian">Emirian</option>
<option value="fijian">Fijian</option>
<option value="filipino">Filipino</option>
<option value="finnish">Finnish</option>
<option value="french">French</option>
<option value="gabonese">Gabonese</option>
<option value="gambian">Gambian</option>
<option value="georgian">Georgian</option>
<option value="german">German</option>
<option value="ghanaian">Ghanaian</option>
<option value="greek">Greek</option>
<option value="grenadian">Grenadian</option>
<option value="guatemalan">Guatemalan</option>
<option value="guinea-bissauan">Guinea-Bissauan</option>
<option value="guinean">Guinean</option>
<option value="guyanese">Guyanese</option>
<option value="haitian">Haitian</option>
<option value="herzegovinian">Herzegovinian</option>
<option value="honduran">Honduran</option>
<option value="hungarian">Hungarian</option>
<option value="icelander">Icelander</option>
<option value="indian">Indian</option>
<option value="indonesian">Indonesian</option>
<option value="iranian">Iranian</option>
<option value="iraqi">Iraqi</option>
<option value="irish">Irish</option>
<option value="israeli">Israeli</option>
<option value="italian">Italian</option>
<option value="ivorian">Ivorian</option>
<option value="jamaican">Jamaican</option>
<option value="japanese">Japanese</option>
<option value="jordanian">Jordanian</option>
<option value="kazakhstani">Kazakhstani</option>
<option value="kenyan">Kenyan</option>
<option value="kittian and nevisian">Kittian and Nevisian</option>
<option value="kuwaiti">Kuwaiti</option>
<option value="kyrgyz">Kyrgyz</option>
<option value="laotian">Laotian</option>
<option value="latvian">Latvian</option>
<option value="lebanese">Lebanese</option>
<option value="liberian">Liberian</option>
<option value="libyan">Libyan</option>
<option value="liechtensteiner">Liechtensteiner</option>
<option value="lithuanian">Lithuanian</option>
<option value="luxembourger">Luxembourger</option>
<option value="macedonian">Macedonian</option>
<option value="malagasy">Malagasy</option>
<option value="malawian">Malawian</option>
<option value="malaysian">Malaysian</option>
<option value="maldivan">Maldivan</option>
<option value="malian">Malian</option>
<option value="maltese">Maltese</option>
<option value="marshallese">Marshallese</option>
<option value="mauritanian">Mauritanian</option>
<option value="mauritian">Mauritian</option>
<option value="mexican">Mexican</option>
<option value="micronesian">Micronesian</option>
<option value="moldovan">Moldovan</option>
<option value="monacan">Monacan</option>
<option value="mongolian">Mongolian</option>
<option value="moroccan">Moroccan</option>
<option value="mosotho">Mosotho</option>
<option value="motswana">Motswana</option>
<option value="mozambican">Mozambican</option>
<option value="namibian">Namibian</option>
<option value="nauruan">Nauruan</option>
<option value="nepalese">Nepalese</option>
<option value="new zealander">New Zealander</option>
<option value="ni-vanuatu">Ni-Vanuatu</option>
<option value="nicaraguan">Nicaraguan</option>
<option value="nigerien">Nigerien</option>
<option value="north korean">North Korean</option>
<option value="northern irish">Northern Irish</option>
<option value="norwegian">Norwegian</option>
<option value="omani">Omani</option>
<option value="pakistani">Pakistani</option>
<option value="palauan">Palauan</option>
<option value="panamanian">Panamanian</option>
<option value="papua new guinean">Papua New Guinean</option>
<option value="paraguayan">Paraguayan</option>
<option value="peruvian">Peruvian</option>
<option value="Philippine">Philippine</opion>
<option value="polish">Polish</option>
<option value="portuguese">Portuguese</option>
<option value="qatari">Qatari</option>
<option value="romanian">Romanian</option>
<option value="russian">Russian</option>
<option value="rwandan">Rwandan</option>
<option value="saint lucian">Saint Lucian</option>
<option value="salvadoran">Salvadoran</option>
<option value="samoan">Samoan</option>
<option value="san marinese">San Marinese</option>
<option value="sao tomean">Sao Tomean</option>
<option value="saudi">Saudi</option>
<option value="scottish">Scottish</option>
<option value="senegalese">Senegalese</option>
<option value="serbian">Serbian</option>
<option value="seychellois">Seychellois</option>
<option value="sierra leonean">Sierra Leonean</option>
<option value="singaporean">Singaporean</option>
<option value="slovakian">Slovakian</option>
<option value="slovenian">Slovenian</option>
<option value="solomon islander">Solomon Islander</option>
<option value="somali">Somali</option>
<option value="south african">South African</option>
<option value="south korean">South Korean</option>
<option value="spanish">Spanish</option>
<option value="sri lankan">Sri Lankan</option>
<option value="sudanese">Sudanese</option>
<option value="surinamer">Surinamer</option>
<option value="swazi">Swazi</option>
<option value="swedish">Swedish</option>
<option value="swiss">Swiss</option>
<option value="syrian">Syrian</option>
<option value="taiwanese">Taiwanese</option>
<option value="tajik">Tajik</option>
<option value="tanzanian">Tanzanian</option>
<option value="thai">Thai</option>
<option value="togolese">Togolese</option>
<option value="tongan">Tongan</option>
<option value="tunisian">Tunisian</option>
<option value="turkish">Turkish</option>
<option value="tuvaluan">Tuvaluan</option>
<option value="ugandan">Ugandan</option>
<option value="ukrainian">Ukrainian</option>
<option value="uruguayan">Uruguayan</option>
<option value="uzbekistani">Uzbekistani</option>
<option value="venezuelan">Venezuelan</option>
<option value="vietnamese">Vietnamese</option>
<option value="welsh">Welsh</option>
<option value="yemenite">Yemenite</option>
<option value="zambian">Zambian</option>
<option value="zimbabwean">Zimbabwean</option>
</select><i class="fa fa-chevron-down arrow1"></i><b class="tooltip tip-right-top"><em>Select Your Nationality. for example: i'am Emirati</em></b>
</label>
</div>
</div>



<div class="frm-row">
<div class="section colm colm6">
<p class="step2_labels">Your Date of Birth</p>
</div>
<div class="section colm colm6  percentage">
<label class="field prepend-icon">
<input type="text" name="datepicker1" placeholder="DD-MM-YYY" id="datepicker1" class="gui-input"  required>
<b class="tooltip tip-right-top"><em>Please Select Your Date of Birth (MM/DD/YY)</em></b>
<span class="field-icon"><i class="fa fa-calendar"></i></span>
</label>
</div>
<script type="text/javascript">
jQuery(function() {
jQuery("#datepicker1").datepicker({
numberOfMonths: 1,
prevText: '<i class="fa fa-chevron-left"></i>',
nextText: '<i class="fa fa-chevron-right"></i>',
showButtonPanel: false
});
});
</script>
</div>

<div class="frm-row" id="year_uae_driving_licence">
<div class="section colm colm6">
<p class="step2_labels">How many Years of Driving Experience in UAE</p>
</div>

<div class="section colm colm6">
<label class="field select">
<select class="calculate percentage" id="years_uaedrivinglicence" name="years_uaedrivinglicence"  required>
<option class="option" value="" >--- Select How many Years ---</option>
<option value="less then one year" > Less then one Year </option>
<option value="1 year" > 1 Year </option>
<option value="2 years" > 2 Years </option>
<option value="3 years" > 3 Years </option>
<option value="more then 4 years"> More then 4 Years </option>
</select><i class="fa fa-chevron-down arrow1"></i><b class="tooltip tip-right-top"><em>Your Total Driving Experience in UAE Country</em></b>
</label>
</div>
</div>

<div class="frm-row">

<div class="section colm colm6">

<p class="step2_labels">Country of Your First Driver's license</p>

</div>

<div class="section colm colm6">
<label class="field select">
<select class="calculate percentage" id="country_firstdl" name="country_firstdl"  required>
<option class="option" value="" >-- Select First Driving Country --</option>
<option class="option" value="United Arab Emirates">United Arab Emirates</option>
<option class="option" value="Albania">Albania</option>
<option class="option" value="Algeria">Algeria</option>
<option class="option" value="American Samoa">American Samoa</option>
<option class="option" value="Andorra">Andorra</option>
<option class="option" value="Angola">Angola</option>
<option class="option" value="Anguilla">Anguilla</option>
<option class="option" value="Antarctica">Antarctica</option>
<option class="option" value="Antigua and Barbuda">Antigua and Barbuda</option>
<option class="option" value="Argentina">Argentina</option>
<option class="option" value="Armenia">Armenia</option>
<option class="option" value="Aruba">Aruba</option>
<option class="option" value="Australia">Australia</option>
<option class="option" value="Austria">Austria</option>
<option class="option" value="Azerbaijan">Azerbaijan</option>
<option class="option" value="Bahamas">Bahamas</option>
<option class="option" value="Bahrain">Bahrain</option>
<option class="option" value="Bangladesh">Bangladesh</option>
<option class="option" value="Barbados">Barbados</option>
<option class="option" value="Belarus">Belarus</option>
<option class="option" value="Belgium">Belgium</option>
<option class="option" value="Belize">Belize</option>
<option class="option" value="Benin">Benin</option>
<option class="option" value="Bermuda">Bermuda</option>
<option class="option" value="Bhutan">Bhutan</option>
<option class="option" value="Bolivia">Bolivia</option>
<option class="option" value="Bosnia and Herzegovina">Bosnia and Herzegovina</option>
<option class="option" value="Botswana">Botswana</option>
<option class="option" value="Bouvet Island">Bouvet Island</option>
<option class="option" value="Brazil">Brazil</option>
<option class="option" value="Brunei Darussalam">Brunei Darussalam</option>
<option class="option" value="Bulgaria">Bulgaria</option>
<option class="option" value="Burkina Faso">Burkina Faso</option>
<option class="option" value="Burundi">Burundi</option>
<option class="option" value="Cambodia">Cambodia</option>
<option class="option" value="Cameroon">Cameroon</option>
<option class="option" value="Canada">Canada</option>
<option class="option" value="Cape Verde">Cape Verde</option>
<option class="option" value="Cayman Islands">Cayman Islands</option>
<option class="option" value="Central African Republic">Central African Republic</option>
<option class="option" value="Chad">Chad</option>
<option class="option" value="Chile">Chile</option>
<option class="option" value="China">China</option>
<option class="option" value="Christmas Island">Christmas Island</option>
<option class="option" value="Cocos (Keeling) Islands">Cocos (Keeling) Islands</option>
<option class="option" value="Colombia">Colombia</option>
<option class="option" value="Comoros">Comoros</option>
<option class="option" value="Congo">Congo</option>
<option class="option" value="Cook Islands">Cook Islands</option>
<option class="option" value="Costa Rica">Costa Rica</option>
<option class="option" value="Cote optionoire">Cote optionoire</option>
<option class="option" value="Croatia">Croatia</option>
<option class="option" value="Cuba">Cuba</option>
<option class="option" value="Cyprus">Cyprus</option>
<option class="option" value="Czech Republic">Czech Republic</option>
<option class="option" value="Denmark">Denmark</option>
<option class="option" value="Djibouti">Djibouti</option>
<option class="option" value="Dominica">Dominica</option>
<option class="option" value="Dominican Republic">Dominican Republic</option>
<option class="option" value="East Timor">East Timor</option>
<option class="option" value="Ecuador">Ecuador</option>
<option class="option" value="Egypt">Egypt</option>
<option class="option" value="El Salvador">El Salvador</option>
<option class="option" value="Equatorial Guinea">Equatorial Guinea</option>
<option class="option" value="Eritrea">Eritrea</option>
<option class="option" value="Estonia">Estonia</option>
<option class="option" value="Ethiopia">Ethiopia</option>
<option class="option" value="Faroe Islands">Faroe Islands</option>
<option class="option" value="Fiji">Fiji</option>
<option class="option" value="Finland">Finland</option>
<option class="option" value="France">France</option>
<option class="option" value="France Metropolitan">France Metropolitan</option>
<option class="option" value="French Guiana">French Guiana</option>
<option class="option" value="French Polynesia">French Polynesia</option>
<option class="option" value="Gabon">Gabon</option>
<option class="option" value="Gambia">Gambia</option>
<option class="option" value="Georgia">Georgia</option>
<option class="option" value="Germany">Germany</option>
<option class="option" value="Ghana">Ghana</option>
<option class="option" value="Gibraltar">Gibraltar</option>
<option class="option" value="Greece">Greece</option>
<option class="option" value="Greenland">Greenland</option>
<option class="option" value="Grenada">Grenada</option>
<option class="option" value="Guadeloupe">Guadeloupe</option>
<option class="option" value="Guam">Guam</option>
<option class="option" value="Guatemala">Guatemala</option>
<option class="option" value="Guinea">Guinea</option>
<option class="option" value="Guinea-bissau">Guinea-bissau</option>
<option class="option" value="Guyana">Guyana</option>
<option class="option" value="Haiti">Haiti</option>
<option class="option" value="Honduras">Honduras</option>
<option class="option" value="Hong Kong">Hong Kong</option>
<option class="option" value="Hungary">Hungary</option>
<option class="option" value="Iceland">Iceland</option>
<option class="option" value="India">India</option>
<option class="option" value="Indonesia">Indonesia</option>
<option class="option" value="Iran">Iran</option>
<option class="option" value="Iraq">Iraq</option>
<option class="option" value="Ireland">Ireland</option>
<option class="option" value="Italy">Italy</option>
<option class="option" value="Jamaica">Jamaica</option>
<option class="option" value="Japan">Japan</option>
<option class="option" value="Jordan">Jordan</option>
<option class="option" value="Kazakhstan">Kazakhstan</option>
<option class="option" value="Kenya">Kenya</option>
<option class="option" value="Kiribati">Kiribati</option>
<option class="option" value="Korea">Korea</option>
<option class="option" value="Kuwait">Kuwait</option>
<option class="option" value="Kyrgyzstan">Kyrgyzstan</option>
<option class="option" value="Latvia">Latvia</option>
<option class="option" value="Lebanon">Lebanon</option>
<option class="option" value="Lesotho">Lesotho</option>
<option class="option" value="Liberia">Liberia</option>
<option class="option" value="Libya">Libya</option>
<option class="option" value="Liechtenstein">Liechtenstein</option>
<option class="option" value="Lithuania">Lithuania</option>
<option class="option" value="Luxembourg">Luxembourg</option>
<option class="option" value="Macau">Macau</option>
<option class="option" value="Macedonia">Macedonia</option>
<option class="option" value="Madagascar">Madagascar</option>
<option class="option" value="Malawi">Malawi</option>
<option class="option" value="Malaysia">Malaysia</option>
<option class="option" value="Maloptiones">Maloptiones</option>
<option class="option" value="Mali">Mali</option>
<option class="option" value="Malta">Malta</option>
<option class="option" value="Martinique">Martinique</option>
<option class="option" value="Mauritania">Mauritania</option>
<option class="option" value="Mauritius">Mauritius</option>
<option class="option" value="Mayotte">Mayotte</option>
<option class="option" value="Mexico">Mexico</option>
<option class="option" value="Monaco">Monaco</option>
<option class="option" value="Mongolia">Mongolia</option>
<option class="option" value="Montserrat">Montserrat</option>
<option class="option" value="Morocco">Morocco</option>
<option class="option" value="Mozambique">Mozambique</option>
<option class="option" value="Myanmar">Myanmar</option>
<option class="option" value="Namibia">Namibia</option>
<option class="option" value="Nauru">Nauru</option>
<option class="option" value="Nepal">Nepal</option>
<option class="option" value="Netherlands">Netherlands</option>
<option class="option" value="New Caledonia">New Caledonia</option>
<option class="option" value="New Zealand">New Zealand</option>
<option class="option" value="Nicaragua">Nicaragua</option>
<option class="option" value="Niger">Niger</option>
<option class="option" value="Nigeria">Nigeria</option>
<option class="option" value="Niue">Niue</option>
<option class="option" value="Norfolk Island">Norfolk Island</option>
<option class="option" value="Norway">Norway</option>
<option class="option" value="Oman">Oman</option>
<option class="option" value="Pakistan">Pakistan</option>
<option class="option" value="Palau">Palau</option>
<option class="option" value="Palestine">Palestine</option>
<option class="option" value="Panama">Panama</option>
<option class="option" value="Papua New Guinea">Papua New Guinea</option>
<option class="option" value="Paraguay">Paraguay</option>
<option class="option" value="Peru">Peru</option>
<option class="option" value="Philippines">Philippines</option>
<option class="option" value="Pitcairn">Pitcairn</option>
<option class="option" value="Poland">Poland</option>
<option class="option" value="Portugal">Portugal</option>
<option class="option" value="Puerto Rico">Puerto Rico</option>
<option class="option" value="Qatar">Qatar</option>
<option class="option" value="Romania">Romania</option>
<option class="option" value="Russia">Russia</option>
<option class="option" value="Rwanda">Rwanda</option>
<option class="option" value="Saint Kitts and Nevis">Saint Kitts and Nevis</option>
<option class="option" value="Saint Lucia">Saint Lucia</option>
<option class="option" value="Samoa">Samoa</option>
<option class="option" value="San Marino">San Marino</option>
<option class="option" value="Sao Tome and Principe">Sao Tome and Principe</option>
<option class="option" value="Saudi Arabia">Saudi Arabia</option>
<option class="option" value="Serbia">Serbia</option>
<option class="option" value="Senegal">Senegal</option>
<option class="option" value="Seychelles">Seychelles</option>
<option class="option" value="Sierra Leone">Sierra Leone</option>
<option class="option" value="Singapore">Singapore</option>
<option class="option" value="Slovakia (Slovak Republic)">Slovakia (Slovak Republic)</option>
<option class="option" value="Slovenia">Slovenia</option>
<option class="option" value="Solomon Islands">Solomon Islands</option>
<option class="option" value="Somalia">Somalia</option>
<option class="option" value="South Africa">South Africa</option>
<option class="option" value="Spain">Spain</option>
<option class="option" value="Sri Lanka">Sri Lanka</option>
<option class="option" value="St. Helena">St. Helena</option>
<option class="option" value="Sudan">Sudan</option>
<option class="option" value="Suriname">Suriname</option>
<option class="option" value="Swaziland">Swaziland</option>
<option class="option" value="Sweden">Sweden</option>
<option class="option" value="Switzerland">Switzerland</option>
<option class="option" value="Syrian Arab Republic">Syrian Arab Republic</option>
<option class="option" value="Taiwan">Taiwan</option>
<option class="option" value="Tajikistan">Tajikistan</option>
<option class="option" value="Thailand">Thailand</option>
<option class="option" value="Togo">Togo</option>
<option class="option" value="Tokelau">Tokelau</option>
<option class="option" value="Tonga">Tonga</option>
<option class="option" value="Trinidad and Tobago">Trinidad and Tobago</option>
<option class="option" value="Tunisia">Tunisia</option>
<option class="option" value="Turkey">Turkey</option>
<option class="option" value="Turkmenistan">Turkmenistan</option>
<option class="option" value="Tuvalu">Tuvalu</option>
<option class="option" value="Uganda">Uganda</option>
<option class="option" value="Ukraine">Ukraine</option>
<option class="option" value="United Arab Emirates">United Arab Emirates</option>
<option class="option" value="United Kingdom">United Kingdom</option>
<option class="option" value="United States">United States</option>
<option class="option" value="Uruguay">Uruguay</option>
<option class="option" value="Uzbekistan">Uzbekistan</option>
<option class="option" value="Vanuatu">Vanuatu</option>
<option class="option" value="Venezuela">Venezuela</option>
<option class="option" value="Viet Nam">Viet Nam</option>
<option class="option" value="Virgin Islands (British)">Virgin Islands (British)</option>
<option class="option" value="Virgin Islands (U.S.)">Virgin Islands (U.S.)</option>
<option class="option" value="Wallis and Futuna Islands">Wallis and Futuna Islands</option>
<option class="option" value="Western Sahara">Western Sahara</option>
<option class="option" value="Yemen">Yemen</option>
<option class="option" value="Yugoslavia">Yugoslavia</option>
<option class="option" value="Zambia">Zambia</option>
<option class="option" value="Zimbabwe">Zimbabwe</option>
</option>
</select><i class="fa fa-chevron-down arrow1"></i><b class="tooltip tip-right-top"><em>Your First Driving license Country </em></b>
</label>
</div>
</div>

<div class="actions_c clearfix">
<ul aria-label="Pagination">
	<li style="display: list-item;">
	<a href="#" id="qoute_custom">Get Qoute</a></li>
</ul>
</div>


</fieldset>
</form>
</div>
</div>
</div>
</div>

<style>
.icomoon-checkmark-circle.false:before {
    content: "\ed66" !important;
}
</style>

<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>

<!-- Jquery  Code -->

<?php

$json_form_setVehicleInfo = json_encode($form_setVehicleInfo);
$json_coverage_dataTpl = json_encode($coverage_dataTpl);
$uniq_id = uniqid();
?>

<script>

jQuery(document).ready(function() {

	jQuery( "#step5" ).hide();
	jQuery( "#bread_crums_custom" ).hide();
	localStorage.setItem("policyType_final","Private");
	jQuery("#vehicle_val").prop('disabled', true);
	jQuery("#car_value_details").hide();
	jQuery("#model_year").prop('disabled', true);
	jQuery("#vehicle_model_detail").prop('disabled', true);


	//Get Qoute Custom Click
	jQuery( "#qoute_custom" ).click(function() {
	var veh_fullname_filled = jQuery('#firstname').val();
	var veh_contact_num2 = jQuery('#contact_num2').val();
    var veh_email_address = jQuery('#email_address').val();
    var veh_nationality = jQuery('#nationality option:selected').val();
    var years_uaedrivinglicence = jQuery('#years_uaedrivinglicence option:selected').val();
    var country_firstdl = jQuery('#country_firstdl option:selected').val();
    var age_less_25 = jQuery('#datepicker1').val();

    if(!validate_date(age_less_25)){
            alert('Invalid Date');
	 }
	 // else{
  //           alert('Invalid Date');
	 // }  	
	 else if((veh_fullname_filled == "") || (country_firstdl == "")){
  			alert("Please fill the Driver Information Frist");
	 }
	 else if((veh_contact_num2 == "") || (veh_email_address == 'none')){
	  		alert("Please fill the Driver Information Frist");
	 }
	 else if((veh_nationality == "") || (years_uaedrivinglicence == "")){
	  		alert("Please fill the Driver Information Frist");
	 }
   		
  
  	else{
		jQuery( "#step4" ).show();
		jQuery( "#step5" ).hide();
		jQuery( "#form_main_div" ).hide();
		jQuery( ".form_desc" ).hide();
		//jQuery( ".actions ul li" ).last().css( "display", "block" );
	}
});


function validate_date(age_less_25){
var currVal = age_less_25;
    if(currVal == '')
        return false;
    
    var rxDatePattern = /^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{4})$/; //Declare Regex
    var dtArray = currVal.match(rxDatePattern); // is format OK?
    
    if (dtArray == null) 
        return false;
    
    //Checks for mm/dd/yyyy format.
    dtMonth = dtArray[1];
    dtDay= dtArray[3];
    dtYear = dtArray[5];        
    
    if (dtMonth < 1 || dtMonth > 12) 
        return false;
    else if (dtDay < 1 || dtDay> 31) 
        return false;
    else if ((dtMonth==4 || dtMonth==6 || dtMonth==9 || dtMonth==11) && dtDay ==31) 
        return false;
    else if (dtMonth == 2) 
    {
        var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));
        if (dtDay> 29 || (dtDay ==29 && !isleap)) 
                return false;
    }
    return true;
}





jQuery( "#goto_table" ).click(function() {
	jQuery( "#step4" ).show();
	jQuery( "#step5" ).hide();
	jQuery( "#form_main_div" ).hide();
	jQuery( ".form_desc" ).hide();
});
jQuery( "#goto_form" ).click(function() {
	jQuery( "#step4" ).hide();
	jQuery( "#step5" ).hide();
	jQuery( "#form_main_div" ).show();
	jQuery( ".form_desc" ).hide();
});

//Model Condition
jQuery( "#popup_dangermain111" ).hide();
jQuery( "#popup_infomain" ).hide();
var ideal_date_21    = "01/1/1996"

//Hide Popup Danger on Ok Click
jQuery( "#danger_ok" ).click(function() {
jQuery( "#popup_dangermain111" ).hide();
});

jQuery( "#dangerPolicy_ok" ).click(function() {
jQuery( "#popup_dangerPolicyStartDate" ).hide();
});

jQuery( "#info_ok" ).click(function() {
jQuery( "#popup_infomain" ).hide();
});

jQuery( "#info_ok_brand_new" ).click(function() {
jQuery( "#popup_infomain_brand_new" ).hide();
});

jQuery( "#ins_expired_danger_ok" ).click(function() {
jQuery( "#popup_current_ins_expired" ).hide();
});

jQuery( "#veh_notcommercial_ok" ).click(function() {
jQuery( "#vehicle_notfor_commercialpurpose" ).hide();
});

jQuery( "#info_outsideuae_ok" ).click(function() {
jQuery( "#popup_outsideuae_cover" ).hide();
});

jQuery( "#pa_coverDriver_ok" ).click(function() {
jQuery( "#popup_pa_coverDriver" ).hide();
});

var ideal_date_21    = "01/1/1992"

jQuery( "#datepicker1" ).change(function() {

var age_less_25 = jQuery('#datepicker1').val();
////console.log("ideal_date_21.."+ideal_date_21);
////console.log("age_less_25.."+age_less_25);

if(new Date(age_less_25) > new Date(ideal_date_21)){
////console.log("age is less then 21 ...");
jQuery( "#popup_dangermain111" ).show();
}else{
jQuery( "#popup_dangermain111" ).hide();
}
});

var getCanvas; // global variable
//jQuery("#rt_react").on('click', function () {
function capture_precheckout(){
//console.log("Image render check");
html2canvas(document.body, { type: 'view' }).then(function(canvas) {
getCanvas = canvas;

var imgageData = getCanvas.toDataURL("image/png");

});
//});
}

//When would you like the Policy to start..
jQuery( "#vec_policy_date1" ).change(function() {
checkdaysPolicy();
});
jQuery( "#vec_policy_date2" ).change(function() {
checkdaysPolicy();
});
jQuery( "#vec_policy_date3" ).change(function() {
checkdaysPolicy();
});

function checkdaysPolicy(){
var vec_policy_date1 = jQuery('#vec_policy_date1 option:selected').val();
var vec_policy_date2 = jQuery('#vec_policy_date2 option:selected').val();
var vec_policy_date3 = jQuery('#vec_policy_date3 option:selected').val();

var selected_Policydate = '"'+vec_policy_date2+'/'+vec_policy_date1+'/'+vec_policy_date3+'"';
//console.log("selected_Policydate.."+selected_Policydate);

///
var numDaysBetween = function(d1, d2) {
var diff = Math.abs(d1.getTime() - d2.getTime());
return diff / (1000 * 60 * 60 * 24);
};

var d1 = new Date(selected_Policydate); // Jan 1, 2011
var d2 = new Date(); // Jan 2, 2011
var difference = numDaysBetween(d1, d2); // => 1
//console.log("d1.."+d1);
//console.log("d2.."+d2);
//console.log("difference.."+difference);
if(difference > 60){
//console.log("you have selected more then 60 days.");
//jQuery( "#popup_dangerPolicyStartDate" ).show();
}else{
//console.log("you have selected Within 60 days.");
}
}



/* var today = new Date();
if(new Date(selected_Policydate) > (new Date(today.getFullYear(), today.getMonth(), today.getDate()+60)) ){
//console.log("you have selected more then 60 days.");
jQuery( "#popup_dangerPolicyStartDate" ).show();
}else{
//console.log("you have selected Within 60 days.");
}
}); */
/* jQuery( "#vec_policy_date2" ).change(function() {

var vec_policy_date1 = jQuery('#vec_policy_date1 option:selected').val();
var vec_policy_date2 = jQuery('#vec_policy_date2 option:selected').val();
var vec_policy_date3 = jQuery('#vec_policy_date3 option:selected').val();

var selected_Policydate = '"'+vec_policy_date1+'/'+vec_policy_date2+'/'+vec_policy_date3+'"';
//console.log("selected_Policydate.."+selected_Policydate);

var today = new Date();
if(new Date(selected_Policydate) > (new Date(today.getFullYear(), today.getMonth(), today.getDate()+60)) ){
//console.log("you have selected more then 60 days.");
jQuery( "#popup_dangerPolicyStartDate" ).show();
}else{
//console.log("you have selected Within 60 days.");
}
}); */
/* jQuery( "#vec_policy_date3" ).change(function() {

var vec_policy_date1 = jQuery('#vec_policy_date1 option:selected').val();
var vec_policy_date2 = jQuery('#vec_policy_date2 option:selected').val();
var vec_policy_date3 = jQuery('#vec_policy_date3 option:selected').val();

var selected_Policydate = '"'+vec_policy_date1+'/'+vec_policy_date2+'/'+vec_policy_date3+'"';
//console.log("selected_Policydate.."+selected_Policydate);

var today = new Date();
if(new Date(selected_Policydate) > (new Date(today.getDate()+60, today.getMonth(), today.getFullYear())) ){
//console.log("you have selected more then 60 days.");
jQuery( "#popup_dangerPolicyStartDate" ).show();
}else{
//console.log("you have selected Within 60 days.");
}
}); */

function refreshPrice(){
}

jQuery(function () {
jQuery('#reg_date').datepicker({
format: 'mm/dd/yyyy',
startDate: '-3d'
});
});

jQuery(function () {
jQuery('#polstart_date').datepicker({
format: 'mm/dd/yyyy',
startDate: '-3d'
});
});

jQuery(function () {
jQuery('#dob').datepicker({
format: 'mm/dd/yyyy',
startDate: '-3d'
});
});

jQuery(".percentage").change(function(){

var veh_fullname_filled = jQuery('#firstname').val();
jQuery('#user_filled').html(veh_fullname_filled);

get_companies();
jQuery(this).addClass("changed");
var change_count = jQuery('.changed').length;
var currentval = parseFloat(change_count);
currentval1 = currentval * 2.94;
currentval11 = currentval1.toFixed(1);
jQuery("#percentage-text").html(currentval11+'%');

var circlefill = 'p'+currentval1;
jQuery( "#progress:last" ).addClass( circlefill );
});

jQuery('[data-toggle="tooltip"]').tooltip();
jQuery("#btn_step1").addClass("active_step");

jQuery("#btn_step1").click(function(){
jQuery("#btn_step2").removeClass("active_step");
jQuery("#btn_step1").addClass("active_step");
});

jQuery("#btn_step2").click(function(){
jQuery("#btn_step1").removeClass("active_step");
jQuery("#btn_step2").addClass("active_step");
});

jQuery("#btn_nextstep2inner").click(function(){
jQuery("#btn_step1").removeClass("active_step");
jQuery("#btn_step2").removeClass("active_step");
});

var basePrice = 3.00;
var width;

//jQuery( "#ins_submodel" ).hide();
jQuery( "#step2" ).hide();
jQuery( "#step3" ).hide();
jQuery( "#step4" ).hide();
jQuery("#calCheckout_url").hide();

jQuery( "#btn_step2" ).click(function() {
jQuery( "#step1" ).hide();
jQuery( "#step2" ).show();
});

jQuery( "#btn_nextstep1inner" ).click(function() {
jQuery( "#step1" ).hide();
jQuery( "#step2" ).show();
jQuery("#btn_step1").removeClass("active_step");
jQuery("#btn_step2").addClass("active_step");
});

jQuery( "#btn_step1" ).click(function() {
jQuery( "#step2" ).hide();
jQuery( "#step1" ).show();
});

jQuery("#model_year").change(function () {
jQuery("#vehicle_model_detail").prop('disabled', false);
var model_year_switch = jQuery('#model_year option:selected').val();
/* 	if(model_year_switch == "2017" || model_year_switch == "2018"){
jQuery("#fr3").prop('checked', true);
jQuery('#dor_row').addClass( "hide_this" );
}	 */
show_models();
});

jQuery("#vehicle_reg_date3").change(function () {
var vehicle_firstReg_dateYear = jQuery('#vehicle_reg_date3 option:selected').val();
////console.log("vehicle_firstReg_dateYear"+vehicle_firstReg_dateYear);
/* if(vehicle_firstReg_dateYear == "2018"){
jQuery("#fr3").prop('checked', true);
jQuery('#dor_row').addClass( "hide_this" );
} */
});

jQuery("#vehicle_brand").change(function () {
jQuery("#model_year").prop('disabled', false);
show_models();
});

function show_models(){

var step_1datam = <?php echo $json_form_setVehicleInfo; ?>;
var selected_modelYear = jQuery('#model_year option:selected').val();
var vehicle_brand_filter = jQuery('#vehicle_brand option:selected').val();
var final_array = [];
var final_array2 =[];


jQuery.each( step_1datam, function( m, brand_filter ) {
if(brand_filter[0] == vehicle_brand_filter && brand_filter[1] == selected_modelYear){
final_array2.push(brand_filter[2]);
}
});

//Set to option Make
jQuery('.modeloption').html('');
jQuery.each( final_array2, function( i, veh_model_set ) {
jQuery('.modeloption').append('<option class="options" value="'+veh_model_set+'">'+veh_model_set+'</option>');
});
//show_minmax();
}

//Disable Car Value field on Model Changed

jQuery("#vehicle_model_detail").change(function () {
jQuery("#vehicle_val").prop('disabled', false);
jQuery("#car_value_details").show();
//show_minmax();
});


//Default value for Personal Accident Cover Driver
localStorage.setItem("pa_cover_driver_check", "No");
localStorage.setItem("outside_uae_cov_value", "No");
localStorage.setItem("rent_car_check_val", "No");

function get_companies(){

//Get selected values

var vehicle_brand = jQuery('#vehicle_brand option:selected').val();
jQuery('#cars_brand').html(vehicle_brand);

//Year Selection Logic
var selected_modelYear = jQuery('#model_year option:selected').val();
var currentYear2 = (new Date).getFullYear();

jQuery('.veh_brand_new').click(function () {
var veh_brandnewcheck = jQuery(this).val();

if(veh_brandnewcheck == "Yes"){
var selected_modelYear = currentYear2;
}

else if(veh_brandnewcheck == "No"){
var selected_modelYear = jQuery('#model_year option:selected').val();
}
});

var vehicle_model_detail = jQuery('#vehicle_model_detail option:selected').val();
jQuery('#cars_model').html(vehicle_model_detail);
var selected_vehicle_repair = jQuery('#per_agency_repair option:selected').val();
var selected_vehicle_modified = jQuery('#vehicle_modified').is(':checked');
var selected_outside_uae = jQuery('#outside_uae').is(':checked');
var selected_roadside_assistance = jQuery('#roadside_assistance').is(':checked');
var selected_ins_rentACar = jQuery('#ins_rentACar').is(':checked');
var need_per_acc_cover_driver = "No";
var acc_cover_passangers = "Yes";
var rent_a_car_val;
var outside_uae_cov_val;

//need_per_acc_cover_driver = localStorage.setItem("pa_cover_driver_check","No");

jQuery('#pa_cover_driver_id input').on('change', function() {
var need_per_acc_cover_driver = jQuery('input[name=per_accidentcover_chk]:checked').val();
localStorage.setItem("pa_cover_driver_check", need_per_acc_cover_driver);
//console.log("need_per_acc_cover_driver.."+need_per_acc_cover_driver);
});

need_per_acc_cover_driver = localStorage.getItem("pa_cover_driver_check");

//console.log("need_per_acc_cover_driver.."+need_per_acc_cover_driver);
if(need_per_acc_cover_driver == "Yes"){
	var need_per_acc_cover_driver1 = "Yes";
	jQuery( "#popup_pa_coverDriver" ).show();
}
else{
	var need_per_acc_cover_driver1 = "No";
}


//Set data to Emai and Policy detail
jQuery('#tpl_pacoverDriverchk_e').html(need_per_acc_cover_driver1);
jQuery('#tpl_pacoverDriverchk').html(need_per_acc_cover_driver1);


jQuery('#pa_cover_passangers_id input').on('change', function() {
var need_per_acc_cover_pass = jQuery('input[name=per_accidentcover_chk]:checked').val();
localStorage.setItem("pa_cover_pass_check", need_per_acc_cover_pass);
//console.log("need_per_acc_cover_pass..Check.."+need_per_acc_cover_pass);
});

jQuery('#rent_a_car_id input').on('change', function() {
var rent_a_car_val = jQuery('input[name=veh_replacement_chk]:checked').val();
localStorage.setItem("rent_car_check_val", rent_a_car_val);
});

rent_car_val_get = localStorage.getItem("rent_car_check_val");
if(rent_car_val_get == "Yes"){
var rent_car_check = rent_car_val_get;
}else{
var rent_car_check = "No";
}

jQuery('#outside_uae_coverage_id input').on('change', function() {
var outside_uae_cov_val = jQuery('input[name=outsideuaeCover_chk]:checked').val();
localStorage.setItem("outside_uae_cov_value", outside_uae_cov_val);
//////console.log("outside_uae_cov_val.."+outside_uae_cov_val);
});

outside_uae_cov_val = localStorage.getItem("outside_uae_cov_value");
if(outside_uae_cov_val == "Yes"){
var outside_uae_cov_inner = outside_uae_cov_val;
}else{
var outside_uae_cov_inner = "No";
}

//Private or Commercial Check
var policyType_inner = "No";

jQuery('#veh_PolicyType_id input').on('change', function() {
var policy_typeCheck_val = jQuery('input[name=ptype_check]:checked').val();

if(policy_typeCheck_val == "Yes"){
policyType_inner = "Yes";
localStorage.setItem("policyType_final","Private");
}
else if(policy_typeCheck_val == "No"){
policyType_inner = "No";
localStorage.setItem("policyType_final","Commercial");
}
});
var policyType_result = localStorage.getItem("policyType_final");
//console.log("policyType_result.."+policyType_result);

var sel_cylenders = jQuery('#veh_cylenders option:selected').val();

var pa_cover_passangers = jQuery('#pa_cover_pass option:selected').val();
localStorage.setItem("pa_cover_pass_check",pa_cover_passangers);

var ideal_date    = "01/1/1992"
var ideal_date_21    = "01/1/1996"
var age_less_25 = jQuery('#datepicker1').val();

//Select Options End
var selected_category_tpl;

//Step1 and selected Data arrays
var step_1data = <?php echo $json_form_setVehicleInfo; ?>;
var step_dataTpl = <?php echo $json_coverage_dataTpl; ?>;

jQuery.each( step_1data, function( j, step_1Values ) {
//Compare Step1 data to get category
if(vehicle_brand == step_1Values[0] && selected_modelYear == step_1Values[1] && vehicle_model_detail == step_1Values[2]){
selected_category_tpl = step_1Values[3];
localStorage.setItem("selected_cat",selected_category_tpl);
//console.log("selected_category_tpl.."+selected_category_tpl);
}
});

var rt_min_val = 0;
var all_companies = [];
jQuery.each( step_dataTpl, function( j, valuecomp ) {

var get_selectedYear_agRepair = jQuery('#model_year option:selected').val();
var cat_selected = localStorage.getItem("selected_cat");
var csv_cylenders = valuecomp[6];

//Check Selected Cylenders
if(sel_cylenders == csv_cylenders && cat_selected == valuecomp[4] && policyType_result == valuecomp[5]){
var tpl_price1 = valuecomp[7];
var tpl_price = tpl_price1.replace(/\,/g, "");
//console.log("tpl_price..."+tpl_price);

var cal_rate_tpl_unparsed = tpl_price;
var cal_rate_tpl = parseFloat(cal_rate_tpl_unparsed);

//Per Acc Cover Driver
cal_personal_accident_c_driver = valuecomp[9];

if(need_per_acc_cover_driver == "Yes"){
cal_rate_tpl = cal_rate_tpl + parseFloat(cal_personal_accident_c_driver);
}else{
cal_rate_tpl = cal_rate_tpl;
}

//console.log("pa_cover_passangers..."+pa_cover_passangers);

//Per Acc Cover Passengers
if(pa_cover_passangers >= 1){

if(pa_cover_passangers == 1){
cal_rate_tpl = cal_rate_tpl + parseFloat(valuecomp[10]);
}
else if(pa_cover_passangers == 2){
var passangers2 = parseFloat(valuecomp[10]) + parseFloat(valuecomp[10]);
cal_rate_tpl = parseFloat(cal_rate_tpl) + passangers2;
}
else if(pa_cover_passangers == 3){
var passangers2 = parseFloat(valuecomp[10]) + parseFloat(valuecomp[10]) + parseFloat(valuecomp[10]);
cal_rate_tpl = parseFloat(cal_rate_tpl) + passangers2;
}
else if(pa_cover_passangers == 4){
var passangers2 = parseFloat(valuecomp[10]) + parseFloat(valuecomp[10]) + parseFloat(valuecomp[10]) + parseFloat(valuecomp[10]);
cal_rate_tpl = parseFloat(cal_rate_tpl) + passangers2;
}
else if(pa_cover_passangers == 5){
var passangers2 = parseFloat(valuecomp[10]) + parseFloat(valuecomp[10]) + parseFloat(valuecomp[10]) + parseFloat(valuecomp[10]) + parseFloat(valuecomp[10]);
cal_rate_tpl = parseFloat(cal_rate_tpl) + passangers2;
}
else if(pa_cover_passangers == 6){
var passangers2 = parseFloat(valuecomp[10]) + parseFloat(valuecomp[10]) + parseFloat(valuecomp[10]) + parseFloat(valuecomp[10]) + parseFloat(valuecomp[10]) + parseFloat(valuecomp[10]);
cal_rate_tpl = parseFloat(cal_rate_tpl) + passangers2;
}
else if(pa_cover_passangers == 7){
var passangers2 = parseFloat(valuecomp[10]) + parseFloat(valuecomp[10]) + parseFloat(valuecomp[10]) + parseFloat(valuecomp[10]) + parseFloat(valuecomp[10]) + parseFloat(valuecomp[10]) + parseFloat(valuecomp[10]);
cal_rate_tpl = parseFloat(cal_rate_tpl) + passangers2;
}
else{
cal_rate_tpl = cal_rate_tpl;
}
}

////console.log("price_ Before disc.."+cal_rate_tpl);

// Apply Discounts
var discount_apply = valuecomp[2].replace(/\%/g, "");
var discount_apply2 = discount_apply / 100;
var discount_amount = parseFloat(discount_apply2) * cal_rate_tpl;
////console.log("discount_apply.."+discount_apply);
////console.log("discount_apply2.."+discount_apply2);
////console.log("discount_amount.."+discount_amount);
var price_afterdeduct = cal_rate_tpl - parseFloat(discount_amount);
//console.log("price_Before DeductB.."+price_afterdeductB);

//Adding vat ammount
// var apply_vatAmmount = valuecomp[8].replace(/\%/g, "");
// var percentage_toadd = (parseFloat(price_afterdeductB) / 100) * parseFloat(apply_vatAmmount);
// var price_afterdeductB = price_afterdeductB + parseFloat(percentage_toadd);
// var price_afterdeduct = price_afterdeductB.toFixed(2);

//console.log("price_after Deduct.."+price_afterdeduct);


all_companies.push({
compname: valuecomp[0],
tpl_class: valuecomp[1],
tpl_discounts: valuecomp[2],
exc_vehList: valuecomp[3],
tpl_vehCat: valuecomp[4],
tpl_policy: valuecomp[5],
tpl_cylender_cap: valuecomp[6],
tpl_price: cal_rate_tpl,
tpl_vat: valuecomp[8],
tpl_pac_driver: valuecomp[9],
tpl_pac_passanger: valuecomp[10],
tpl_tird_partyLimit: valuecomp[11],
tpl_tird_partydeath: valuecomp[12],
tpl_ambulance_cover: valuecomp[13],
tpl_road_assistance: valuecomp[14],
price_afterdisc1: price_afterdeduct,
price_beforedisc: cal_rate_tpl,

cart_btn: '<a value="'+price_afterdeduct+'" data1="'+price_afterdeduct+'#'+valuecomp[0]+'#'+valuecomp[1]+'#'+valuecomp[2]+'#'+valuecomp[3]+'#'+valuecomp[4]+'#'+valuecomp[5]+'#'+valuecomp[6]+'#'+valuecomp[7]+'#'+valuecomp[8]+'#'+valuecomp[9]+'#'+valuecomp[10]+'#'+valuecomp[11]+'#'+valuecomp[12]+'#'+valuecomp[13]+'#'+valuecomp[14]+'#'+valuecomp[15]+'" class="pricing-tables-widget-purchase-button add-tocartbtn button_1" href="#">Buy Now</a>'
});
}
});

return all_companies;
}

jQuery(".percentage").change(function() {
jQuery('#priceingtable_tpl').html('');
all_companies = get_companies();

console.log(all_companies);

if(all_companies){

var pa_cover_driver_check = localStorage.getItem("pa_cover_driver_check");
var pa_cover_driver_show;
var pa_cover_passangers_selected = localStorage.getItem("pa_cover_pass_check");
var companies_count = all_companies.length;

localStorage.setItem("comp_count",companies_count);
jQuery('#cars_count').html(companies_count);
////console.log("companies_count..."+companies_count);
jQuery.each( all_companies, function(g, companies1) {
var pa_cover_pass_amount;
//Checking PA Cover for Passangers
if(pa_cover_passangers_selected == 1){
pa_cover_pass_amount = 30;
}if(pa_cover_passangers_selected == 2){
pa_cover_pass_amount = 60;
}if(pa_cover_passangers_selected == 3){
pa_cover_pass_amount = 90;
}if(pa_cover_passangers_selected == 4){
pa_cover_pass_amount = 120;
}if(pa_cover_passangers_selected == 5){
pa_cover_pass_amount = 150;
}if(pa_cover_passangers_selected == 6){
pa_cover_pass_amount = 180;
}if(pa_cover_passangers_selected == 7){
pa_cover_pass_amount = 210;
}else if(pa_cover_passangers_selected == 0){
pa_cover_pass_amount = "No";
}

//Data set to email and policy detail
jQuery("#tpl_pacoverPassangerchk").html(pa_cover_pass_amount);
jQuery("#tpl_pacoverPassangerchk_e").html(pa_cover_pass_amount);

//Checking PA Cover for Driver
var pa_cover_driver_show;
var need_per_acc_cover_driver = localStorage.getItem("pa_cover_driver_check");
if(need_per_acc_cover_driver == "Yes"){
pa_cover_driver_show = companies1.tpl_pac_driver;
}else{
pa_cover_driver_show = "No";
}

//Checking PA Cover for Passangers
var pa_cover_passangers_show;
var need_per_acc_cover_passangers = localStorage.getItem("pa_cover_pass_check");
if(need_per_acc_cover_passangers >= 1){
pa_cover_passangers_show = pa_cover_pass_amount;
}else{
pa_cover_passangers_show = "No";
}

//console.log("pa_cover_passangers_show.."+pa_cover_passangers_show);

jQuery('#priceingtable_tpl').append('<div class="pricing-widget-wrapper mainrow"><div class="pricing-tables-widget pricing-tables-widget-default pricing-tables-widget-blue pricing-tables-widget-dark"><div class="grid-container"><div class="grid-row"><div class="grid-item item-lg-12 item-sm-6 item-xs-marginb30"><div class="grid-row grid-row-collapse"><div class="grid-item item-lg-3 item-xs-12 leftbar"><div class="pricing-tables-widget-header tablerighthead"><div id="companylogo"><p  id="companylogoinner"><span  class="'+companies1.compname.toString().toLowerCase()+'"></span></p></div><div class="pricing-tables-widget-purchase">'+companies1.compname+'</div><div id="compclass"><div class="pricing-tables-widget-purchase compclassname">'+companies1.tpl_class+'</div></div></div></div><div class="grid-item item-lg-6 item-xs-12"><div class="pricing-tables-widget-section"><div class="grid-row"><div class="grid-item item-lg-6 item-xs-12"><ul class="pricing-tables-widget-divider-res-section"><li><div class="pricing-tables-widget-tooltip pricing-tables-widget-top-left pricing-tables-widget-fixed" data-tooltip="Our Infrastructure is prepared for Instant Activation Services For a Better User Experience"></div><i class="icomoon-circle2 iconcircle"></i>Third Party Limit <span class="dataval"> '+companies1.tpl_tird_partyLimit+'</span></li><li id="chkeckthis11"><div class="pricing-tables-widget-tooltip pricing-tables-widget-top-left pricing-tables-widget-fixed" data-tooltip="With our Cloud Network you have Peace of Mind and can look at your business only"></div><i class="icomoon-circle2 iconcircle"></i>Ambulance Cover <span class="dataval"><i class="icomoon-checkmark-circle '+companies1.tpl_ambulance_cover.toString().toLowerCase()+'"></i></span></li><li><div class="pricing-tables-widget-tooltip pricing-tables-widget-top-left pricing-tables-widget-fixed" data-tooltip="One piece of Software amazing and Beautiful organized with user in Mind"></div><i class="icomoon-circle2 iconcircle"></i>Personal Accident Cover For Driver <span class="dataval"> '+pa_cover_driver_show+'</span></li><li></li></ul></div><div class="grid-item item-lg-6 item-xs-12"><ul><li><div class="pricing-tables-widget-tooltip pricing-tables-widget-top-left pricing-tables-widget-fixed" data-tooltip="Our Infrastructure is prepared for give good space in Services For a Better User Experience"></div><i class="icomoon-circle2 iconcircle"></i>Third Party Death / Body Injury <span class="dataval">'+companies1.tpl_tird_partydeath+'</span></li><li><div class="pricing-tables-widget-tooltip pricing-tables-widget-top-left pricing-tables-widget-fixed" data-tooltip="With our Cloud Network you have Peace of Mind and can look at your business only with this Bandwidth"></div><i class="icomoon-circle2 iconcircle"></i><span class="switchlabel">Road-Side Assistance <span class="dataval"><i class="icomoon-checkmark-circle '+companies1.tpl_road_assistance.toString().toLowerCase()+'"></i></span></li><li><div class="pricing-tables-widget-tooltip pricing-tables-widget-top-left pricing-tables-widget-fixed" data-tooltip="In this Package you have 100 Emails per Month"></div><i class="icomoon-circle2 iconcircle"></i>Personal Accident Cover For Passangers <span class="dataval">'+pa_cover_passangers_show+'</span></li><li></li></ul></div></div> </div></div><div class="grid-item item-lg-3 item-xs-12"><div class="pricing-tables-widget-footer ptablefooter"><p class="pricing-tables-widget-top-left rightdiscount">Save '+companies1.tpl_discounts+'</p><h3 class="item-xs-marginb0"><strike>'+companies1.tpl_price+'</strike></h3><p>Total Price Are</p><h1 class="item-xs-marginb0"><span class="dataval1">'+companies1.price_afterdisc1+'</span> AED</h1><div class="pricing-tables-widget-price">'+companies1.price_afterdisc1+' AED</div>'+companies1.cart_btn+'</div></div></div></div></div><div id="demo'+g+'" class="" style="display:none;"></div></div></div></div>');
});
}
});
});

</script>

<!-- Price Table Excludes
<p class="pricing-tables-widget-purchase seemoredetails">see more details<i class="icomoon-menu-open2 seemoredetailsicon"></i></p>
<span class="pricing-tables-widget-bold">100GB</span>
<span class="pricing-tables-widget-badges">New</span> -->
<script>

//jQuery(document).ready(function() {
jQuery(document).on('click', '.seemoretoggle', function () {
var content_toggle= jQuery(this).attr('href');
jQuery(content_toggle).toggle();
});

//Call to Capture PDF Function.
//Canvas code

/* 	function capture_precheckout(){
//var uniqid = '<?php echo uniqid(); ?>';
//jQuery("#btnSave").click(function() {
//Canvas to show.
var imgageData;
//console.log("Image render check");
html2canvas(jQuery("#pre_chkout_policy"), {
onrendered: function(canvas) {
var imgageData = canvas.toDataURL("image/png");
//jQuery("#img-out").append('<img src="'+imgageData+'">');
//console.log(imgageData);
localStorage.setItem("precheckout_image",imgageData);
}
});

var pre_checkimage_code = localStorage.getItem("precheckout_image");
//var pre_checkimage_code = imgageData;
//console.log("pre_checkimage_code..",pre_checkimage_code);
var data = {
'action': 			'my_ajax_rt_precheckout',
'ins_pdf_image' : 	pre_checkimage_code };

jQuery("#img-out").append('<img src="'+pre_checkimage_code+'">');
var ajax_url_custom = "http://insurancetopup.com/wp-admin/admin-ajax.php";

jQuery.post(ajax_url_custom, data, function(response) {
console.log(response);
});
} */



jQuery(document).on('click', '.add-tocartbtn', function () {

//capture_precheckout();
//capture_precheckout();
//setTimeout(capture_precheckout, 1000);

//Set form name TPL
localStorage.setItem("form_type",'tpl');

// your function here
var alldataclicked = jQuery(this).attr('data1');
//console.log("alldatacheck.."+alldataclicked);

var dataSelected = alldataclicked.split('#');
var price_final = dataSelected[0];

var third_prty_limit12 = dataSelected[12];
console.log(dataSelected);
console.log("third_prty_limit12.."+third_prty_limit12);

//Getting Selected Company and let it go

var selected_compny_data = dataSelected;
var price = 	dataSelected[0];
var compname =  dataSelected[1];
var gcc_spec =  dataSelected[2];
var vehicle_cat = dataSelected[3];
//var repair_cond = dataSelected[12];
var minimum_val = dataSelected[5];
var maximum_val = dataSelected[6];
var rate = dataSelected[7];
var minimum = dataSelected[8];
var dl_less_than_year = dataSelected[9];
var less_than_year = dataSelected[10];
var os_coverage = dataSelected[11];
var pa_cover_driver = dataSelected[13];
var pa_cover_passanger = dataSelected[14];
var rs_assistance = dataSelected[14];
var rent_car = dataSelected[16];
var excess_amount = dataSelected[16];
var third_prty_limit = dataSelected[12];
var em_med_expence2 = dataSelected[25];
var em_loss_of_per_belongings = dataSelected[28];

//User Details
var veh_email_address = jQuery('#email_address').val();
var veh_vehicle_brand = jQuery('#vehicle_brand').val();
var veh_model_year = jQuery('#model_year').val();
var veh_fullname = jQuery('#firstname').val();
var veh_dobirth = jQuery('#datepicker1').val();
var veh_nationality = jQuery('#nationality option:selected').val();
var veh_country_first_driving_lic = jQuery('#country_firstdl').val();
var veh_brandnew = jQuery('#fr3').val();
var veh_manyear1 = jQuery('#dor_date').val();
var veh_manyear2 = jQuery('#dor_month').val();
var veh_manyear3 = jQuery('#dor_year').val();
var veh_manufactured_year = veh_manyear1+'/'+veh_manyear2+'/'+veh_manyear3;
var veh_contact_num1 = jQuery('#contact_num1').val();
var veh_contact_num2 = jQuery('#contact_num2').val();
var veh_contact_numF = veh_contact_num1+veh_contact_num2;
var veh_model_detail = jQuery('#vehicle_model_detail').val();
var veh_reg_date1 = jQuery('#vehicle_reg_date1').val();
var veh_reg_date2 = jQuery('#vehicle_reg_date2').val();
var veh_reg_date3 = jQuery('#vehicle_reg_date3').val();
var veh_reg_dateF = veh_reg_date1+'/'+veh_reg_date2+'/'+veh_reg_date3;
var veh_city_ofreg = jQuery('#cityof_reg').val();
var veh_car_val = price;
var veh_policystart_date1 = jQuery('#vec_policy_date1').val();
var veh_policystart_date2 = jQuery('#vec_policy_date2').val();
var veh_policystart_date3 = jQuery('#vec_policy_date3').val();
var veh_policy_sdateF = veh_policystart_date1+'/'+veh_policystart_date2+'/'+veh_policystart_date3;
var veh_country_first_dl = jQuery('#country_firstdl').val();
var tpl_third_party_death = dataSelected[13];
var tpl_ambulance_cover = dataSelected[14];
var tpl_roadSide_assistance = dataSelected[15];
var sel_vehicle_cylender = dataSelected[7];
var sel_vehicle_type = dataSelected[6];

//console.log("sel_type"+dataSelected[6]);
//console.log("tpl_roadSide_assistance"+tpl_roadSide_assistance);

var years_uaedriving_lic = jQuery('#years_uaedrivinglicence option:selected').val();
var years_exp_dl_home = jQuery('#home_country_drivingexp option:selected').val();
var total_Drivingexperience = years_uaedriving_lic;
var tpl = 'tpl';

//Set data to stap 5

jQuery('#contact_num_email').html(veh_contact_numF);
jQuery('#comp_name').html(compname);
jQuery('#ins_value').html(veh_car_val);
jQuery('#sel_vehicleModel').html(veh_model_year);
jQuery('#sel_vehicleBrand').html(veh_vehicle_brand);
jQuery('#sel_vehicleModel_detail').html(veh_model_detail);
jQuery('#driv_name').html(veh_fullname);
jQuery('#driv_nationality').html(veh_nationality);
jQuery('#driv_drivingExp').html(total_Drivingexperience);
jQuery('#driv_FirstLicCountry').html(veh_country_first_driving_lic);
jQuery('#driv_DOB').html(veh_dobirth);
jQuery('#driv_email').html(veh_email_address);
jQuery('#car_tpl').html(third_prty_limit);
jQuery('#car_ExcessAmount').html(excess_amount);
jQuery('#car_EmergencyMedical').html(em_med_expence2);
jQuery('#rent_aCar_chkout').html(rent_car);
jQuery('#loss_per_belong_chkout').html(em_loss_of_per_belongings);
jQuery('#pa_cover_passangers').html(pa_cover_passanger);
jQuery('#outside_uae_cover').html(os_coverage);
jQuery('#ins_price').html(price);
jQuery('#ins_price1').html(price);
compname2 = compname.toLowerCase();
localStorage.setItem("compname2",compname2);
//jQuery('#company_logoname').addClass(compname2);
jQuery('#policy_startFrom').html(veh_policy_sdateF);
jQuery('#policy_end1').html(veh_policystart_date1);
jQuery('#policy_end2').html(veh_policystart_date2);
jQuery('#tpl_third_party_death_show').html(tpl_third_party_death);
jQuery('#tpl_third_party_limit').html(third_prty_limit);
jQuery('#sel_vehicle_cylender').html(sel_vehicle_cylender);
jQuery('#sel_vehicle_type').html(sel_vehicle_type);

var next_year = parseInt(veh_policystart_date3) + 1;
jQuery('#policy_end3').html(next_year);
var tpl_ambulance_coverLow = tpl_ambulance_cover.toLowerCase();
jQuery('#tpl_amb_cover').addClass(tpl_ambulance_coverLow);
var tpl_roadSide_assistanceLow = tpl_roadSide_assistance.toLowerCase();
jQuery('#tpl_road_assistance').addClass(tpl_roadSide_assistanceLow);

//Set data to Email Template
jQuery('#comp_name_e').html(compname);
jQuery('#driv_name_e').html(veh_fullname);
jQuery('#sel_vehicleModel_e').html(veh_model_detail);
jQuery('#contact_num_email').html(veh_contact_numF);
jQuery('#driv_drivingExp_e').html(total_Drivingexperience);
jQuery('#policy_startFrom_e').html(veh_policy_sdateF);
//jQuery('#tpl_pacoverDriverchk_e').html(pa_cover_driver);
jQuery('#tpl_third_party_limit_e').html(third_prty_limit);
//jQuery('#tpl_pacoverPassangerchk_e').html(pa_cover_passanger);
jQuery('#tpl_third_party_death_show_e').html(tpl_third_party_death);
jQuery('#ambulance_cover_e').html(tpl_ambulance_cover);
jQuery('#roadside_assistance_e').html(rs_assistance);


//jQuery('#ins_discount').html(discount_applied);

//Apply Vat on Price
var disc_price = parseFloat(price) * 0.05;
var vat_added_amount = parseFloat(price) + disc_price;
//console.log("vat_added_amount.."+vat_added_amount);

localStorage.setItem("final_price_cart1",vat_added_amount);
jQuery('#ins_total_after_vat').html(vat_added_amount);

what_included();

var data = {

'action': 			'my_ajax_rt',
'ins_price' : 	price,
'ins_compname' :  compname,
'ins_discount' :  gcc_spec,
'ins_vehicle_cat' : vehicle_cat,
//'ins_repair_cond' : repair_cond,
'ins_minimum_val' : minimum_val,
'ins_maximum_val' : maximum_val,
'ins_rate' : rate,
'ins_minimum' : minimum,
'ins_dl_less_than_year' : dl_less_than_year,
'ins_less_than_year' : less_than_year,
'ins_os_coverage' : os_coverage,
'ins_pa_cover_driver' : pa_cover_driver,
'ins_cover_passanger' : pa_cover_passanger,
'ins_rent_car' : rent_car,
'ins_excess_amount' : excess_amount,
'ins_third_prty_limit' : third_prty_limit,
'ins_emergency_med_exp' : em_med_expence2,
'ins_loss_of_per_belongings' : em_loss_of_per_belongings,
'tpl_ambulanceCover' : tpl_ambulance_cover,
'tpl_roadSide_assistance' : tpl_roadSide_assistance,
'tpl_third_party_death' : tpl_third_party_death,

'ins_total_drivexp' : total_Drivingexperience,
'ins_fullname' : veh_fullname,
'ins_vehicle_brand' : veh_vehicle_brand,
'ins_veh_model_year' : veh_model_year,
'ins_contact_num' : veh_contact_numF,
'ins_veh_man_year' : veh_manufactured_year,
'ins_emailaddress' : veh_email_address,
'ins_nationality_sel' : veh_nationality,
'ins_brand_new' : veh_brandnew,
'ins_model_detail' : veh_model_detail,
'ins_vehiclereg_date' : veh_reg_dateF,
'ins_cityof_reg' : veh_city_ofreg,
'ins_car_value' : veh_car_val,
'ins_policy_startdate' : veh_policy_sdateF,
'ins_country_first_dl' : veh_country_first_dl,
'ins_cntry_fir_driv_lic' : veh_country_first_driving_lic,
'ins_dob_date' : veh_dobirth,
'ins_selected_compny_data' : selected_compny_data,
'form_type' : tpl
};

var ajax_url_custom = "https://insurancetopup.com/wp-admin/admin-ajax.php";

jQuery.post(ajax_url_custom, data,   function(response) {
console.log(response);
});


showEstimatedFareHtml(price_final);
document.getElementById("stern_taxi_fare_estimated_fare").value =  price_final;
localStorage.setItem("finalPrice", price_final);

//Show pre checkot
jQuery( "#step5" ).show();
jQuery( "#bread_crums_custom" ).show();
jQuery( "#step4" ).hide();
//jQuery( ".smart-wrap" ).hide();
jQuery( ".form_desc" ).hide();
});
// global variable
//jQuery("#rt_react").on('click', function () {
var imgageData;
function capture_precheckout(){
var getCanvas;
//console.log("Image render check");
/* html2canvas(document.querySelector("#pre_chkout_policy")).then(function(canvas) {
getCanvas = canvas;

var imgageData = getCanvas.toDataURL("image/png");
// jQuery("#img-out").append('<img src="'+imgageData+'">');
localStorage.setItem("precheckout_image",imgageData);
}); */
//});
//var imgageData1 = localStorage.getItem("precheckout_image");

var html_email = jQuery( "#email_container" ).html();
var data = {
'action': 			'my_ajax_rt_precheckout',
'ins_pdf_image' : 	html_email };
//jQuery("#img-out1").append(html_email);
var ajax_url_custom = "https://insurancetopup.com/wp-admin/admin-ajax.php";

jQuery.post(ajax_url_custom, data, function(response) {
console.log(response);
});

}
jQuery(document).on('click', '#proceed_checkout', function () {
capture_precheckout();
checkout_url_function();
//setTimeout(capture_precheckout, 1000);
//
});

//checkout_url_function();
//});
</script>
<!-- Old Calculater -->

<form  id="stern_taxi_fare_div" method="post">
<div  style="@display:none;">
<input type="hidden"  name="stern_taxi_fare_estimated_fare" id="stern_taxi_fare_estimated_fare" value=""/><div class="row">
</div>
</div>
</form>

</div>



<div class="col-lg-3 form_desc" >
<div class="desc_icon"><img class="form_descicon" src="https://insurancetopup.com//xio_outww2/insurance-quote-icon.png">
</div>
<p class="form_descright">Can't Find What You Need? Not sure about something? Please Contact us at
<a href="mailto:hello@insurancetopup.com">hello@insurancetopup.com</a>, or <br>Call Us at +971 65661505 We'll be able to pull up your quote and help you get the coverage that's right for you</p>
</div>

</div>


<!--
<script type="text/javascript" src="http://doptiq.com/smart-forms/demos/samples/elegant/js/jquery.formShowHide.min.js"></script>
-->
<script type="text/javascript">
jQuery(document).ready(function(jQuery){

/* jQuery(function(){
jQuery('.smartfm-ctrl').formShowHide();
}); */

/* @normal masking rules */

jQuery.mask.definitions['f'] = "[A-Fa-f0-9]";

jQuery("#contact_num2").mask('9999999', {placeholder:'X'});
//jQuery("#datepicker1").mask('99-99-9999', {placeholder:'_'});
//jQuery("#firstname").mask('Mustafa Jamal', {placeholder:'X'});
jQuery('#firstname').bind('keyup blur',function(){
var node = jQuery(this);
node.val(node.val().replace(/^[0-9]*$/,'') );
});
//jQuery("#firstname").mask('aaaaaa-aaaaaa', {placeholder:'X'});

//Showpopups on Select.
jQuery('#rent_a_car_id input').on('change', function() {
var rent_a_car_val = jQuery('input[name=veh_replacement_chk]:checked').val();
if(rent_a_car_val == 'Yes' ){
jQuery( "#popup_infomain" ).show();
//alert(" You Have choosed rental car option. So... 200 AED will be added ");
//////console.log("Rent Car Yes");
}
});

//Show Brand New Popup
jQuery('#veh_brand_new_id input').on('change', function() {
var brand_newchk = jQuery('input[name=new_check]:checked').val();
if(brand_newchk == 'Yes' ){
//console.log("Brand New Yes");
jQuery( "#popup_infomain_brand_new" ).show();
}
});

//Current Insurence Not Expired alert
jQuery('#my_current_ins_not_exp input').on('change', function() {
var current_ins_notexp_val = jQuery('input[name=mycurrentins_chk]:checked').val();
if(current_ins_notexp_val == 'No' ){
jQuery( "#popup_current_ins_expired" ).show();
}
});

//My Vehicle is not for Commercial Purpose
jQuery('#veh_not_forcommercial input').on('change', function() {
var veh_not_forcommercial = jQuery('input[name=vehiclecommercial_chk]:checked').val();
if(veh_not_forcommercial == 'No' ){
jQuery( "#vehicle_notfor_commercialpurpose" ).show();
}
});

//Outside UAE Coverage
jQuery('#outside_uae_coverage_id input').on('change', function() {
var outside_uae_coverage = jQuery('input[name=outsideuaeCover_chk]:checked').val();
if(outside_uae_coverage == 'Yes' ){
jQuery( "#popup_outsideuae_cover" ).show();
}
});

//Model
/*
jQuery('[data-smartmodal-close]').on('click', function(e)  {
e.preventDefault();
var smartInactiveModal = jQuery(this).attr('data-smartmodal-close');
jQuery(smartInactiveModal).removeClass('smartforms-modal-visible');
jQuery('body').removeClass('smartforms-modal-scroll');
});
});
*/

// get the mPopup
/* 	var mpopup = document.getElementById('mpopupBox');
var close = document.getElementById('close-popup');
*/
// close the mPopup when user clicks outside of the box
/* 	window.onclick = function(event) {
if (event.target == mpopup) {
mpopup.style.display = "none";
}
} */
//Personnal Account Cover Driver Price Table Check
//jQuery('#pa_cover_driver_chekcbox0').on('change', 'input', function(){
//alert('hello');
//});

jQuery('#pa_cover_driver_chekcbox0 input').on('change', function() {
var pa_cover_driver_chekcbox_val = jQuery('input[name=pac_driver0]:checked').val();
////console.log("pa_cover_driver_chekcbox_val.."+pa_cover_driver_chekcbox_val);
});
});

</script>

<style>

/* mPopup box style */
.mpopup {
display: none;
position: fixed;
z-index: 1;
padding-top: 100px;
left: 0;
top: 0;
width: 100%;
height: 100%;
overflow: auto;
background-color: rgb(0,0,0);
background-color: rgba(0,0,0,0.4);
}
.mpopup-content {
position: relative;
background-color: #fefefe;
margin: auto;
padding: 0;
width: 60%;
box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19);
-webkit-animation-name: animatetop;
-webkit-animation-duration: 0.4s;
animation-name: animatetop;
animation-duration: 0.4s
}
.mpopup-head {
padding: 2px 16px;
background-color: #ff0000;
color: white;
}
.mpopup-main {padding: 2px 16px;}
.mpopup-foot {
padding: 2px 16px;
background-color: #ff0000;
color: #ffffff;
}
/* add animation effects */
@-webkit-keyframes animatetop {
from {top:-300px; opacity:0}
to {top:0; opacity:1}
}
@keyframes animatetop {
from {top:-300px; opacity:0}
to {top:0; opacity:1}
}
/* close button style */
.close {
color: white;
float: right;
font-size: 28px;
font-weight: bold;
}
.close:hover, .close:focus {
color: #000;
text-decoration: none;
cursor: pointer;
}
</style>

<!-- Model Start -->

<div id="popup_pa_coverDriver" style="display:none;">
<input class="bunta-switcher" id="bunta-success" type="checkbox">
<div class="bunta" data-bunta-style="dos" data-bunta-theme="success">
<label class="bunta-close" for="bunta-success">&times;</label>
<label class="bunta-overlay" for="bunta-success" data-bunta-opacity="40"></label>
<div class="bunta-container">
<div class="bunta-center">
<h3 class="bunta-heading-icon"><i class="fa fa-info-circle"></i></h3>
<h2 class="bunta-heading text1">That's Good job!<i class="fa fa-thumbs-up"></i></h2>
<h4 class="bunta-heading text1"><p></p>
I Need Personal Accident Cover for Driver</h4>
<p>You have chosen Personal Accident Cover for Driver Option. So Amount  will be added in Quote</p>
<label class="bunta-button bunta-circle" id="pa_coverDriver_ok" for="bunta-success" data-bunta-theme="white"> - OK - </label>
</div>
</div>
</div>
</div>


<div id="popup_dangerPolicyStartDate" style="display:none;">
<input class="bunta-switcher" id="bunta-danger" type="checkbox">
<div id="mpopupBox" class="bunta" data-bunta-style="dos" data-bunta-theme="danger">
<label class="bunta-close" for="bunta-danger">&times;</label>
<label class="bunta-overlay" for="bunta-danger" data-bunta-opacity="40"></label>
<div class="bunta-container" id="banta_cont">
<div class="bunta-center">
<h3 class="bunta-heading-icon"><i class="fa fa-calendar"></i></h3>
<h1 class="bunta-heading text1">Oops!</h1>
<h2 class="bunta-heading text1">You have Selected Policy Start Date more than 60 Days.</h2>
<p class="text1 text2">Please! choose date range within under 60 days...</p>
<label class="bunta-button bunta-circle" id="dangerPolicy_ok" for="bunta-danger" data-bunta-theme="white"> - OK - </label>
</div>
</div>
</div>
</div>


<div id="popup_dangermain111" style="display:none;">
<input class="bunta-switcher" id="bunta-danger" type="checkbox">
<div id="mpopupBox" class="bunta" data-bunta-style="dos" data-bunta-theme="danger">
<label class="bunta-close" for="bunta-danger">&times;</label>
<label class="bunta-overlay" for="bunta-danger" data-bunta-opacity="40"></label>
<div class="bunta-container" id="banta_cont">
<div class="bunta-center">
<h3 class="bunta-heading-icon"><i class="fa fa-id-card"></i></h3>
<h1 class="bunta-heading text1">Oops!</h1>
<h2 class="bunta-heading text1">Your Age is Less then 21 Years</h2>
<p class="text1 text2">Dear Valued Customer, Your age is less then 21 years, so please call us and talk to one of our Agents.
We'll be able to pull up your quote and help you get the coverage that's right for you.<br>
It doesn’t matter if you are 18 or 80, there are always ways for you to save on auto insurance.</p>
<h3 class="bunta-heading text1"><i class="fa fa-phone"></i> Call Now @ +971 65661505</h3>
<label class="bunta-button bunta-circle" id="danger_ok" for="bunta-danger" data-bunta-theme="white"> - OK - </label>
</div>
</div>
</div>
</div>


<!-- Model End -->
<!-- Pricing Table -->
<!-- FONT LINK -->
<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,600,600italic,700,700italic,800,800italic|Source+Sans+Pro:400,300,300italic,400italic,600,600italic,700,700italic,900,900italic|Alegreya+Sans:400,300,300italic,400italic,500,500italic,700,700italic,800,800italic,900,900italic|Poiret+One|Dosis:400,500,300,600,700,800|Lobster+Two:400,400italic,700,700italic|Raleway:400,300,300italic,400italic,500,500italic,600,600italic,700,700italic,800,800italic,900,900italic|Roboto+Condensed:400,300italic,300,400italic,700,700italic|Roboto:400,300,300italic,400italic,500,500italic,700,700italic,900,900italic|Courgette">


<div id="step4">
<div id="user_inforow">
<!-- <h2 class="user_info"><?php //global $current_user; get_currentuserinfo(); echo 'Dear, ' . $current_user->user_login . ""; ?></h2> -->
<h2 class="user_info">Dear: <span id="user_filled"></span></h2>
<h2 class="user_details">We have Found <span id="cars_count"></span> car isurance qoutes for your <span id="cars_brand"></span> <span id="cars_model"></span></h2>
<p class="user_tooltip"><i class="icomoon-info2"></i>Tool tip appear on all title (for detailed info), and for more info about policy expand the table please.</p>
</div>
<div id="priceingtable_tpl"> </div>
</div>

<?php  ?>

<div id="step5">
<?
//Get data from Ajax
$ins_price_sel = $_REQUEST['ins_price'];
$ins_compname_sel = $_REQUEST['ins_compname'];
$ins_discount_sel = $_REQUEST['ins_gcc_spec'];
$ins_vehicle_cat = $_REQUEST['ins_vehicle_cat'];
$ins_repair_cond_sel = $_REQUEST['ins_repair_cond'];
$ins_minimum_val = $_REQUEST['ins_minimum_val'];
$ins_maximum_val = $_REQUEST['ins_maximum_val'];
$ins_rate = $_REQUEST['ins_rate'];
$ins_minimum = $_REQUEST['ins_minimum'];
$ins_dl_less_than_year_sel = $_REQUEST['ins_dl_less_than_year'];
$ins_less_than_year_sel = $_REQUEST['ins_less_than_year'];
$ins_os_coverage_sel = $_REQUEST['ins_os_coverage'];
$ins_pa_cover_driver1 = $_REQUEST['ins_pa_cover_driver'];
$ins_papp = $_REQUEST['ins_papp'];
$ins_rs_assistance = $_REQUEST['ins_rs_assistance'];
$ins_rent_car = $_REQUEST['ins_rent_car'];
$ins_excess_amount = $_REQUEST['ins_excess_amount'];
$ins_third_prty_limit1 = $_REQUEST['ins_third_prty_limit'];
$ins_em_med_expense = $_REQUEST['ins_emergency_med_exp'];
$ins_loss_of_per_belongings_set = $_REQUEST['ins_loss_of_per_belongings'];
$ins_selected_compny_data = $_REQUEST['ins_selected_compny_data'];

//User Data
$ins_fullname = $_REQUEST['ins_fullname'];
$ins_veh_model_year = $_REQUEST['ins_veh_model_year'];
$ins_model_detail = $_REQUEST['ins_model_detail'];
$ins_contact_num = $_REQUEST['ins_contact_num'];
$ins_veh_man_year = $_REQUEST['ins_veh_man_year'];
$veh_email_address = $_REQUEST['ins_emailaddress'];
$veh_modelno = $_REQUEST['ins_vehicle_brand'];
$veh_reg_dateF = $_REQUEST['ins_vehiclereg_date'];
$veh_city_ofreg = $_REQUEST['ins_cityof_reg'];
$ins_car_val = $_REQUEST['ins_car_value'];
$veh_policy_sdateF = $_REQUEST['ins_policy_startdate'];
$veh_country_first_dl = $_REQUEST['ins_country_first_dl'];
$veh_dob_date = $_REQUEST['ins_dob_date'];
$ins_total_drivexp = $_REQUEST['ins_total_drivexp'];

//Selected Data
$veh_brandnewcheck = $_REQUEST['veh_brandnewcheck'];
$current_ins_exp = $_REQUEST['current_ins_exp'];
$current_policy_comp_ins = $_REQUEST['current_policy_comp_ins'];
$cur_policy_agency_repair = $_REQUEST['cur_policy_agency_repair'];
$vehicle_is_gcc_spec = $_REQUEST['vehicle_is_gcc_spec'];
$not_for_commercial_purpose = $_REQUEST['not_for_commercial_purpose'];
$reg_under_myname = $_REQUEST['reg_under_myname'];
$claim_in_last12months = $_REQUEST['claim_in_last12months'];
$excess_amountpay = $_REQUEST['excess_amountpay'];
$need_veh_replacement = $_REQUEST['need_veh_replacement'];
$need_per_acc_cover = $_REQUEST['need_per_acc_cover'];
$need_out_uae_cover = $_REQUEST['need_out_uae_cover'];
$ins_cntry_fir_driv_lic = $_REQUEST['ins_cntry_fir_driv_lic'];
$ins_nationality_aj1 = $_REQUEST['ins_nationality_sel'];
$ins_form_type = $_REQUEST['form_type'];
$tpl_ambulanceCover_sess = $_REQUEST['tpl_ambulanceCover'];
$tpl_roadSide_assistance_sess = $_REQUEST['tpl_roadSide_assistance'];
$tpl_third_party_death_sess = $_REQUEST['tpl_third_party_death'];
?>

<div class="smart-wrap">
<div class="smart-forms smart-container wrap-4">

<form method="post" action="" id="form-ui22">
<div class="form-body">

<div class="frm-row main">

<!-- main Content left-->
<div id="pre_chkout_policy" class="section colm colm8 main-colm7">
<div class="frm-row">
<div class="qp_box01 section colm colm3">
<div id="companylogo">
<p id="companylogoinner">
<span id="company_logoname"></span>
</p>
</div>
</div>
<?php $current_date = date("d/m/Y"); ?>
<div class="qp_box09 section colm colm6">
<div class="qp_box09_inner091 boxtext02 section"><span id="tpl_name">TPL</span> Insurance Premium By<br><span id="comp_name"></span></div>
<div class="qp_box09_inner091 boxtext02">Policy Start From: <span id="policy_startFrom"></span> -to- <span id="policy_end1"></span>/<span id="policy_end2"></span>/<span id="policy_end3"></span>

<?php
/* $sel_date_array = explode("/", $veh_policy_sdateF_sel);
$sel_date_array[0];
$sel_date_array[1] = $sel_date_array[1] + 1;
$sel_date_array[2] = $sel_date_array[2] + 1;
echo "$sel_date_array[0]/$sel_date_array[1]/$sel_date_array[2]"; */
?></div>
<div class="qp_box09_inner091 boxtext02">Your Premium is: <span id="ins_price"></span> AED - Excluding VAT</div>
</div>

<div class="qp_box03 section colm colm2">
<div class="qp_box09_inner091 boxtext03 section">Quote Ref. No.<br><?php $ins_compname_sel1 = mt_rand(562222,1000000);
echo "Topup-".$ins_compname_sel1; ?></div>
<div class="qp_box09_inner091 boxtext03">Date:<br><?php echo $current_date; ?></div>
</div>
</div>


<div class="frm-row">
<div class="qp_box05 section">
<div class="frm-row">
<div class="qp_box05_inner051 boxtext01 section colm colm3">Your Vehicle Model is:</div>
<div class="qp_box05_inner051 boxtext02 section colm colm8"><span id="sel_vehicleModel"></span> <span id="sel_vehicleYear"></span> <span id="sel_vehicleBrand"></span> <span id="sel_vehicleModel_detail"></span> -- <span id="sel_vehicle_cylender"></span> -- <span id="sel_vehicle_type"></span>
</div>
</div>
</div>

<div class="frm-row-x">
<div class="qp_H2Box section">------ Driver info as per Policy Requirements ------</div>
</div>
<?php //$ins_nationality_aj_show ?>
<script>
//jQuery(document).ready(function() {
//var veh_nationality = localStorage.getItem("veh_nationality");
//////console.log("veh_nationality.Chkout.."+veh_nationality);
//jQuery( "#chkout_nationality" ).html(veh_nationality);
//});
</script>

<div class="frm-row">
<div class="qp_box08 section">
<div class="frm-row">
<div class="qp_box08_inner081 boxtext01 section colm colm3">Your Name is:</div>
<div class="qp_box08_inner081 boxtext02 section colm colm8"><span id="driv_name"></span></div>
<div class="qp_box08_inner081 boxtext01 section colm colm3">Your Nationality is:</div>
<div class="qp_box08_inner081 boxtext02 section colm colm8"><span id="driv_nationality"></span></div>
<div class="qp_box08_inner081 boxtext01 section colm colm3">Your Driving Experience:</div>
<div class="qp_box08_inner081 boxtext02 section colm colm8"><span id="driv_drivingExp"></span></div>
<div class="qp_box08_inner081 boxtext01 section colm colm3">First Licence Country is:</div>
<div class="qp_box08_inner081 boxtext02 section colm colm8"><span id="driv_FirstLicCountry"></span></div>
<div class="qp_box08_inner081 boxtext01 section colm colm3">Date of Birth:</div>
<div class="qp_box08_inner081 boxtext02 section colm colm8"><span id="driv_DOB"></span></div>
<div class="qp_box08_inner081 boxtext01 section colm colm3">Your Email is:</div>
<div class="qp_box08_inner081 boxtext02 section colm colm8"><span id="driv_email"></span></div>
</div>
</div>
</div>

<script>
jQuery(document).ready(function(jQuery){

var rent_car_val_get_chkout = localStorage.getItem("rent_car_check_val");
var pa_cover_driver_checkout = localStorage.getItem("pa_cover_driver_check");
var pa_cover_pass_checkout = localStorage.getItem("pa_cover_pass_check");
var pa_cover_driver_chk = localStorage.getItem("pacover_driver_checkout");
var pa_coverpassangersChkout = localStorage.getItem("pa_coverpassangersChkout");
var car_repair_chk1 = localStorage.getItem("garrage_repair");
var car_repair_chk2 = localStorage.getItem("premier_workshop");
var car_repair_chk3 = localStorage.getItem("inside_agency");

if(car_repair_chk1 == "Garage Repair"){
jQuery("#car_repairin").append('Garrage Repair');
}
if(car_repair_chk2 == "Premier Workshop"){
jQuery("#car_repairin").append(' - Premier Workshop');
}
if(car_repair_chk3 == " - Inside Agency"){
jQuery("#car_repairin").append('Inside Agency');
}

if(pa_cover_driver_chk != "NULL"){
jQuery("#pa_cover_driver_chkout").append('120 AED');
}
if(pa_cover_driver_chk == "NULL"){
jQuery("#pa_cover_driver_chkout").append('NULL');
}

if(pa_coverpassangersChkout >= 1){
jQuery("#pa_cover_passangers").html(pa_coverpassangersChkout);
}else{
jQuery("#pa_cover_passangers").html("NULL");
}




////console.log("pa_cover_driver_checkout.."+pa_cover_driver_checkout);
////console.log("pa_cover_pass_checkout.."+pa_cover_pass_checkout);

var form_type_set = localStorage.getItem("form_type");

if(form_type_set == 'tpl'){
jQuery("#ins").hide();
jQuery("#icluded").hide();
jQuery("#tpl").show();
jQuery("#tpl_name").show();
jQuery("#tpl_insuredVal").hide();
////console.log("hide ins");
}else {
jQuery("#tpl").hide();
jQuery("#ins").show();
jQuery("#tpl_name").hide();
jQuery("#tpl_insuredVal").show();
////console.log("hide tpl");
}

/* 	jQuery("ul #outside_uae_cover_chkout").hide();
jQuery("ul #rent_aCar_chkout").hide();
jQuery("ul #pa_cover_driver_checkout").hide();

if(rent_car_val_get_chkout == "Yes"){
jQuery("#rent_aCar_chkout").show();
}else{
jQuery("#rent_aCar_chkout").hide();
} */

});
</script>
<?php
//if($ins_form_type_session_show != 'tpl'){ ?>
<!--
<div id="ins">
<div class="frm-row-x">
<div class="qp_H2Box section">---- Full Coverage Premium Details ----</div>
</div>

<div class="frm-row">
<div class="qp_box06 section">
<div class="frm-row">
<div class="qp_box06_inner061 boxtext05 section">Policy Feature Summary</div>
<div class="qp_box06_inner061 boxtext02 section colm colm6">Car Repair in: <span id="car_repairin"></span></div>
<div class="qp_box06_inner061 boxtext02 section colm colm5">Third Party Limit: <span id="car_tpl"></span></div>
<div class="qp_box06_inner061 boxtext02 section colm colm6">Personal Accident Cover for Driver: <span id="pa_cover_driver_chkout"></span></div>
<div class="qp_box06_inner061 boxtext02 section colm colm5">Excess/Own Damage Cover: <span id="car_ExcessAmount"></span></div>
<div class="qp_box06_inner061 boxtext02 section colm colm6">Emergency Medical Expenses: <span id="car_EmergencyMedical"></span></div>
<div class="qp_box06_inner061 boxtext02 section colm colm5">Rant a Car: <span id="rent_aCar_chkout"></span> </div>
<div class="qp_box06_inner061 boxtext02 section colm colm6">Loss of Personal Belongings: <span id="loss_per_belong_chkout"></span></div>
<div class="qp_box06_inner061 boxtext02 section colm colm5">Additional Cover for Passangers: <span id="pa_cover_passangers"></span></div>
<div class="qp_box06_inner061 boxtext02 section colm colm6">Outside UAE Coverage: <span id="outside_uae_cover"></span></div>
</div>
</div>
</div>
</div>-->

<?php// }
// else if($ins_form_type_session_show == 'tpl'){ ?>

<div id="tpl">
<div class="frm-row">
<div class="qp_H2Box section">------ TPL Policy Cover Details ------</div>
</div>

<div class="frm-row">
<div class="qp_box06 section">
<div class="frm-row">
<div class="qp_box06_inner061 boxtext05 section">Third Party Only Feature Summary</div>
<div class="qp_box06_inner061 boxtext02 section colm colm6">Third Party Limit is: <span id="tpl_third_party_limit"></span></div>
<div class="qp_box06_inner061 boxtext02 section colm colm5">Third Party Death Limit is: <span id="tpl_third_party_death_show"></span></div>
<div class="qp_box06_inner061 boxtext02 section colm colm6">Ambulance Cover: <span class="dataval "><i id="tpl_amb_cover" class="icomoon-checkmark-circle"></i></span></div>
<div class="qp_box06_inner061 boxtext02 section colm colm5">Road Side Assistance: <span class="dataval "><i id="tpl_road_assistance" class="icomoon-checkmark-circle"></i></span></div>
<div class="qp_box06_inner061 boxtext02 section colm colm6">Personal Accident Cover for Driver: <span id="tpl_pacoverDriverchk"></span></div>
<div class="qp_box06_inner061 boxtext02 section colm colm5">Accident Cover for Passangers: <span id="tpl_pacoverPassangerchk"></span> Passangers</div>
</div>
</div>
</div>
</div>

<?php //} ?>

<script>
var label_array =
["Price",
"Company Name",
"Class",
"Discounts",
"Empty",
"Excluded Vehicles List",
"Vehicle Category",
"Min. Value",
"Max. Value",
"Rate",
"Minimum",
"No Claim Certificate",
"Self Declaration",
"Repair Condition",
"Personal Accident Cover for Driver",
"Accident Cover for Passengers",
"Rant a Car",
"Own Damage Cover",
"Out Side UAE Coverage",
"IF UAE License Age less than 1 Year",
"iF Age less than 25 year",
"Road-side Assistance",
"Third Party Limit",
"Windscreen Damage",
"Dent Repair",
"Off-Road & Desert Recovery",
"Emergency Medical Expenses",
"Ambulance Cover",
"Death or Bodily Injury",
"Loss of Personal Belongings",
"Fire and Theft Cover",
"Valet Parking Theft",
"Replacment of locks",
"Natural Calamity Cover",
"Car Registration / Renewal",
"Modified / Non-GCC Support"];
</script>

<div class="frm-row-x" id="icluded">
<div class="qp_box04 section">
<div class="frm-row">
<div class="qp_box04_inner041 boxtext05 section colm colm6">What's Included</div>
<div class="qp_box04_inner041x boxtext02 included section colm colm6">
<?php $all_sel_data = $ins_selected_compny_data_sel?>
<ul id="is_icludedul">
</ul>
<script>
function what_included(){
var compname3 = localStorage.getItem("compname2");
jQuery('#company_logoname').attr('class','');
jQuery('#company_logoname').addClass(compname3);

/* jQuery('#is_icludedul').html('');
var all_sel_data = JSON.parse(localStorage.getItem("dataSelected"));
////console.log("all_sel_data.."+all_sel_data);
var final_array =[];
var included =[];

jQuery.each( all_sel_data, function( i, val ) {
if(all_sel_data[i] == "TRUE" || all_sel_data[i] == "true" || all_sel_data[i] == "True"){
final_array[i] = label_array[i];
}
});
//console.log(final_array);
var final_array1 = final_array.filter(function(e){ return e.replace(/(\r\n|\n|\r)/gm,"")});

jQuery.each( final_array1, function( i, traverse ) {
//console.log(traverse);
jQuery('#is_icludedul').append('<li class="included">'+traverse+'</li>');
});
what_included1(); */
}
</script>
</div>
<div class="qp_box04_inner041 boxtext05 section">What's Not Included</div>
<div class="qp_box04_inner041x boxtext02 not_included section colm colm5">
<ul id="not_icludedul">
</ul>
<script>
function what_included1(){

jQuery('#not_icludedul').html('');
var all_sel_data = JSON.parse(localStorage.getItem("dataSelected"));
////console.log("all_sel_data.."+all_sel_data);
var final_array =[];
var included =[];

jQuery.each( all_sel_data, function( i, val ) {
if(all_sel_data[i] == "FALSE" || all_sel_data[i] == "false" || all_sel_data[i] == "False" && (all_sel_data[i] != "")){
final_array[i] = label_array[i];
}
});
//console.log(final_array);
var final_array1 = final_array.filter(function(e){ return e.replace(/(\r\n|\n|\r)/gm,"")});

jQuery.each( final_array1, function( i, traverse ) {
//console.log(traverse);
jQuery('#not_icludedul').append('<li class="not-included">'+traverse+'</li>');
});
}
</script>
</div>
</div>
</div>
</div>


<?php
$pdf_companyName = strtolower($ins_compname_sel);
//echo $pdf_companyName;
if($pdf_companyName == "qatar general insurance"){
$pdf_fileLink = 'https://insurancetopup.com/xio_outww2/Policy_PDF/Qatar_General_Insurance_Policy.pdf';
}
else if($pdf_companyName == "watania insurance"){
$pdf_fileLink = 'https://insurancetopup.com/xio_outww2/Policy_PDF/Watania_Insurance_Policy.pdf';
}
else if($pdf_companyName == "watania"){
$pdf_fileLink = 'https://insurancetopup.com/xio_outww2/Policy_PDF/Watania_Insurance_Policy.pdf';
}
else if($pdf_companyName == "insurance house"){
$pdf_fileLink = 'https://insurancetopup.com/xio_outww2/Policy_PDF/Insurance_House_Policy.pdf';
}
else if($pdf_companyName == "adamjee insurance"){
$pdf_fileLink = 'https://insurancetopup.com/xio_outww2/Policy_PDF/Adamjee_Insurance_Policy.pdf';
}
else if($pdf_companyName == "the oriental insurance"){
$pdf_fileLink = 'https://insurancetopup.com/xio_outww2/Policy_PDF/The_Oriental_Insurance_Policy.pdf';
}
else if($pdf_companyName == "tokio marine"){
$pdf_fileLink = 'https://insurancetopup.com/xio_outww2/Policy_PDF/Tokio_Marine_Policy.pdf';
}
else if($pdf_companyName == "dubai national insurance"){
$pdf_fileLink = 'https://insurancetopup.com/xio_outww2/Policy_PDF/Dubai_National_Insurance_Policy.pdf';
}

?>

<div class="frm-row">
<div class="qp_box02 boxtext02 section">View Policy Documents <a href="<?php echo $pdf_fileLink;?>">Insurance Company PDF File.</a></div>
</div>

<div class="frm-row">
<div class="qp_box02x boxtext06 section">
<div class="boxtext01 section">Disclaimer</div>
Dear Customer! You must tell us immediately if any of the information stated in your quotation is in-correct. Failure to do may invalidate your policy. Because no list of questions can be exhaustive, so please consider carefully whether there is any other material information known to you which could influence our acceptance and assessment of the risk.<br>
Please check whether the value indicated represents the correct market value of your vehicle. Proof may be requested. Rule of thumb is that the car depreciates 15% in value each year after purchase.</div>
</div>
</div></div><!-- end colm7 section -->
<!-- main Content Left -->

<!-- Sidebar right -->
<!-- <div class="frm-row">
<div class="section colm colm3 rightsidebar">
<input type="button" value="Proceed to Checkout" id="proceed_checkout" class="btn btn-primary" >
</div>
</div> -->
<!-- end Sidebar right -->

<!-- Sidebar right -->
<div class="frm-row">
<div class="section colm colm4 main-colmX">
<h3>Secure Checkout</h3>
<h5>Cart Totals</h5>
<!--<h2><span id="ins_price"></span> AED</h2>-->
<div class="base_price_block colm colm6">
<h5 class="main-colmX-txt">Subtotal:</h5>
<h5 class="main-colmX-txt">5% VAT:</h5>
<h5 class="main-colmX-txt">Total:</h5>

</div>
<div class="base_price_block colm colm6">
<h5 class="main-colmX-txt2"><span id="ins_price1"></span> AED</h5>
<h5 class="main-colmX-txt2"><span id="vat_applied">5%</span></h5>
<h5 class="main-colmX-txt2"><span id="ins_total_after_vat"></span> AED</h5>
</div>

<div id="proceed_to_checkout">
<input type="button" value="Proceed to Checkout" id="proceed_checkout" class="btn btn-primary" >
</div>
</div>
</div>

</div>
</div>
</form>

</div>
</div>
</div>

<!-- TPL Policy Email Template -->
<div id="email_container">

<div class="p_txt">
<p>Dear Valuable Customer,<br />
First of all, we would like to Thank you, for choosing your auto policy from our platform. Your order is on hold, because the payment for your car insurance policy is still pending. To make sure you’re covered as soon as possible, we will be in touch to arrange an address to pick up the cost (you can pay by Cash/Credit-Card to our courier guy) of your policy. You should expect a call within 20 mints time. in our business hours (Saturday to Thursday, 8:30AM – 8.00PM).<br />
Following documents are required for process your auto policy:
<ul>
<li>Your Vehicle Registration Copy</li>
<li>Your Driving Licence Copy</li>
<li>Your Emirates ID Copy</li>
<li>No Claims Certificate (if you have - optional)</li>
</ul>
Please send your all documents as soon as possible via email, and its mentioned below.</p>

<p class="small_txt">Please Note:<br />
Once we have collected all the relevant documents and payment confirmation on the above details (invoice), we will issue your chosen insurance policy cover, kindly acknowledge that sometimes vehicle value needs the approval. in some cases, issuance of the policy requires a vehicle survey, in that case you will be asked to provide the current dated photograph of your vehicle. Once all the requirements are completed we’ll issue your policy. Please note that some policies can only be approved during working hours.<br />
Also! The cover will only start and become effective once the original policy document has been issued.</p>
</div>


<div class="section group">
<div class="col span_2_of_2 ml_bgmail"><p>doc@insurancetopup.com</p></div>
</div>

<div style="text-align: center"> <h3>Third Party Only Policy Cover Information</h3> </div>

<div class="section group">
<div class="col span_2_of_2 ml_txt">
<ul>
<li>Insurance Premium By: <strong><span id="comp_name_e"></span></strong></li>
<li>Your Name is: <strong><span id="driv_name_e"></span></strong></li>
<li>Your Vehicle Model is: <strong><span id="sel_vehicleModel_e"></span></strong></li>
<li>Contact Number: <strong><span id="contact_num_email"></span></strong></li>
<li>Your Driving Experience is: <strong><span id="driv_drivingExp_e"></span></strong></li>
<li>Your Policy Start Date is: <strong><span id="policy_startFrom_e"></span></strong></li>
</ul></div>
</div>

<div class="section group"> <div class="col span_2_of_2 hr">.</div> </div>

<div class="section group">
<div class="col span_1_of_2 ml_txt">
<ul>
<li>Personal Accident Cover for Driver: <strong><span id="tpl_pacoverDriverchk_e"></span></strong></li>
<li>Third Party Limit is: <strong><span id="tpl_third_party_limit_e"></span></strong></li>
<li>Ambulance Cover: <strong><span id="ambulance_cover_e"></span></strong></li>
</ul></div>

<div class="col span_1_of_2 ml_txt">
<ul>
<li>Accident Cover for Passengers: <strong><span id="tpl_pacoverPassangerchk_e"></span></strong></li>
<li>Third Party Death Cover: <strong><span id="tpl_third_party_death_show_e"></span></strong></li>
<li>Road Side Assistance: <strong><span id="roadside_assistance_e"></span></strong></li>
</ul></div>
</div>

<p class="p_txt">And more policy features you will see in origanl policy cover when we realse you after all basic requirements by insurance company</p>

<div class="_">
<p class="small_txt2">Please confirm that all the information you’ve provided above is correct. You can do this by simply responding to this email saying ‘Yes i confirm’. And If the information above is incorrect, Please! talk to our insurance agent on this number +971-4-2485886</p>
<p class="p_txt">If you have any questions feel free to Contact Us.<br /><br />
Thank you Again,<br />
Insurance TopUp Team.</p>
</div>

</div>
<!-- Policy Email Template end -->

<?php
}
