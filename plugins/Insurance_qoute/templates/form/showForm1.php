<?php
function showForm1($atts) {
$args = array(
'post_type' => 'stern_taxi_car_type',
'nopaging' => true,
'order'     => 'ASC',
'orderby' => 'meta_value',
'meta_key' => '_stern_taxi_car_type_organizedBy'
);
$allTypeCars = get_posts( $args );
$args = array(
'post_type' => 'stern_listAddress',
'nopaging' => true,
'meta_query' => array(
array(
'key'     => 'typeListAddress',
'value'   => 'destination',
'compare' => '=',
),
array(
'key'     => 'isActive',
'value'   => 'true',
'compare' => '=',
),
),
);

$allListAddressesDestination = get_posts( $args );
$backgroundColor= get_option('stern_taxi_fare_bcolor');
if($backgroundColor!="")
{
$backgroundColor='background-color:'.stern_taxi_fare_hex2rgba($backgroundColor,get_option('stern_taxi_fare_bTransparency'));
}
global $woocommerce;
$currency_symbol = get_woocommerce_currency_symbol();
if (isBootstrapSelectEnabale()=="true")
{
$class = "selectpicker show-tick";
} else {
$class = "form-control";
}



$class_full_row12 = (get_option('stern_taxi_fare_full_row') != "true") ? "class='col-lg-12 col-md-12 col-sm-12 col-xs-12'" : "class='col-lg-12 col-md-12 col-sm-12 col-xs-12'";

$class_full_row6 = (get_option('stern_taxi_fare_full_row') != "true") ? "class='col-lg-6 col-md-6 col-sm-12 col-xs-12'" : "class='col-lg-12 col-md-12 col-sm-12 col-xs-12'";

$class_full_row3 = (get_option('stern_taxi_fare_full_row') != "true") ? "class='col-lg-3 col-md-3 col-sm-12 col-xs-12'" : "class='col-lg-12 col-md-12 col-sm-12 col-xs-12'";

$classMain = (get_option('stern_taxi_fare_form_full_row') != "true") ? "class='col-xs-12 col-sm-6 col-lg-6'" : "class='col-xs-12 col-sm-12 col-lg-12'";

//showDemoSettings($backgroundColor);

?>
<div class="container1 row">
<div class="col col-lg-9">
<!-- <div class="row"> -->
<!-- <div  <?php //echo $classMain; ?> id="main1" style="<?php //echo $backgroundColor; ?>;padding-bottom: 5px" > -->
<?php


	$form_dataset1_json = get_option( 'frontend_dataVinfo1' );
	$form_dataset2_json = get_option( 'frontend_dataVinfo2' );
	$form_dataset = array_merge($form_dataset1_json,$form_dataset2_json);


//$data_setfd = json_decode($form_dataset, true);

$form_dataComp = get_option( 'frontend_dataComp' );

//$data_setComp = json_decode($form_dataComp, true);

foreach ( $form_dataComp as $data_dataComp ) {

$veh_cat_2[] = $data_dataComp[2];

$repair_con[] = $data_dataComp[12];

}

foreach ( $form_dataset as $data_vbrand ) {

$name_arr[] = $data_vbrand[0];

$medel_year[] = $data_vbrand[1];

$modelno_submodel[] = $data_vbrand[2];

$vehicle_cat[] = $data_vbrand[5];

//$sub_model[]= $data_vbrand[4];

}
$vehicle_brand = array_unique($name_arr);
$veh_medel_year = array_unique($medel_year);
$veh_modelno = array_unique($modelno_submodel);
$veh_vehicle_cat = array_unique($vehicle_cat);
//$veh_sub_model = array_unique($sub_model);
$vehicle_cat_2 = array_unique($veh_cat_2);
$repair_condition = array_unique($repair_con);
?>

<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700" rel="stylesheet">
<link href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css" rel="stylesheet">

<div style="@display:none;" id="bread_crums_custom">
<div class="steps clearfix">
<ul id="tablist11">
<li class="first">
<a id="goto_form" ><span class="number">Vehicle Details</span></a>
</li>
<li class="second">
<a id="goto_table"><span class="number">Insurance Quote</span></a>
</li>
<!--<li class="second">
<a id="goto_checkout"><span class="number">2 Goto Checkout</span></a>
</li> -->
</ul>
</div>
</div>

<div id="form_main_div">
<div id="" class="smart-wrap" style="display:block !important;">
<div id="XXstep1" class="smart-forms smart-container wrap-1">
<div class="smart-steps steps-progress stp-three">
<form  id="smart-form" method="post">
<h2>Vehicle Information</h2>
<fieldset>
<!-- STEP 1 HTML -->

<div class="frm-row">
<div class="section colm colm6"><p class="step1_labels">Your Vehicle Brand Name is</p></div>
<div class="section colm colm6">
<label class="field select">
<select class="calculate1 percentage" id="vehicle_brand" name="vehicle_brand" required>
<option value="" >--- Select Auto Brand Name ---</option>
<?php
foreach ( $vehicle_brand as $data_vbrand ) { ?>
<option class="options" value="<?php echo $data_vbrand;?>"><?php echo $data_vbrand;?></option>
<?php } ?>
</select><i class="fa fa-chevron-down arrow1"></i>
<b class="tooltip tip-left-bottom"><em>Please Select Your Vehicle Brand</em></b>
</label>
</div>
</div>



<div class="frm-row" id="veh_brand_new_id">
<div class="section colm colm6"><p class="step1_labels">Is Your Vehicle Brand New?</p></div>
<div class="section colm colm3 percentage">
<label class="switch switch-round block option brand-new-yes">
<input type="radio" class="veh_brand_new" name="new_check" id="fr3" value="Yes" >
<span class="switch-label" data-on="YES" data-off="YES"></span>
<span>  </span>
</label>
</div>

<div class="section colm colm3 percentage">
<label class="switch switch-round block option brand-new-no">
<input type="radio" class="veh_brand_new" name="new_check" id="fr4" value="No" checked>
<span class="switch-label" data-on="NO" data-off="NO"></span>
<span>  </span>
</label>
</div>
</div>



<div id="dor_row_m" class="frm-row form_row-space">
<div class="section colm colm6"><p class="step1_labels">Your Vehicle Manufacture Year is</p></div>
<div class="section colm colm6"><div class="form-group">
<label class="field select">
<select class="calculate1 percentage" id="model_year" name="model_year" required>
<option value=""> --- Model Year --- </option>
<?php
foreach ( $veh_medel_year as $data_medel_year ) { ?>
<option value="<?php echo $data_medel_year;?>"><?php echo $data_medel_year;?></option>
<?php } ?>
</select><i class="fa fa-chevron-down arrow1"></i>
<b class="tooltip tip-right-top"><em>Please! Select Your Car Manufacture / Make Year.</em></b>
</label>
</div>
</div>
</div>



<div class="frm-row form_row-space">
<div class="section colm colm6"><p class="step1_labels">Your Vehicle Model is</p></div>
<div class="section colm colm6"><div class="form-group">
<label class="field select">
<select class="calculate percentage modeloption" id="vehicle_model_detail" name="vec_model_num" required>
<option value="0"> --- Model Details --- </option>
<?php
//$data_unique_mno = array_unique($data_setfd);
//foreach ( $veh_modelno as $data_modelNo ) { ?>
<!-- <option data-price="5.00" value="<?php// echo $data_modelNo;?>"><?php //echo $data_modelNo;?></option> -->
<?php // } ?>
</select><i class="fa fa-chevron-down arrow1"></i>
<b class="tooltip tip-right-top"><em>Select Your Vehicle Model</em></b>
</label>
</div>
</div>
</div>



<div id="dor_row" class="frm-row form_row-space">
<div class="section colm colm6"><p class="step2_labels">Vehicle First Registration Date is</p></div>
<div class="section colm colm6"><div class="form-group">

<span id="phone1">
<label class="field select">
<select class="calculate percentage ignore" id="vehicle_reg_date1" name="vehicle_reg_date" required>
<option value="">Date</option>
<option value="1">1</option>
<option value="2">2</option>
<option value="3">3</option>
<option value="4">4</option>
<option value="5">5</option>
<option value="6">6</option>
<option value="7">7</option>
<option value="8">8</option>
<option value="9">9</option>
<option value="10">10</option>
<option value="11">11</option>
<option value="12">12</option>
<option value="13">13</option>
<option value="14">14</option>
<option value="15">15</option>
<option value="16">16</option>
<option value="17">17</option>
<option value="18">18</option>
<option value="19">19</option>
<option value="20">20</option>
<option value="21">21</option>
<option value="22">22</option>
<option value="23">23</option>
<option value="24">24</option>
<option value="25">25</option>
<option value="26">26</option>
<option value="27">27</option>
<option value="28">28</option>
<option value="29">29</option>
<option value="30">30</option>
<option value="31">31</option>
</select><i class="fa fa-chevron-down arrow1"></i>
<b class="tooltip tip-right-top"><em>Select! First Registration Date</em></b>
</label>
</span>


<span id="month">
<label class="field select">
<select class="calculate percentage ignore" id="vehicle_reg_date1=2" name="dor_month" required>
<option value=""> Month </option>
<option value="1">January</option>
<option value="2">February</option>
<option value="3">March</option>
<option value="4">April</option>
<option value="5">May</option>
<option value="6">June</option>
<option value="7">July</option>
<option value="8">August</option>
<option value="9">September</option>
<option value="10">October</option>
<option value="11">November</option>
<option value="12">December</option>
</select><i class="fa fa-chevron-down arrow1"></i>
<b class="tooltip tip-right-top"><em>Select! First Registration Month</em></b>
</label>
</span>

<span id="phone1">
<label class="field select">
<select class="calculate percentage" id="vehicle_reg_date3" name="vehicle_reg_date3" required>
<option value=""> Year </option>
<option value="2018">2018</option>
<option value="2017">2017</option>
<option value="2016">2016</option>
<option value="2015">2015</option>
<option value="2014">2014</option>
<option value="2013">2013</option>
<option value="2012">2012</option>
<option value="2011">2011</option>
<option value="2010">2010</option>
<option value="2009">2009</option>
<option value="2008">2008</option>
</select><i class="fa fa-chevron-down arrow1"></i>
<b class="tooltip tip-right-top"><em>Select! First Registration Year</em></b>
</label>
</span>
</div>
</div>
</div>

<div class="frm-row form_row-space">
<div class="section colm colm6"><p class="step1_labels">Your Car Value is</p></div>
<div class="section colm colm6">
<label for="home_phone" class="field prepend-icon">
<input type="text" placeholder="Car Value" value="" class="gui-input percentage" id="vehicle_val" name="vehicle_val" required>
<b class="tooltip tip-right-top"><em>This is Your Car Value! But you can also write your vehicle value.</em></b>
<span class="field-icon"><i class="fa fa-car"></i></span>

<div style="@display:none;">
<p id="car_value_details" class="car_value_box">Dear! Your estimated car value is between <span id="item_minval">0.00</span> AED to <span id="item_maxval">0.00</span> AED and must be approved by the insurance company</p></div>
</label>
</div>
</div>

<div class="frm-row form_row-space">
<div class="section colm colm6"><p class="step2_labels">Your Vehicle Registration City is</p></div>
<div class="section colm colm6">
<label class="field select">
<select class="calculate percentage" id="cityof_reg" name="cityof_reg" required>
<option class="option" value="">--- Select City ---</option>
<option class="option" value="abu-dahbi">Abu Dahbi</option>
<option class="option" value="dubai">Dubai</option>
<option class="option" value="sharjah">Sharjah</option>
<option class="option" value="Al-Ain">Al Ain</option>
<option class="option" value="ajman">Ajman</option>
<option class="option" value="Ras-Al-Khaimah">Ras Al Khaimah</option>
<option class="option" value="Fujairah">Fujairah</option>
<option class="option" value="Umm-al-Quwain">Umm al-Quwain</option>
</select><i class="fa fa-chevron-down arrow1"></i>
<b class="tooltip tip-right-top"><em>Select! Vehicle Registration City</em></b>
</label>
</div>
</div>



<div class="frm-row form_row-space" style="display: none;">
<div class="section colm colm6"><p class="step2_labels">When would you like to Start Policy?</p></div>
<div class="section colm colm6"><div class="form-group">
<span id="phone1">
<label class="field select">
<select class="calculate percentage" id="vec_policy_date1" name="vec_policy_date" required>
<option value=""> Date </option>
<option value="1">1</option>
<option value="2">2</option>
<option value="3">3</option>
<option value="4">4</option>
<option value="5">5</option>
<option value="6">6</option>
<option value="7">7</option>
<option value="8">8</option>
<option value="9">9</option>
<option value="10">10</option>
<option value="11">11</option>
<option value="12">12</option>
<option value="13">13</option>
<option value="14">14</option>
<option value="15">15</option>
<option value="16">16</option>
<option value="17">17</option>
<option value="18">18</option>
<option value="19">19</option>
<option value="20">20</option>
<option value="21">21</option>
<option value="22">22</option>
<option value="23">23</option>
<option value="24">24</option>
<option value="25">25</option>
<option value="26">26</option>
<option value="27">27</option>
<option value="28">28</option>
<option value="29">29</option>
<option value="30">30</option>
<option value="31">31</option>
</select><i class="fa fa-chevron-down arrow1"></i>
<b class="tooltip tip-right-top"><em>Select! Policy Starting Date</em></b>
</label>
</span>



<span id="month">
<label class="field select">
<select class="calculate percentage" id="vec_policy_date2" name="vec_policy_month2" required>
<option value=""> Month </option>
<option value="1">January</option>
<option value="2">February</option>
<option value="3">March</option>
<option value="4">April</option>
<option value="5">May</option>
<option value="6">June</option>
<option value="7">July</option>
<option value="8">August</option>
<option value="9">September</option>
<option value="10">October</option>
<option value="11">November</option>
<option value="12">December</option>
</select><i class="fa fa-chevron-down arrow1"></i>
<b class="tooltip tip-right-top"><em>Select! Policy Starting Month</em></b>
</label>
</span>



<span id="phone1">
<label class="field select">
<select class="calculate percentage" id="vec_policy_date3" name="vec_policy_date2" required>
<option value=""> Year </option>
<option value="2018">2018</option>
</select><i class="fa fa-chevron-down arrow1"></i>
<b class="tooltip tip-right-top"><em>Select! Policy Starting Year 2018</em></b>
</label>
</span>

</div>
</div>
</div>

</fieldset>


<h2>Cover Information</h2>
<fieldset>
<!-- STEP 2 HTML MARKUP GOES HERE-->
<div class="frm-row" id="my_current_ins_not_exp">
<div class="section colm colm6"><p class="step1_labels">My Current Insurance is Not Expired</p></div>
<div class="section colm colm3  percentage">
<label class="switch switch-round block option">
<input type="radio" class="current_ins_exp" name="mycurrentins_chk" id="fr5" value="Yes" checked>
<span class="switch-label" data-on="YES" data-off="YES"></span>
<span>  </span>
<b class="tooltip"><em>My Current Insurance is Not Expired</em></b>
</label>
</div>

<div class="section colm colm3">
<label class="switch switch-round block option">
<input type="radio" class="current_ins_exp" name="mycurrentins_chk" id="fr6" value="No">
<span class="switch-label" data-on="NO" data-off="NO"></span>
<span>  </span>
<b class="tooltip"><em>My Current Insurance is Expired</em></b>
</label>
</div>
</div>



<div class="frm-row" id="mycurrentpolicy">
<div class="section colm colm6"><p class="step1_labels">My Current Policy is Comprehensive Insurance</p></div>
<div class="section colm colm3 percentage">
<label class="switch switch-round block option">
<input type="radio" class="current_policy_comp_ins" name="mycurrentpolicy_chk" id="fr7" value="Yes" checked>
<span class="switch-label" data-on="YES" data-off="YES"></span>
<span>  </span>
<b class="tooltip"><em>Yes! My Current Policy is Comprehensive Cover</em></b>
</label>
</div>

<div class="section colm colm3 percentage">
<label class="switch switch-round block option">
<input type="radio" class="current_policy_comp_ins" name="mycurrentpolicy_chk" id="fr8" value="No">
<span class="switch-label" data-on="NO" data-off="NO"></span>
<span>  </span>
<b class="tooltip"><em>No! My Insurance is TPL</em></b>
</label>
</div>
</div>



<div class="frm-row" id="my_veh_is_gcc">
<div class="section colm colm6"><p class="step1_labels">My Vehicle is GCC Specification / Not Modified</p></div>
<div class="section colm colm3 percentage">
<label class="switch switch-round block option">
<input type="radio" class="vehicle_is_gcc_spec" name="vehicle_isgcc_chk" id="fr11" value="Yes" checked>
<span class="switch-label" data-on="YES" data-off="YES"></span>
<span>  </span>
<b class="tooltip"><em>Vehicle is GCC Specification</em></b>
</label>
</div>

<div class="section colm colm3">
<label class="switch switch-round block option">
<input type="radio" class="vehicle_is_gcc_spec" name="vehicle_isgcc_chk" id="fr12" value="No">
<span class="switch-label" data-on="NO" data-off="NO"></span>
<span>  </span>
<b class="tooltip"><em>Vehicle is Modified</em></b>
</label>
</div>
</div>



<div class="frm-row" id="veh_not_forcommercial">
<div class="section colm colm6"><p class="step1_labels">My Vehicle is Not for Commercial Purpose</p></div>
<div class="section colm colm3 percentage">
<label class="switch switch-round block option">
<input type="radio" class="not_for_commercial_purpose" name="vehiclecommercial_chk" id="fr13" value="Yes" checked>
<span class="switch-label" data-on="YES" data-off="YES"></span>
<span>  </span>
<b class="tooltip"><em>Yes! Vehicle is Personal Use</em></b>
</label>
</div>

<div class="section colm colm3">
<label class="switch switch-round block option ">
<input type="radio" class="not_for_commercial_purpose" name="vehiclecommercial_chk" id="fr14" value="No">
<span class="switch-label" data-on="NO" data-off="NO"></span>
<span>  </span>
<b class="tooltip"><em>Vehicle for Commercial Purpose</em></b>
</label>
</div>
</div>


<!--
<div style="display:none;" class="frm-row">
<div class="section colm colm6"><p class="step2_labels">Do you have NCD Certificate from your insurance?</p></div>
<div class="section colm colm6">
<label class="field select">
<select class="calculate1 percentage" id="years_ncd" name="years_ncd" required>
<option class="options" value="">-- Please Select One --</option>
<option class="options" value="0">No, I have No Certificate</option>
<option class="options" value="1">Yes 1 Year Proof</option>
<option class="options" value="2">Yes 2 Year Proof</option>
<option class="options" value="3">Yes 3 Year Proof</option>
<option class="options" value="4">Yes More then 3 Year Proof</option>
</select><i class="fa fa-chevron-down arrow1"></i>
</label>

<div style="@display:none;">
<p id="car_ncd_details" class="car_value_box">Save up to 10% if you have a No Claims Certificate from your insurers Policy</p>
</div>
</div>
</div> -->


<div class="spacer-b30">
<div class="tagline"><span> Additional Cover for Your Peace of Mind </span></div><!-- .tagline -->
</div>



<div class="frm-row" id="rent_a_car_id">
<div class="section colm colm6"><p class="step1_labels">I Need My Vehicle Replacement / Rental Car</p></div>
<div class="section colm colm3 percentage">
<label class="switch switch-round block option">
<input type="radio" class="need_veh_replacement" name="veh_replacement_chk" id="fr21" value="Yes">
<span class="switch-label" data-on="YES" data-off="YES"></span>
<span>  </span>
<b class="tooltip"><em>Yes! i Need Rental Car Option</em></b>
</label>
</div>

<div class="section colm colm3">
<label class="switch switch-round block option">
<input type="radio" class="need_veh_replacement" name="veh_replacement_chk" id="fr22" value="No" checked>
<span class="switch-label" data-on="NO" data-off="NO"></span>
<span>  </span>
<b class="tooltip"><em>No! i Don't Need Rental Car Option</em></b>
</label>
</div>
</div>



<div class="frm-row" id="pa_cover_driver_id">
<div class="section colm colm6"><p class="step1_labels">I Need Personal Accident Cover for Driver</p></div>
<div class="section colm colm3 percentage">
<label class="switch switch-round block option">
<input type="radio" class="need_per_acc_cover" name="per_accidentcover_chk" id="fr23" value="Yes" checked>
<span class="switch-label" data-on="YES" data-off="YES"></span>
<span>  </span>
<b class="tooltip"><em>Yes! I Need Personal Accident Cover for Driver</em></b>
</label>
</div>

<div class="section colm colm3">
<label class="switch switch-round block option">
<input type="radio" class="need_per_acc_cover" name="per_accidentcover_chk" id="fr24" value="No">
<span class="switch-label" data-on="NO" data-off="NO"></span>
<span>  </span>
<b class="tooltip"><em>NO! i Don't Need Accident Cover for Driver</em></b>
</label>
</div>
</div>



<div class="frm-row form_row-space" id="pa_cover_passangers_id">
<div class="section colm colm6"><p class="step2_labels">I Need Personal Accident Cover for Passengers</p></div>
<div class="section colm colm6">
<label class="field select">
<select class="calculate percentage" id="pa_cover_pass" name="pa_cover_pass" required>
<option class="option" value="0">--- Please Choose Passengers ---</option>
<option class="option" value="1">For 1 Person</option>
<option class="option" value="2">For 2 Persons</option>
<option class="option" value="3">For 3 Persons</option>
<option class="option" value="4">For 4 Persons</option>
<option class="option" value="5">For 5 Persons</option>
<option class="option" value="6">For 6 Persons</option>
<option class="option" value="7">For 7 Persons</option>
</select><i class="fa fa-chevron-down arrow1"></i>
<b class="tooltip tip-right-top"><em>Please! Select Number of Passengers, For Accident Cover</em></b>
</label>
</div>
</div>



<div class="frm-row form_row-space" style="display: none;">
<div class="section colm colm6"><p class="step2_labels">I Choose Auto Repair from</p></div>
<div class="section colm colm6">

<div class="section colm colm6" id="per_agency_repair_pw">
<label class="switch-label111 modern-switch">
<input type="checkbox" class="personal_agency_repair_pw" name="personal_agency_repair_pw" value="Standard Wrokshop" checked>
<span class="switch-toggle"></span>
<span class="switch-label">Standard Wrokshop</span>
<b class="tooltip tip-right-top"><em>Need Standard Wrokshop</em></b>
</label>
</div>

<div class="section colm colm6" id="per_agency_repair_ia">
<label class="switch-label111 modern-switch">
<input type="checkbox" class="personal_agency_repair_ia" name="personal_agency_repair_ia" value="Inside Agency">
<span class="switch-toggle"></span>
<span class="switch-label">Inside Agency</span>
<b class="tooltip"><em>Need Inside Agency</em></b>
</label>
</div>
</div>

</div>



<div class="frm-row" id="outside_uae_coverage_id" style="display: none;">
<div class="section colm colm6"><p class="step1_labels">I Need Outside UAE Coverage</p></div>
<div class="section colm colm3 percentage">
<label class="switch switch-round block option">
<input type="radio" class="need_out_uae_cover" name="outsideuaeCover_chk" id="fr25" value="Yes">
<span class="switch-label" data-on="YES" data-off="YES"></span>
<span>  </span>
<b class="tooltip"><em>Yes! Need Outside UAE Coverage</em></b>
</label>
</div>

<div class="section colm colm3">
<label class="switch switch-round block option">
<input type="radio" class="need_out_uae_cover" name="outsideuaeCover_chk" id="fr26" value="No" checked>
<span class="switch-label" data-on="NO" data-off="NO"></span>
<span>  </span>
<b class="tooltip"><em>Don't Need Outside UAE Coverage</em></b>
</label>
</div>
</div>


</fieldset>


<h2>Driver Information</h2>
<fieldset>
<!-- STEP 3 HTML MARKUP GOES HERE-->

<div class="frm-row">
<div class="section colm colm6"><p class="step2_labels">Your Full Name is</p></div>
<div class="section colm colm6">
<label for="firstName" class="field prepend-icon">
<input type="text" name="firstName" placeholder="Your Full Name" class="gui-input required percentage" id="firstname" value="" required />
<b class="tooltip tip-right-top"><em>Please! Fill Your Full Name</em></b>
<span class="field-icon"><i class="fa fa-user"></i></span>
</label>
</div>
</div>

<div class="frm-row">
<div class="section colm colm6"><p class="step1_labels">Your Email Address is</p></div>
<div class="section colm colm6">
<label for="email_address" class="field prepend-icon">
<input type="email" placeholder="Email Address" class="gui-input required percentage" id="email_address" name="email_address" />
<b class="tooltip tip-right-top"><em>Write Email address..then! we will send you policy documents.</em></b>
<span class="field-icon"><i class="fa fa-envelope"></i></span>
</label>
</div>
</div>


<div class="frm-row">
<div class="section colm colm6"><p class="step2_labels">Your Contact Number is</p></div>
<div class="section colm colm2">
<label class="field select">
<select class="calculate percentage" id="contact_num1" name="contact_num1" required>
<option value="" selected> NO. Code </option>
<option value="050"> 050 </option>
<option value="052"> 052 </option>
<option value="053"> 053 </option>
<option value="054"> 054 </option>
<option value="055"> 055 </option>
<option value="056"> 056 </option>
<option value="058"> 058 </option>
</select><i class="fa fa-chevron-down arrow1"></i>
<b class="tooltip tip-right-top"><em>Your Mobile Code</em></b>
</label>
</div>

<div class="section colm colm4">
<label class="field prepend-icon">
<input type="text" name="contact_num2" placeholder="e.g. 8204101" class="gui-input percentage" id="contact_num2" value="" required>
<b class="tooltip tip-right-top"><em>Please Enter a Valid Phone Number</em></b>
<span class="field-icon"><i class="fa fa-phone-square"></i></span>
</label>
</div>
</div>



<div class="frm-row">
<div class="section colm colm6"><p class="step2_labels">Your Nationality is</p></div>
<div class="section colm colm6">
<label class="field select">
<select class="calculate percentage" id="nationality" name="packaging" required>
<option value="none" selected>--- My Nationality is ---</option>

<option value="afghan">Afghan</option>
<option value="albanian">Albanian</option>
<option value="algerian">Algerian</option>
<option value="american">American</option>
<option value="andorran">Andorran</option>
<option value="angolan">Angolan</option>
<option value="antiguans">Antiguans</option>
<option value="argentinean">Argentinean</option>
<option value="armenian">Armenian</option>
<option value="australian">Australian</option>
<option value="austrian">Austrian</option>
<option value="azerbaijani">Azerbaijani</option>
<option value="bahamian">Bahamian</option>
<option value="bahraini">Bahraini</option>
<option value="bangladeshi">Bangladeshi</option>
<option value="barbadian">Barbadian</option>
<option value="barbudans">Barbudans</option>
<option value="batswana">Batswana</option>
<option value="belarusian">Belarusian</option>
<option value="belgian">Belgian</option>
<option value="belizean">Belizean</option>
<option value="beninese">Beninese</option>
<option value="bhutanese">Bhutanese</option>
<option value="bolivian">Bolivian</option>
<option value="bosnian">Bosnian</option>
<option value="brazilian">Brazilian</option>
<option value="british">British</option>
<option value="bruneian">Bruneian</option>
<option value="bulgarian">Bulgarian</option>
<option value="burkinabe">Burkinabe</option>
<option value="burmese">Burmese</option>
<option value="burundian">Burundian</option>
<option value="cambodian">Cambodian</option>
<option value="cameroonian">Cameroonian</option>
<option value="canadian">Canadian</option>
<option value="cape verdean">Cape Verdean</option>
<option value="central african">Central African</option>
<option value="chadian">Chadian</option>
<option value="chilean">Chilean</option>
<option value="chinese">Chinese</option>
<option value="colombian">Colombian</option>
<option value="comoran">Comoran</option>
<option value="congolese">Congolese</option>
<option value="costa rican">Costa Rican</option>
<option value="croatian">Croatian</option>
<option value="cuban">Cuban</option>
<option value="cypriot">Cypriot</option>
<option value="czech">Czech</option>
<option value="danish">Danish</option>
<option value="djibouti">Djibouti</option>
<option value="dominican">Dominican</option>
<option value="dutch">Dutch</option>
<option value="east timorese">East Timorese</option>
<option value="ecuadorean">Ecuadorean</option>
<option value="egyptian">Egyptian</option>
<option value="equatorial guinean">Equatorial Guinean</option>
<option value="eritrean">Eritrean</option>
<option value="estonian">Estonian</option>
<option value="ethiopian">Ethiopian</option>
<option value="Emirati">Emirati</option>
<option value="emirian">Emirian</option>
<option value="fijian">Fijian</option>
<option value="filipino">Filipino</option>
<option value="finnish">Finnish</option>
<option value="french">French</option>
<option value="gabonese">Gabonese</option>
<option value="gambian">Gambian</option>
<option value="georgian">Georgian</option>
<option value="german">German</option>
<option value="ghanaian">Ghanaian</option>
<option value="greek">Greek</option>
<option value="grenadian">Grenadian</option>
<option value="guatemalan">Guatemalan</option>
<option value="guinea-bissauan">Guinea-Bissauan</option>
<option value="guinean">Guinean</option>
<option value="guyanese">Guyanese</option>
<option value="haitian">Haitian</option>
<option value="herzegovinian">Herzegovinian</option>
<option value="honduran">Honduran</option>
<option value="hungarian">Hungarian</option>
<option value="icelander">Icelander</option>
<option value="indian">Indian</option>
<option value="indonesian">Indonesian</option>
<option value="iranian">Iranian</option>
<option value="iraqi">Iraqi</option>
<option value="irish">Irish</option>
<option value="israeli">Israeli</option>
<option value="italian">Italian</option>
<option value="ivorian">Ivorian</option>
<option value="jamaican">Jamaican</option>
<option value="japanese">Japanese</option>
<option value="jordanian">Jordanian</option>
<option value="kazakhstani">Kazakhstani</option>
<option value="kenyan">Kenyan</option>
<option value="kittian and nevisian">Kittian and Nevisian</option>
<option value="kuwaiti">Kuwaiti</option>
<option value="kyrgyz">Kyrgyz</option>
<option value="laotian">Laotian</option>
<option value="latvian">Latvian</option>
<option value="lebanese">Lebanese</option>
<option value="liberian">Liberian</option>
<option value="libyan">Libyan</option>
<option value="liechtensteiner">Liechtensteiner</option>
<option value="lithuanian">Lithuanian</option>
<option value="luxembourger">Luxembourger</option>
<option value="macedonian">Macedonian</option>
<option value="malagasy">Malagasy</option>
<option value="malawian">Malawian</option>
<option value="malaysian">Malaysian</option>
<option value="maldivan">Maldivan</option>
<option value="malian">Malian</option>
<option value="maltese">Maltese</option>
<option value="marshallese">Marshallese</option>
<option value="mauritanian">Mauritanian</option>
<option value="mauritian">Mauritian</option>
<option value="mexican">Mexican</option>
<option value="micronesian">Micronesian</option>
<option value="moldovan">Moldovan</option>
<option value="monacan">Monacan</option>
<option value="mongolian">Mongolian</option>
<option value="moroccan">Moroccan</option>
<option value="mosotho">Mosotho</option>
<option value="motswana">Motswana</option>
<option value="mozambican">Mozambican</option>
<option value="namibian">Namibian</option>
<option value="nauruan">Nauruan</option>
<option value="nepalese">Nepalese</option>
<option value="new zealander">New Zealander</option>
<option value="ni-vanuatu">Ni-Vanuatu</option>
<option value="nicaraguan">Nicaraguan</option>
<option value="nigerien">Nigerien</option>
<option value="north korean">North Korean</option>
<option value="northern irish">Northern Irish</option>
<option value="norwegian">Norwegian</option>
<option value="omani">Omani</option>
<option value="pakistani">Pakistani</option>
<option value="palauan">Palauan</option>
<option value="panamanian">Panamanian</option>
<option value="papua new guinean">Papua New Guinean</option>
<option value="paraguayan">Paraguayan</option>
<option value="peruvian">Peruvian</option>
<option value="Philippine">Philippine</opion>
<option value="polish">Polish</option>
<option value="portuguese">Portuguese</option>
<option value="qatari">Qatari</option>
<option value="romanian">Romanian</option>
<option value="russian">Russian</option>
<option value="rwandan">Rwandan</option>
<option value="saint lucian">Saint Lucian</option>
<option value="salvadoran">Salvadoran</option>
<option value="samoan">Samoan</option>
<option value="san marinese">San Marinese</option>
<option value="sao tomean">Sao Tomean</option>
<option value="saudi">Saudi</option>
<option value="scottish">Scottish</option>
<option value="senegalese">Senegalese</option>
<option value="serbian">Serbian</option>
<option value="seychellois">Seychellois</option>
<option value="sierra leonean">Sierra Leonean</option>
<option value="singaporean">Singaporean</option>
<option value="slovakian">Slovakian</option>
<option value="slovenian">Slovenian</option>
<option value="solomon islander">Solomon Islander</option>
<option value="somali">Somali</option>
<option value="south african">South African</option>
<option value="south korean">South Korean</option>
<option value="spanish">Spanish</option>
<option value="sri lankan">Sri Lankan</option>
<option value="sudanese">Sudanese</option>
<option value="surinamer">Surinamer</option>
<option value="swazi">Swazi</option>
<option value="swedish">Swedish</option>
<option value="swiss">Swiss</option>
<option value="syrian">Syrian</option>
<option value="taiwanese">Taiwanese</option>
<option value="tajik">Tajik</option>
<option value="tanzanian">Tanzanian</option>
<option value="thai">Thai</option>
<option value="togolese">Togolese</option>
<option value="tongan">Tongan</option>
<option value="tunisian">Tunisian</option>
<option value="turkish">Turkish</option>
<option value="tuvaluan">Tuvaluan</option>
<option value="ugandan">Ugandan</option>
<option value="ukrainian">Ukrainian</option>
<option value="uruguayan">Uruguayan</option>
<option value="uzbekistani">Uzbekistani</option>
<option value="venezuelan">Venezuelan</option>
<option value="vietnamese">Vietnamese</option>
<option value="welsh">Welsh</option>
<option value="yemenite">Yemenite</option>
<option value="zambian">Zambian</option>
<option value="zimbabwean">Zimbabwean</option>

</select><i class="fa fa-chevron-down arrow1"></i>
<b class="tooltip tip-right-top"><em>Select Your Nationality. for example: i'am Emirati</em></b>
</label>
</div>
</div>



<div class="frm-row">
<div class="section colm colm6"><p class="step2_labels">Your Date of Birth</p></div>
<div class="section colm colm6 percentage">
<label class="field prepend-icon">
<input type="text" name="datepicker1" placeholder="05-26-1980" id="datepicker1" class="gui-input" required>
<b class="tooltip tip-right-top"><em>Please Select Your Date of Birth (MM/DD/YY)</em></b>
<span class="field-icon"><i class="fa fa-calendar"></i></span>
</label>
</div>

<script type="text/javascript">
	jQuery(function() {
	jQuery("#datepicker1").datepicker({
	numberOfMonths: 1,
	prevText: '<i class="fa fa-chevron-left"></i>',
	nextText: '<i class="fa fa-chevron-right"></i>',
	showButtonPanel: false,
	changeMonth: true,
	changeYear: true,
	yearRange: "1975:2002"
	});
	});
</script>
</div>



<div class="frm-row" id="year_uae_driving_licence">
<div class="section colm colm6"><p class="step2_labels">How many Years of Driving Experience in UAE</p></div>
<div class="section colm colm6">
<label class="field select">
<select class="calculate percentage" id="years_uaedrivinglicence" name="years_uaedrivinglicence" required>
<option class="option" value="">--- Select How many Years ---</option>
<option value="66"> Less then one Year </option>
<option value="1"> 1 Year </option>
<option value="2"> 2 Years </option>
<option value="3"> 3 Years </option>
<option value="44"> More then 4 Years </option>
</select><i class="fa fa-chevron-down arrow1"></i>
<b class="tooltip tip-right-top"><em>Your Total Driving Experience in UAE Country</em></b>
</label>
</div>
</div>



<div style="display:none;" class="frm-row">
<div class="section colm colm6"><p class="step2_labels">Country of Your First Driver's license is</p></div>
<div class="section colm colm6">
<label class="field select">
<select class="calculate percentage" id="country_firstdl" name="country_firstdl" required>
<option class="option" value="">-- Select First Driving License Country --</option>
<option class="option" value="United Arab Emirates">United Arab Emirates</option>
<option class="option" value="Albania">Albania</option>
<option class="option" value="Algeria">Algeria</option>
<option class="option" value="American Samoa">American Samoa</option>
<option class="option" value="Andorra">Andorra</option>
<option class="option" value="Angola">Angola</option>
<option class="option" value="Anguilla">Anguilla</option>
<option class="option" value="Antarctica">Antarctica</option>
<option class="option" value="Antigua and Barbuda">Antigua and Barbuda</option>
<option class="option" value="Argentina">Argentina</option>
<option class="option" value="Armenia">Armenia</option>
<option class="option" value="Aruba">Aruba</option>
<option class="option" value="Australia">Australia</option>
<option class="option" value="Austria">Austria</option>
<option class="option" value="Azerbaijan">Azerbaijan</option>
<option class="option" value="Bahamas">Bahamas</option>
<option class="option" value="Bahrain">Bahrain</option>
<option class="option" value="Bangladesh">Bangladesh</option>
<option class="option" value="Barbados">Barbados</option>
<option class="option" value="Belarus">Belarus</option>
<option class="option" value="Belgium">Belgium</option>
<option class="option" value="Belize">Belize</option>
<option class="option" value="Benin">Benin</option>
<option class="option" value="Bermuda">Bermuda</option>
<option class="option" value="Bhutan">Bhutan</option>
<option class="option" value="Bolivia">Bolivia</option>
<option class="option" value="Bosnia and Herzegovina">Bosnia and Herzegovina</option>
<option class="option" value="Botswana">Botswana</option>
<option class="option" value="Bouvet Island">Bouvet Island</option>
<option class="option" value="Brazil">Brazil</option>
<option class="option" value="Brunei Darussalam">Brunei Darussalam</option>
<option class="option" value="Bulgaria">Bulgaria</option>
<option class="option" value="Burkina Faso">Burkina Faso</option>
<option class="option" value="Burundi">Burundi</option>
<option class="option" value="Cambodia">Cambodia</option>
<option class="option" value="Cameroon">Cameroon</option>
<option class="option" value="Canada">Canada</option>
<option class="option" value="Cape Verde">Cape Verde</option>
<option class="option" value="Cayman Islands">Cayman Islands</option>
<option class="option" value="Central African Republic">Central African Republic</option>
<option class="option" value="Chad">Chad</option>
<option class="option" value="Chile">Chile</option>
<option class="option" value="China">China</option>
<option class="option" value="Christmas Island">Christmas Island</option>
<option class="option" value="Cocos (Keeling) Islands">Cocos (Keeling) Islands</option>
<option class="option" value="Colombia">Colombia</option>
<option class="option" value="Comoros">Comoros</option>
<option class="option" value="Congo">Congo</option>
<option class="option" value="Cook Islands">Cook Islands</option>
<option class="option" value="Costa Rica">Costa Rica</option>
<option class="option" value="Cote optionoire">Cote optionoire</option>
<option class="option" value="Croatia">Croatia</option>
<option class="option" value="Cuba">Cuba</option>
<option class="option" value="Cyprus">Cyprus</option>
<option class="option" value="Czech Republic">Czech Republic</option>
<option class="option" value="Denmark">Denmark</option>
<option class="option" value="Djibouti">Djibouti</option>
<option class="option" value="Dominica">Dominica</option>
<option class="option" value="Dominican Republic">Dominican Republic</option>
<option class="option" value="East Timor">East Timor</option>
<option class="option" value="Ecuador">Ecuador</option>
<option class="option" value="Egypt">Egypt</option>
<option class="option" value="El Salvador">El Salvador</option>
<option class="option" value="Equatorial Guinea">Equatorial Guinea</option>
<option class="option" value="Eritrea">Eritrea</option>
<option class="option" value="Estonia">Estonia</option>
<option class="option" value="Ethiopia">Ethiopia</option>
<option class="option" value="Faroe Islands">Faroe Islands</option>
<option class="option" value="Fiji">Fiji</option>
<option class="option" value="Finland">Finland</option>
<option class="option" value="France">France</option>
<option class="option" value="France Metropolitan">France Metropolitan</option>
<option class="option" value="French Guiana">French Guiana</option>
<option class="option" value="French Polynesia">French Polynesia</option>
<option class="option" value="Gabon">Gabon</option>
<option class="option" value="Gambia">Gambia</option>
<option class="option" value="Georgia">Georgia</option>
<option class="option" value="Germany">Germany</option>
<option class="option" value="Ghana">Ghana</option>
<option class="option" value="Gibraltar">Gibraltar</option>
<option class="option" value="Greece">Greece</option>
<option class="option" value="Greenland">Greenland</option>
<option class="option" value="Grenada">Grenada</option>
<option class="option" value="Guadeloupe">Guadeloupe</option>
<option class="option" value="Guam">Guam</option>
<option class="option" value="Guatemala">Guatemala</option>
<option class="option" value="Guinea">Guinea</option>
<option class="option" value="Guinea-bissau">Guinea-bissau</option>
<option class="option" value="Guyana">Guyana</option>
<option class="option" value="Haiti">Haiti</option>
<option class="option" value="Honduras">Honduras</option>
<option class="option" value="Hong Kong">Hong Kong</option>
<option class="option" value="Hungary">Hungary</option>
<option class="option" value="Iceland">Iceland</option>
<option class="option" value="India">India</option>
<option class="option" value="Indonesia">Indonesia</option>
<option class="option" value="Iran">Iran</option>
<option class="option" value="Iraq">Iraq</option>
<option class="option" value="Ireland">Ireland</option>
<option class="option" value="Italy">Italy</option>
<option class="option" value="Jamaica">Jamaica</option>
<option class="option" value="Japan">Japan</option>
<option class="option" value="Jordan">Jordan</option>
<option class="option" value="Kazakhstan">Kazakhstan</option>
<option class="option" value="Kenya">Kenya</option>
<option class="option" value="Kiribati">Kiribati</option>
<option class="option" value="Korea">Korea</option>
<option class="option" value="Kuwait">Kuwait</option>
<option class="option" value="Kyrgyzstan">Kyrgyzstan</option>
<option class="option" value="Latvia">Latvia</option>
<option class="option" value="Lebanon">Lebanon</option>
<option class="option" value="Lesotho">Lesotho</option>
<option class="option" value="Liberia">Liberia</option>
<option class="option" value="Libya">Libya</option>
<option class="option" value="Liechtenstein">Liechtenstein</option>
<option class="option" value="Lithuania">Lithuania</option>
<option class="option" value="Luxembourg">Luxembourg</option>
<option class="option" value="Macau">Macau</option>
<option class="option" value="Macedonia">Macedonia</option>
<option class="option" value="Madagascar">Madagascar</option>
<option class="option" value="Malawi">Malawi</option>
<option class="option" value="Malaysia">Malaysia</option>
<option class="option" value="Maloptiones">Maloptiones</option>
<option class="option" value="Mali">Mali</option>
<option class="option" value="Malta">Malta</option>
<option class="option" value="Martinique">Martinique</option>
<option class="option" value="Mauritania">Mauritania</option>
<option class="option" value="Mauritius">Mauritius</option>
<option class="option" value="Mayotte">Mayotte</option>
<option class="option" value="Mexico">Mexico</option>
<option class="option" value="Monaco">Monaco</option>
<option class="option" value="Mongolia">Mongolia</option>
<option class="option" value="Montserrat">Montserrat</option>
<option class="option" value="Morocco">Morocco</option>
<option class="option" value="Mozambique">Mozambique</option>
<option class="option" value="Myanmar">Myanmar</option>
<option class="option" value="Namibia">Namibia</option>
<option class="option" value="Nauru">Nauru</option>
<option class="option" value="Nepal">Nepal</option>
<option class="option" value="Netherlands">Netherlands</option>
<option class="option" value="New Caledonia">New Caledonia</option>
<option class="option" value="New Zealand">New Zealand</option>
<option class="option" value="Nicaragua">Nicaragua</option>
<option class="option" value="Niger">Niger</option>
<option class="option" value="Nigeria">Nigeria</option>
<option class="option" value="Niue">Niue</option>
<option class="option" value="Norfolk Island">Norfolk Island</option>
<option class="option" value="Norway">Norway</option>
<option class="option" value="Oman">Oman</option>
<option class="option" value="Pakistan">Pakistan</option>
<option class="option" value="Palau">Palau</option>
<option class="option" value="Palestine">Palestine</option>
<option class="option" value="Panama">Panama</option>
<option class="option" value="Papua New Guinea">Papua New Guinea</option>
<option class="option" value="Paraguay">Paraguay</option>
<option class="option" value="Peru">Peru</option>
<option class="option" value="Philippines">Philippines</option>
<option class="option" value="Pitcairn">Pitcairn</option>
<option class="option" value="Poland">Poland</option>
<option class="option" value="Portugal">Portugal</option>
<option class="option" value="Puerto Rico">Puerto Rico</option>
<option class="option" value="Qatar">Qatar</option>
<option class="option" value="Romania">Romania</option>
<option class="option" value="Russia">Russia</option>
<option class="option" value="Rwanda">Rwanda</option>
<option class="option" value="Saint Kitts and Nevis">Saint Kitts and Nevis</option>
<option class="option" value="Saint Lucia">Saint Lucia</option>
<option class="option" value="Samoa">Samoa</option>
<option class="option" value="San Marino">San Marino</option>
<option class="option" value="Sao Tome and Principe">Sao Tome and Principe</option>
<option class="option" value="Saudi Arabia">Saudi Arabia</option>
<option class="option" value="Serbia">Serbia</option>
<option class="option" value="Senegal">Senegal</option>
<option class="option" value="Seychelles">Seychelles</option>
<option class="option" value="Sierra Leone">Sierra Leone</option>
<option class="option" value="Singapore">Singapore</option>
<option class="option" value="Slovakia (Slovak Republic)">Slovakia (Slovak Republic)</option>
<option class="option" value="Slovenia">Slovenia</option>
<option class="option" value="Solomon Islands">Solomon Islands</option>
<option class="option" value="Somalia">Somalia</option>
<option class="option" value="South Africa">South Africa</option>
<option class="option" value="Spain">Spain</option>
<option class="option" value="Sri Lanka">Sri Lanka</option>
<option class="option" value="St. Helena">St. Helena</option>
<option class="option" value="Sudan">Sudan</option>
<option class="option" value="Suriname">Suriname</option>
<option class="option" value="Swaziland">Swaziland</option>
<option class="option" value="Sweden">Sweden</option>
<option class="option" value="Switzerland">Switzerland</option>
<option class="option" value="Syrian Arab Republic">Syrian Arab Republic</option>
<option class="option" value="Taiwan">Taiwan</option>
<option class="option" value="Tajikistan">Tajikistan</option>
<option class="option" value="Thailand">Thailand</option>
<option class="option" value="Togo">Togo</option>
<option class="option" value="Tokelau">Tokelau</option>
<option class="option" value="Tonga">Tonga</option>
<option class="option" value="Trinidad and Tobago">Trinidad and Tobago</option>
<option class="option" value="Tunisia">Tunisia</option>
<option class="option" value="Turkey">Turkey</option>
<option class="option" value="Turkmenistan">Turkmenistan</option>
<option class="option" value="Tuvalu">Tuvalu</option>
<option class="option" value="Uganda">Uganda</option>
<option class="option" value="Ukraine">Ukraine</option>
<option class="option" value="United Arab Emirates">United Arab Emirates</option>
<option class="option" value="United Kingdom">United Kingdom</option>
<option class="option" value="United States">United States</option>
<option class="option" value="Uruguay">Uruguay</option>
<option class="option" value="Uzbekistan">Uzbekistan</option>
<option class="option" value="Vanuatu">Vanuatu</option>
<option class="option" value="Venezuela">Venezuela</option>
<option class="option" value="Viet Nam">Viet Nam</option>
<option class="option" value="Virgin Islands (British)">Virgin Islands (British)</option>
<option class="option" value="Virgin Islands (U.S.)">Virgin Islands (U.S.)</option>
<option class="option" value="Wallis and Futuna Islands">Wallis and Futuna Islands</option>
<option class="option" value="Western Sahara">Western Sahara</option>
<option class="option" value="Yemen">Yemen</option>
<option class="option" value="Yugoslavia">Yugoslavia</option>
<option class="option" value="Zambia">Zambia</option>
<option class="option" value="Zimbabwe">Zimbabwe</option>
</select>
<i class="fa fa-chevron-down arrow1"></i>
<b class="tooltip tip-right-top"><em>Your First Driving license Country</em></b>
</label>
</div>
</div>



<div class="frm-row" style="display: none;">
<div class="section colm colm6"><p class="step2_labels">Your Home Country Driving Experience is</p></div>
<div class="section colm colm6">
<label class="field select">
<select class="calculate percentage" id="home_country_drivingexp" name="home_country_drivingexp">
<option value="">---- Select How many Years ----</option>
<option value="66"> Less then one Year </option>
<option value="1"> 1 Year </option>
<option value="2"> 2 Years </option>
<option value="3"> 3 Years </option>
<option value="44"> More then 4 Years </option>
</select><i class="fa fa-chevron-down arrow1"></i>
<b class="tooltip tip-right-top"><em>Please! Select Your Home Country Driving Experience</em></b>
</label>
</div>
</div>

<div class="actions_c clearfix">
<ul aria-label="Pagination">
	<li style="display: list-item;">
	<a href="#" id="qoute_custom">Get Qoute</a><a href="#" id="qoute_custom_gcc">Get Qoute</a></li>
</ul>
</div>


</fieldset>
</form>
</div>
</div>
</div>
</div>





<!-- Jquery  Code -->
<?php
$json_form_dataset = json_encode($form_dataset);
$json_form_dataComp = json_encode($form_dataComp);
?>


<script>
jQuery(document).ready(function() {


jQuery( "#step5" ).hide();
jQuery( "#qoute_custom_gcc" ).hide();
jQuery( "#bread_crums_custom" ).hide();
jQuery("#vehicle_val").prop('disabled', true);
jQuery("#car_value_details").hide();
jQuery("#model_year").prop('disabled', true);
jQuery("#vehicle_model_detail").prop('disabled', true);
/* var arr_rp_cond = [];
jQuery('#CheckBoxList').on('change', 'input[type=checkbox]', function() {
var checkedValues = jQuery('input:checkbox:checked').map(function() {
return this.value;
}).get();
////////////////console.log("checkedValuesO...."+checkedValues);
//checkedValues.splice(-1);
checkedValues.join('#');
localStorage.setItem("make_list",checkedValues);
}); */

//Set value of info Box to Table
/* 	function setval_info(){
var value_set_info = jQuery( "#vehicle_val" ).val();
}
*/
//var value_set_info = jQuery( "#vehicle_val" ).val();
//jQuery( "#cars_worth" ).html(value_set_info);

//Model Condition

jQuery( "#popup_dangermain1111" ).hide();
jQuery( "#popup_dangermain1111" ).hide();
var ideal_date_21 = "01/1/1993"

//Custom Breadcrumbs Conditions

//official steps structure customize
/* jQuery( "#smart-form-t-1" ).click(function() {
	var vehicle_brand_filter1 = jQuery('#vehicle_brand option:selected').val();
	var veh_city_ofreg1 = jQuery('#cityof_reg').val();
	jQuery( ".actions ul li" ).last().css( "display", "block" );
	if(vehicle_brand_filter1 && veh_city_ofreg1){
		jQuery( "#smart-form-p-0" ).hide();
		jQuery( "#smart-form-p-2" ).hide();
		jQuery( "#smart-form-p-1" ).show();
		jQuery("#smart-form-p-0").css("left", "-856px");
		jQuery("#smart-form-p-2").css("left", "-856px");
		jQuery("#smart-form-p-1").css("left", "0px");
		jQuery( "#smart-form-t-1" ).addClass( "current");
		jQuery( "#smart-form-t-0" ).removeClass( "current");
		jQuery( "#smart-form-t-2" ).removeClass( "current");
	}

});
jQuery( "#smart-form-t-0" ).click(function() {
	var pa_cover_pass1 = jQuery('#pa_cover_pass option:selected').val();
	jQuery( ".actions ul li" ).last().css( "display", "block" );
	if(pa_cover_pass1){
		jQuery( "#smart-form-p-1" ).hide();
		jQuery( "#smart-form-p-2" ).hide();
		jQuery( "#smart-form-p-0" ).show();
		jQuery("#smart-form-p-0").css("left", "0px");
		jQuery("#smart-form-p-1").css("left", "-856px");
		jQuery("#smart-form-p-2").css("left", "-856px");
		jQuery( "#smart-form-t-0" ).addClass( "current");
		jQuery( "#smart-form-t-1" ).removeClass( "current");
		jQuery( "#smart-form-t-2" ).removeClass( "current");
	}
});
jQuery( "#smart-form-t-2" ).click(function() {
	var veh_fullname1 = jQuery('#firstname').val();
	var veh_nationality1 = jQuery('#nationality option:selected').val();
	var years_uaedriving11 = jQuery('#years_uaedrivinglicence option:selected').val();
	jQuery( ".actions ul li" ).last().css( "display", "block" );
	if(veh_fullname1 && veh_nationality1){
		if(years_uaedriving11){
			jQuery( "#smart-form-p-1" ).hide();
			jQuery( "#smart-form-p-0" ).hide();
			jQuery( "#smart-form-p-2" ).show();
			jQuery("#smart-form-p-2").css("left", "0px");
			jQuery("#smart-form-p-0").css("left", "-856px");
			jQuery("#smart-form-p-1").css("left", "-856px");
			jQuery( "#smart-form-t-0" ).removeClass( "current");
			jQuery( "#smart-form-t-1" ).removeClass( "current");
			jQuery( "#smart-form-t-2" ).addClass( "current");
		}
	}

}); */

jQuery( "#goto_table" ).click(function() {
jQuery( "#step4" ).show();
jQuery( "#step5" ).hide();
jQuery( "#form_main_div" ).hide();
jQuery( ".form_desc" ).hide();
});

//Get Qoute Custom Click
jQuery( "#qoute_custom" ).click(function() {

 var veh_fullname_filled = jQuery('#firstname').val();
 var veh_contact_num2 = jQuery('#contact_num2').val();
 var veh_nationality = jQuery('#nationality option:selected').val();
 var years_uaedrivinglicence = jQuery('#years_uaedrivinglicence option:selected').val();
 //var home_country_drivingexp = jQuery('#home_country_drivingexp option:selected').val();
 var age_less_25 = jQuery('#datepicker1').val();

 //console.log("veh_fullname_filled.."+veh_fullname_filled);
 //console.log("veh_contact_num2.."+veh_contact_num2);
 //console.log("veh_nationality.."+veh_nationality);
 //console.log("age_less_25.."+age_less_25);
 
 //console.log(validate_date(age_less_25));

 if(validate_date(age_less_25)){
            //alert('Valid Date');
  }else{
            //alert('Invalid Date');

            
 }       

 if((veh_fullname_filled == "") || (age_less_25 == "")){
  		alert("Please fill the Driver Information Frist");
 }
 else if((veh_contact_num2 == "") || (veh_nationality == 'none')){
  		alert("Please fill the Driver Information Frist");
 }
 else if((years_uaedrivinglicence == "") ){
  		alert("Please fill the Driver Information Frist");
 }
 


 else{
  jQuery( "#step4" ).show();
	jQuery( "#step5" ).hide();
	jQuery( "#form_main_div" ).hide();
	jQuery( ".form_desc" ).hide();
	//jQuery( ".actions ul li" ).last().css( "display", "block" );
  }

function validate_date(age_less_25){
var currVal = age_less_25;
    if(currVal == '')
        return false;
    
    var rxDatePattern = /^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{4})$/; //Declare Regex
    var dtArray = currVal.match(rxDatePattern); // is format OK?
    
    if (dtArray == null) 
        return false;
    
    //Checks for mm/dd/yyyy format.
    dtMonth = dtArray[1];
    dtDay= dtArray[3];
    dtYear = dtArray[5];        
    
    if (dtMonth < 1 || dtMonth > 12) 
        return false;
    else if (dtDay < 1 || dtDay> 31) 
        return false;
    else if ((dtMonth==4 || dtMonth==6 || dtMonth==9 || dtMonth==11) && dtDay ==31) 
        return false;
    else if (dtMonth == 2) 
    {
        var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));
        if (dtDay> 29 || (dtDay ==29 && !isleap)) 
                return false;
    }
    return true;
}

});

jQuery( "#goto_form" ).click(function() {
jQuery( "#step4" ).hide();
jQuery( "#step5" ).hide();
jQuery( "#form_main_div" ).show();
jQuery( ".form_desc" ).show();
//jQuery(".actions").hide();
//jQuery( ".actions ul li" ).last().css( "display", "block" );
});
jQuery( "#goto_checkout" ).click(function() {
jQuery( "#step4" ).hide();
jQuery( "#step5" ).show();
jQuery( "#bread_crums_custom" ).show();
jQuery( "#form_main_div" ).hide();
jQuery( ".form_desc" ).hide();
});


//Hide Popup Danger on Ok Click

jQuery( "#danger_ok" ).click(function() {
jQuery( "#popup_dangermain1111" ).hide();
});
jQuery( "#dangerDOB_ok" ).click(function() {
jQuery( "#popup_dangerdob" ).hide();
});

jQuery( "#dangerPolicy_ok" ).click(function() {
jQuery( "#popup_dangerPolicyStartDate" ).hide();
});

jQuery( "#ncd_ok" ).click(function() {
jQuery( "#popup_dangerNCD" ).hide();
});

jQuery( "#info_ok" ).click(function() {
jQuery( "#popup_infomain" ).hide();
});

jQuery( "#info_ok_brand_new" ).click(function() {
jQuery( "#popup_infomain_brand_new" ).hide();
});

jQuery( "#ins_expired_danger_ok" ).click(function() {
jQuery( "#popup_current_ins_expired" ).hide();
});

jQuery( "#veh_notcommercial_ok" ).click(function() {
jQuery( "#vehicle_notfor_commercialpurpose" ).hide();
});

jQuery( "#info_outsideuae_ok" ).click(function() {
jQuery( "#popup_outsideuae_cover" ).hide();
});


jQuery( "#datepicker1" ).change(function() {

var age_less_25 = jQuery('#datepicker1').val();
////console.log("ideal_date_21.."+ideal_date_21);
////console.log("age_less_25.."+age_less_25);

if(new Date(age_less_25) > new Date(ideal_date_21)){
//////console.log("age is less then 25 ...");
jQuery( "#popup_dangerdob" ).show();

			//logic to hide comparison data for less years
			jQuery( "#qoute_custom_gcc" ).show();
			jQuery( "#qoute_custom" ).hide();
			var website_url = "https://"+window.location.host+"/contact-insurance-aagent";
			jQuery("#qoute_custom_gcc").attr("href", website_url);

}else{
jQuery( "#popup_dangerdob" ).hide();
			
			//Show Comparisons
			jQuery( "#qoute_custom" ).show();
			jQuery( "#qoute_custom_gcc" ).hide();
			var website_url = "https://"+window.location.host+"/contact-insurance-aagent";
			jQuery("#qoute_custom_gcc").attr("href", website_url);
}
});

function refreshPrice(){
}

//When would you like the Policy to start..

// jQuery( "#vec_policy_date1" ).change(function() {
// checkdaysPolicy();
// });
// jQuery( "#vec_policy_date2" ).change(function() {
// checkdaysPolicy();
// });
// jQuery( "#vec_policy_date3" ).change(function() {
// checkdaysPolicy();
// });


// function checkdaysPolicy(){
// var vec_policy_date1 = jQuery('#vec_policy_date1 option:selected').val();
// var vec_policy_date2 = jQuery('#vec_policy_date2 option:selected').val();
// var vec_policy_date3 = jQuery('#vec_policy_date3 option:selected').val();

// var selected_Policydate = '"'+vec_policy_date2+'/'+vec_policy_date1+'/'+vec_policy_date3+'"';
// //////console.log("selected_Policydate.."+selected_Policydate);


// var numDaysBetween = function(d1, d2) {
// var diff = Math.abs(d1.getTime() - d2.getTime());
// return diff / (1000 * 60 * 60 * 24);
// };

// var d1 = new Date(selected_Policydate); // Jan 1, 2011
// var d2 = new Date(); // Jan 2, 2011
// var difference = numDaysBetween(d1, d2); // => 1
// //////console.log("d1.."+d1);
// //////console.log("d2.."+d2);
// //////console.log("difference.."+difference);
// if(difference > 60){
// //////console.log("you have selected more then 60 days.");
// //jQuery( "#popup_dangerPolicyStartDate" ).show();
// }else{
// //////console.log("you have selected Within 60 days.");
// }
// }

//Hide and Remove Date of Registration

jQuery('#dor_row').removeClass( "hide_this" );
jQuery('.brand-new-yes').click(function() {
jQuery('#dor_row').addClass( "hide_this" );
});

jQuery('.brand-new-no').click(function() {

jQuery('#dor_row').removeClass( "hide_this" );

});



jQuery(function () {

jQuery('#reg_date').datepicker({
format: 'mm/dd/yyyy',
startDate: '-3d'
});
});

jQuery(function () {
jQuery('#polstart_date').datepicker({
format: 'mm/dd/yyyy',
startDate: '-3d'
});
});

jQuery(function () {
jQuery('#dob').datepicker({
format: 'mm/dd/yyyy',
startDate: '-3d'
});
});

	

	//Submit data to chat.
	jQuery( "#contact_num2" ).change(function() {
	
	var lead_firstname_chat = jQuery('#firstname').val();
	var lead_email_addres_chat = jQuery('#email_address').val();
	var veh_contact_num1 = jQuery('#contact_num1').val();
	var veh_contact_num2 = jQuery('#contact_num2').val();
	//console.log(veh_contact_num2);
	var veh_contact_numF = veh_contact_num1+veh_contact_num2;

	//console.log(lead_email_addres_chat);

	$zopim( function() {

           $zopim.livechat.setName(lead_firstname_chat);
           $zopim.livechat.setEmail(lead_email_addres_chat);
           $zopim.livechat.setPhone(veh_contact_numF);
		   $zopim.livechat.setGreetings({
	          'online': 'Chat with us Bro...',
	          'offline': 'Leave us a message'
	      });
	      
	      	           

	});
	    javascript:void($zopim.livechat.say('Hi, How can we help you? Sir.'));

	});

	//Submit data to chat



jQuery(".percentage").change(function(){


	// var lead_firstname_chat = jQuery('#firstname').val();
	// var lead_email_addres_chat = jQuery('#email_address').val();
	// //console.log(lead_email_addres_chat);

	// $zopim( function() {

 //           $zopim.livechat.setName(lead_firstname_chat);
 //           $zopim.livechat.setEmail(lead_email_addres_chat);
 //           //$zopim.livechat.setEmail('mustafa@gmail.com');


	// });


	//Code for Lead Form

	var lead_email_addres = jQuery('#email_address').val();
	var lead_firstname = jQuery('#firstname').val();

	if(lead_email_addres){
		jQuery('#NewFrame').contents().find('#sf_email').val(lead_email_addres);
	}

	if (lead_firstname) {
		jQuery('#NewFrame').contents().find('#sf_first_name').val(lead_firstname);
	}
	var veh_contact_num1 = jQuery('#contact_num1').val();
	var veh_contact_num2 = jQuery('#contact_num2').val();
	//console.log(veh_contact_num2);
	var veh_contact_numF = veh_contact_num1+veh_contact_num2;
	
	if (veh_contact_numF) {
		jQuery('#NewFrame').contents().find('#sf_phone').val(veh_contact_numF);
	}

	var brand_newchk = jQuery('input[name=new_check]:checked').val();
	if (brand_newchk) {
		jQuery('#NewFrame').contents().find('#sf_street').val(brand_newchk);
	}

	var model_year_switch = jQuery('#model_year option:selected').val();
	if (model_year_switch) {
		jQuery('#NewFrame').contents().find('#sf_city').val(model_year_switch);
	}

	var vehicle_model_detail = jQuery('#vehicle_model_detail option:selected').val();
	if (vehicle_model_detail) {
		jQuery('#NewFrame').contents().find('#sf_state').val(vehicle_model_detail);
	}

	var updatedcarval1 = jQuery( "#vehicle_val" ).val();
	if (updatedcarval1) {
		jQuery('#NewFrame').contents().find('#sf_zip').val(updatedcarval1);
	}

	var current_ins_notexp_val = jQuery('input[name=mycurrentins_chk]:checked').val();

	//Vehicle Brand
	var vehicle_brand1 = jQuery('#vehicle_brand option:selected').val();
	
	//My Current Policy is Comprehensive Insurance
	var current_policy_comp_ins1 = jQuery('input[name=mycurrentpolicy_chk]:checked').val();
	
	//My Vehicle is GCC Specification / Not Modified
	var vehicle_isgcc_chk = jQuery('input[name=vehicle_isgcc_chk]:checked').val();

	//My Vehicle is Not for Commercial Purpose
	var vehiclecommercial_chk = jQuery('input[name=vehiclecommercial_chk]:checked').val();

	//I Need My Vehicle Replacement / Rental Car
	var veh_replacement_chk1 = jQuery('input[name=veh_replacement_chk]:checked').val();

	//I Need Personal Accident Cover for Driver
	var per_accidentcover_chk1 = jQuery('input[name=per_accidentcover_chk]:checked').val();
	
	//I Need Personal Accident Cover for Passengers
	var pa_cover_pass = jQuery('#pa_cover_pass option:selected').val();

	//Your Nationality is
	var nationality1 = jQuery('#nationality option:selected').val();
	if (nationality1) {
		jQuery('#NewFrame').contents().find('#sf_country').val(nationality1);
	}

	//Your Date of Birth
	var date_of_birth = jQuery('#datepicker1').val();

	//Add Info to the title field.	
	// var full_data1 = "Current Insurance is Not Expired: "+current_ins_notexp_val+"  ---  Car Value: "+updatedcarval1;
	// var full_data2 =" --- Vehicle Model: "+vehicle_model_detail+" --- Vehicle Model Year: "+model_year_switch;
	// var full_data3 =" --- Vehicle Brand New: " +brand_newchk+" --- Model Detail: " +vehicle_model_detail;
	// var full_data4 =" --- Current Ins exp: " +current_ins_notexp_val+" --- Comp Insurance: " +current_policy_comp_ins1;
	// var full_data5 =" --- GCC Specification: " +vehicle_isgcc_chk+" --- Not Commercial Purpose: " +vehiclecommercial_chk;
	// var full_data6 =+" --- Need Veh Rep: " +veh_replacement_chk1+" --- Need Per Acc Cover: " +per_accidentcover_chk1+" --- Need Per Acc Cover: " +pa_cover_pass;
	
	var full_data1 = vehicle_brand1+" "+model_year_switch+" "+vehicle_model_detail+" " +nationality1;
	var full_data2 = "Cur Ins exp: " +current_ins_notexp_val+"- Comp Insurance: " +current_policy_comp_ins1+"- GCC Spec: " +vehicle_isgcc_chk+"- Not Comm Pur:" +vehiclecommercial_chk;
	var full_data3 = "- N Veh Rep: " +veh_replacement_chk1+"-- N Per Acc Cover: " +per_accidentcover_chk1+"-- Per Acc Cover: " +pa_cover_pass+"Cur Ins is Not Expired: "+current_ins_notexp_val;
	

	jQuery('#NewFrame').contents().find('#sf_title').val(full_data1);
	jQuery('#NewFrame').contents().find('#sf_company').val(full_data2);
	jQuery('#NewFrame').contents().find('#sf_website').val(full_data3);
	//jQuery('#NewFrame').contents().find('#sf_city').val(full_data4);
	//jQuery('#NewFrame').contents().find('#sf_state').val(full_data5);
	//jQuery('#NewFrame').contents().find('#sf_zip').val(full_data6);
	


	//Submit Jquery
	jQuery( "#years_uaedrivinglicence" ).change(function() {
		jQuery('#NewFrame').contents().find('.w2linput.submit').trigger('click');
	});






	//Code for lead form Ends.


jQuery(this).addClass("changed");
var change_count = jQuery('.changed').length;
var currentval = parseFloat(change_count);
currentval1 = currentval * 2.94;
currentval11 = currentval1.toFixed(1);
jQuery("#percentage-text").html(currentval11+'%');
var circlefill = 'p'+currentval1;

////////console.log("circlefill..."+circlefill);

jQuery( "#progress:last" ).addClass( circlefill );
});

//jQuery('[data-toggle="tooltip"]').tooltip();
jQuery("#btn_step1").addClass("active_step");


jQuery("#btn_step1").click(function(){
jQuery("#btn_step2").removeClass("active_step");
jQuery("#btn_step1").addClass("active_step");
});


jQuery("#btn_step2").click(function(){
jQuery("#btn_step1").removeClass("active_step");
jQuery("#btn_step2").addClass("active_step");
});


jQuery("#btn_nextstep2inner").click(function(){
jQuery("#btn_step1").removeClass("active_step");
jQuery("#btn_step2").removeClass("active_step");
});


var basePrice = 3.00;
var width;

//jQuery( "#ins_submodel" ).hide();

jQuery( "#step2" ).hide();
jQuery( "#step3" ).hide();
jQuery( "#step4" ).hide();
jQuery("#calCheckout_url").hide();


jQuery( "#btn_step2" ).click(function() {
jQuery( "#step1" ).hide();
jQuery( "#step2" ).show();
});


jQuery( "#btn_nextstep1inner" ).click(function() {
jQuery( "#step1" ).hide();
jQuery( "#step2" ).show();
jQuery("#btn_step1").removeClass("active_step");
jQuery("#btn_step2").addClass("active_step");

//var vehicle_brand = jQuery('#vehicle_brand option:selected').val();
//var selected_modelYear = jQuery('#model_year option:selected').val();
//var vehicle_model_detail = jQuery('#vehicle_model_detail option:selected').val();
//var selected_vehCat = jQuery('#vehicle_cat option:selected').val();
});


jQuery( "#btn_step1" ).click(function() {
jQuery( "#step2" ).hide();
jQuery( "#step1" ).show();
});

//jQuery(".calculate").change(function () {
//	show_minmax();
//});

jQuery("#model_year").change(function () {
	jQuery("#vehicle_model_detail").prop('disabled', false);
	var model_year_switch = jQuery('#model_year option:selected').val();
	localStorage.setItem("selected_model_year",model_year_switch);

	if(model_year_switch == "2018" || model_year_switch == "2019"){
		jQuery("#fr3").prop('checked', true);
		jQuery('#dor_row').addClass( "hide_this" );
	}else{
		jQuery("#fr3").prop('checked', false);
	}
	show_models();
	set_reg_year(model_year_switch);
});


	function set_reg_year(model_year_switch){
		jQuery("#vehicle_reg_date3").val(model_year_switch);

	}

jQuery("#vehicle_reg_date3").change(function () {
var vehicle_firstReg_dateYear = jQuery('#vehicle_reg_date3 option:selected').val();
////////console.log("vehicle_firstReg_dateYear"+vehicle_firstReg_dateYear);

if(vehicle_firstReg_dateYear == "2018"){
jQuery("#fr3").prop('checked', true);
jQuery('#dor_row').addClass( "hide_this" );
}
});


jQuery("#vehicle_brand").change(function () {
jQuery("#model_year").prop('disabled', false);
show_models();
});


function show_models(){
var step_1datam = <?php echo $json_form_dataset; ?>;
var selected_modelYear = jQuery('#model_year option:selected').val();
var vehicle_brand_filter = jQuery('#vehicle_brand option:selected').val();
var final_array = [];
var final_array2 =[];

jQuery.each( step_1datam, function( m, brand_filter ) {
if(brand_filter[0] == vehicle_brand_filter && brand_filter[1] == selected_modelYear){
final_array2.push(brand_filter[2]);
}

});

//////console.log(final_array2);
//Set to option Make

jQuery('.modeloption').html('');
jQuery.each( final_array2, function( i, veh_model_set ) {
jQuery('.modeloption').append('<option class="options" value="'+veh_model_set+'">'+veh_model_set+'</option>');
});
show_minmax();
}


function show_minmax(){
//Step1 and selected Data arrays
var step_1datam = <?php echo $json_form_dataset; ?>;

//Get selected values

var vehicle_brand = jQuery('#vehicle_brand option:selected').val();
var selected_modelYear = jQuery('#model_year option:selected').val();

//Show Model

/* 	var selected_modelYear = jQuery('#model_year option:selected').val();
var vehicle_brand_filter = jQuery('#vehicle_brand option:selected').val();
var final_array = [];
var final_array2 =[];

jQuery.each( step_1datam, function( m, brand_filter ) {
if(brand_filter[0] == vehicle_brand_filter && brand_filter[1] == selected_modelYear){
final_array2.push(brand_filter[2]);

}

});

//////console.log(final_array2);

//Set to option Make

jQuery('.modeloption').html('');
jQuery.each( final_array2, function( i, veh_model_set ) {
jQuery('.modeloption').append('<option class="options" value="'+veh_model_set+'">'+veh_model_set+'</option>');

});

*/

//Show Model Option End
//Year Selection Logic

var selected_modelYear = jQuery('#model_year option:selected').val();
var currentYear = (new Date).getFullYear();
jQuery('.veh_brand_new').click(function () {
var veh_brandnewcheck = jQuery(this).val();

if(veh_brandnewcheck == "Yes"){
////////////////console.log("Checked");
selected_modelYear = currentYear;
////////////////console.log("selected_modelYear..."+selected_modelYear);

}

else if(veh_brandnewcheck == "No"){
////////////////console.log("Un Checked");
selected_modelYear = jQuery('#model_year option:selected').val();
}

});


var vehicle_model_detail = jQuery('#vehicle_model_detail option:selected').val();
var selected_valarraym = [vehicle_brand, selected_modelYear, vehicle_model_detail];
var ins_selected_valarraym = vehicle_brand+selected_modelYear+vehicle_model_detail;

jQuery.each( step_1datam, function( i, valuem ) {
value_min_max1 = valuem[0];
value_min_max2 = valuem[1];
value_min_max3 = valuem[2];
value_min_max4 = valuem[3];
value_min_max5 = valuem[4];
value_min_max6 = valuem[5];
rt_valuem = value_min_max1+value_min_max2+value_min_max3;


if(ins_selected_valarraym == rt_valuem){
var rt_min_valm = valuem[3];
var rt_max_valm = valuem[4];

//////////////console.log("rt_min_valm..."+rt_min_valm);
localStorage.setItem("selected_cat",value_min_max6);


//Display Min and Max Values.
document.getElementById("item_minval").innerHTML = rt_min_valm;
document.getElementById("item_maxval").innerHTML = rt_max_valm;

//document.getElementById("vehicle_submodel").innerHTML = rt_min_valm;

jQuery( "#vehicle_val" ).val( rt_min_valm );
jQuery( "#cars_worth" ).html(rt_min_valm);
}

});

}

//Disable Car Value field on Model Changed

jQuery("#vehicle_model_detail").change(function () {
jQuery("#vehicle_val").prop('disabled', false);
jQuery("#car_value_details").show();
show_minmax();
});

//Limit Value to Min Max

jQuery("#vehicle_val").change(function () {
var updatedcarval1 = jQuery( "#vehicle_val" ).val();
var rt_min_get1 = document.getElementById("item_minval").innerHTML;
var rt_max_get1 = document.getElementById("item_maxval").innerHTML;

var updatedcarval = updatedcarval1.replace( /[^\d.]/g, '' );
var rt_min_get = rt_min_get1.replace( /[^\d.]/g, '' );
var rt_max_get = rt_max_get1.replace( /[^\d.]/g, '' );

//////console.log("updatedcarval.."+updatedcarval);
////////console.log("rt_min_get.."+rt_min_get);
////////console.log("rt_max_get.."+rt_max_get);

if(updatedcarval > rt_max_get){
updatedcarval = rt_max_get;
jQuery( "#vehicle_val" ).val( rt_max_get );
//////console.log("Changed value Greater then ..."+updatedcarval);
}

else if(updatedcarval < rt_min_get){
updatedcarval = rt_min_get;
jQuery( "#vehicle_val" ).val( rt_min_get );
//////console.log("Changed value less then..."+updatedcarval);
}
});

//Default value for Personal Accident Cover Driver
localStorage.setItem("pa_cover_driver_check", "Yes");
localStorage.setItem("outside_uae_cov_value", "FALSE");
localStorage.setItem("rent_car_check_val", "No");
localStorage.setItem("ncd_check","false");
jQuery("#car_ncd_details").hide();
//NCD Check and alert box
jQuery( "#years_ncd" ).change(function() {
var ncd_years = jQuery('#years_ncd option:selected').val();
localStorage.setItem("ncd_check_years",ncd_years);

if(ncd_years != 0){
//jQuery("#popup_dangerNCD").show();
jQuery("#car_ncd_details").show();
localStorage.setItem("ncd_check","true");
}else if(ncd_years == 0){
//jQuery("#popup_dangerNCD").hide();
jQuery("#car_ncd_details").hide();
localStorage.setItem("ncd_check","false");
}
});


function get_companies(){
//Get selected values
var vehicle_brand = jQuery('#vehicle_brand option:selected').val();
//var selected_modelYear = jQuery('#model_year option:selected').val();
jQuery('#cars_brand').html(vehicle_brand);

//Year Selection Logic

var selected_modelYear = jQuery('#model_year option:selected').val();
var currentYear2 = (new Date).getFullYear();
jQuery('.veh_brand_new').click(function () {
var veh_brandnewcheck = jQuery(this).val();


if(veh_brandnewcheck == "Yes"){
var selected_modelYear = currentYear2;
////////////////console.log("selected_modelYear2..."+selected_modelYear);
}

else if(veh_brandnewcheck == "No"){
////////////////console.log("Un Checked");
var selected_modelYear = jQuery('#model_year option:selected').val();
}
});


var mycurrentpolicy_chk = jQuery('input[name=mycurrentpolicy_chk]:checked').val();
localStorage.setItem("mycurrentpolicy_chk_e",mycurrentpolicy_chk);
//////console.log("mycurrentpolicy_chk.."+mycurrentpolicy_chk);


var vehicle_model_detail = jQuery('#vehicle_model_detail option:selected').val();
jQuery('#cars_model').html(vehicle_model_detail);

var selected_vehicle_repair = jQuery('#per_agency_repair option:selected').val();
var selected_vehicle_modified = jQuery('#vehicle_modified').is(':checked');
var selected_outside_uae = jQuery('#outside_uae').is(':checked');
var selected_roadside_assistance = jQuery('#roadside_assistance').is(':checked');
var selected_ins_rentACar = jQuery('#ins_rentACar').is(':checked');
var need_per_acc_cover_driver;
var acc_cover_passangers = "Yes";
var rent_a_car_val;
var outside_uae_cov_val;


jQuery('#pa_cover_driver_id input').on('change', function() {
var need_per_acc_cover_driver = jQuery('input[name=per_accidentcover_chk]:checked').val();
localStorage.setItem("pa_cover_driver_check", need_per_acc_cover_driver);
});

need_per_acc_cover_driver = localStorage.getItem("pa_cover_driver_check");
if(need_per_acc_cover_driver == "Yes"){
var need_per_acc_cover_driver = "Yes";
}else{
var need_per_acc_cover_driver = "No";
}

jQuery('#rent_a_car_id input').on('change', function() {
var rent_a_car_val = jQuery('input[name=veh_replacement_chk]:checked').val();
localStorage.setItem("rent_car_check_val", rent_a_car_val);
});

rent_car_val_get = localStorage.getItem("rent_car_check_val");
if(rent_car_val_get == "Yes"){
var rent_car_check = rent_car_val_get;
}
else{
var rent_car_check = "No";
}


jQuery('#mycurrentpolicy input').on('change', function() {
var mycurrentpolicy_chk1 = jQuery('input[name=mycurrentpolicy_chk]:checked').val();
	if(mycurrentpolicy_chk1 =="Yes"){
	jQuery( "#popup_dangermain1111" ).hide();
	}
	else if(mycurrentpolicy_chk1 =="No"){
	jQuery( "#popup_dangermain1111" ).show();
	}
});






jQuery('#outside_uae_coverage_id input').on('change', function() {
var outside_uae_cov_val = jQuery('input[name=outsideuaeCover_chk]:checked').val();
localStorage.setItem("outside_uae_cov_value", "TRUE");
////console.log("outside_uae_cov_val.."+outside_uae_cov_val);
});


outside_uae_cov_val = localStorage.getItem("outside_uae_cov_value");

if(outside_uae_cov_val == "TRUE"){
	var outside_uae_cov_inner = "TRUE";
}else{
	var outside_uae_cov_inner = "FALSE";
}

var pa_cover_passangers = jQuery('#pa_cover_pass option:selected').val();
////console.log("pa_cover_passangers.."+pa_cover_passangers);
localStorage.setItem("pa_cover_pass_check",pa_cover_passangers);

var ideal_date    = "01/1/1993"
var ideal_date_21    = "01/1/1996"
var age_less_25 = jQuery('#datepicker1').val();
//////////console.log("age_less_25.."+age_less_25);

//Select Options End
//get selected Category form Vehicle Info

var selected_category = localStorage.getItem("selected_cat");
////////console.log("selected_category V info....."+selected_category);
// does not have a value selected Submodel
var selected_valarray = [vehicle_brand, selected_modelYear, vehicle_model_detail];

//Step1 and selected Data arrays
var step_1data = <?php echo $json_form_dataset; ?>;
var step_dataComp = <?php echo $json_form_dataComp; ?>;
var rt_min_val = 0;
jQuery.each( step_1data, function( i, value ) {

value0 = value[0];
value1 = value[1];
value2 = value[2];
value3 = value[3];
value4 = value[4];
value5 = value[5];
value6 = value[6];

selected_valarray0 = selected_valarray[0];
selected_valarray1 = selected_valarray[1];
selected_valarray2 = selected_valarray[2];

//Selected Options array
rt_selected_valarray = selected_valarray0+selected_valarray1+selected_valarray2+selected_category;
rt_value = value0+value1+value2+value5;
////console.log("rt_selected_valarray.."+rt_selected_valarray);
////console.log("rt_value.."+rt_value);
if(rt_selected_valarray == rt_value){
//Getting value from user Entered Field.
var updatedcarval1 = jQuery( "#vehicle_val" ).val();
if(updatedcarval1){
rt_min_val = updatedcarval1;
}
//rt_min_val = value[5];
rt_max_val = value[4];
}
});

var all_companies = [];
jQuery.each( step_dataComp, function( j, valuecomp ) {
var min_valcomp = valuecomp[6];
var max_valcomp = valuecomp[7];

if(min_valcomp){
min_valcomp1 = parseFloat(min_valcomp.replace(/\,/g,''));
}

if(min_valcomp){
max_valcomp1 = parseFloat(max_valcomp.replace(/\,/g,''));
}

if(rt_min_val){
var rt_min_val1 = parseFloat(rt_min_val.replace(/\,/g,''));
}

if( rt_min_val1>=min_valcomp1&&rt_min_val1<=max_valcomp1){

var per_agency_repair_gr_val;
var per_agency_repair_pw_val;
var per_agency_repair_ia_val;

//localStorage.setItem("per_agency_repair_gr_val","Garage Repair");
localStorage.setItem("per_agency_repair_pw_val","Standard Wrokshop");
localStorage.setItem("per_agency_repair_ia_val","false");

jQuery( ".personal_agency_repair_pw" ).prop( "disabled", false );

var get_selectedYear_agRepair = jQuery('#model_year option:selected').val();
////////console.log("Selected year option Check.."+get_selectedYear_agRepair);

if(get_selectedYear_agRepair >= 2014){
jQuery(".personal_agency_repair_pw").prop('checked', true);
var per_agency_repair_pw_val = jQuery('input[name=personal_agency_repair_pw]:checked').val();
localStorage.setItem("per_agency_repair_pw_val", per_agency_repair_pw_val);
}

if(get_selectedYear_agRepair < 2014){
//jQuery( ".personal_agency_repair_pw" ).prop( "disabled", false );
jQuery( ".personal_agency_repair_ia" ).prop( "disabled", true );
//jQuery( ".personal_agency_repair_gr" ).prop( "disabled", false );
}
if(get_selectedYear_agRepair < 2016){
jQuery( ".personal_agency_repair_ia" ).prop( "disabled", true );
//jQuery( ".personal_agency_repair_pw" ).prop( "disabled", false );
//jQuery( ".personal_agency_repair_gr" ).prop( "disabled", false );
}
if(get_selectedYear_agRepair == 2016 || get_selectedYear_agRepair == 2017 || get_selectedYear_agRepair == 2018){
jQuery( ".personal_agency_repair_ia" ).prop( "disabled", false );
//jQuery( ".personal_agency_repair_pw" ).prop( "disabled", true );
//jQuery( ".personal_agency_repair_gr" ).prop( "disabled", true );
}

if(get_selectedYear_agRepair == 2017 || get_selectedYear_agRepair == 2018 ){
jQuery(".personal_agency_repair_ia").prop('checked', true);
//jQuery(".personal_agency_repair_gr").prop('checked', true);
//jQuery(".personal_agency_repair_pw").prop('checked', true);

var per_agency_repair_pw_val = jQuery('input[name=personal_agency_repair_pw]:checked').val();
localStorage.setItem("per_agency_repair_pw_val", per_agency_repair_pw_val);

var per_agency_repair_ia_val = jQuery('input[name=personal_agency_repair_ia]:checked').val();
localStorage.setItem("per_agency_repair_ia_val", per_agency_repair_ia_val);
}

/* jQuery('#per_agency_repair_gr input').on('change', function() {
var per_agency_repair_gr_val = jQuery('input[name=personal_agency_repair_gr]:checked').val();
localStorage.setItem("per_agency_repair_gr_val", per_agency_repair_gr_val);
}); */

/* jQuery('#per_agency_repair_pw input').on('change', function() {
var per_agency_repair_pw_val = jQuery('input[name=personal_agency_repair_pw]:checked').val();
localStorage.setItem("per_agency_repair_pw_val", per_agency_repair_pw_val);
});

jQuery('#per_agency_repair_ia input').on('change', function() {
var per_agency_repair_ia_val = jQuery('input[name=personal_agency_repair_ia]:checked').val();
localStorage.setItem("per_agency_repair_ia_val", per_agency_repair_ia_val);

});	 */

//per_agency_repair_gr_val1 = localStorage.getItem("per_agency_repair_gr_val");
per_agency_repair_pw_val1 = localStorage.getItem("per_agency_repair_pw_val");
per_agency_repair_ia_val1 = localStorage.getItem("per_agency_repair_ia_val");

/* if(per_agency_repair_gr_val1){
var garrage_repair = per_agency_repair_gr_val1;
}else{
var garrage_repair = '';
} */

if(per_agency_repair_pw_val1){
var premier_workshop = per_agency_repair_pw_val1;
}else{
var premier_workshop = '';
}

if(per_agency_repair_ia_val1){
var inside_agency = per_agency_repair_ia_val1;
}else{
var inside_agency = '';
}

//localStorage.setItem("garrage_repair",garrage_repair);
localStorage.setItem("premier_workshop",premier_workshop);
localStorage.setItem("inside_agency",inside_agency);

var cat_selected = localStorage.getItem("selected_cat");;
var cat_compdata = valuecomp[5];
////console.log("cat_compdata .."+cat_compdata);
////console.log("cat_selected .."+cat_selected);

if(cat_selected == cat_compdata){
////console.log(" garrage_repair.."+garrage_repair);
////console.log(" premier_workshop.."+premier_workshop);
////console.log(" inside_agency.."+inside_agency);


//if(valuecomp[12] == premier_workshop || valuecomp[12] == inside_agency){

//valuecomp_min_val1 = valuecomp[9].replace(/,/g , "");

veh_value_get_box = jQuery( "#vehicle_val" ).val();
valuecomp_min_val1 = veh_value_get_box.replace(/,/g , "");
var valuecomp_rate = valuecomp[8].replace("%", "");

////////console.log(" ---------------- ");
////////console.log("Category.."+valuecomp[5]);
////////console.log("before_percentage.."+valuecomp_rate);

var percentage_toadd = valuecomp_min_val1 / 100;
var percentage_toadd_final = percentage_toadd * valuecomp_rate;
var minimum_value_get = valuecomp[9].replace(",", "");

if(percentage_toadd_final < minimum_value_get){
percentage_toadd_final = minimum_value_get;
}

cal_rate = parseFloat(percentage_toadd_final);
localStorage.setItem("cal_rate_afterper",cal_rate);

////////console.log("cal_rate_afterper Value",+cal_rate);

//////////console.log("After Percentage cal_rate....."+cal_rate);
//Gettig Rate After Applying rate in Percentage
var rate_afterpercentage = localStorage.getItem("cal_rate_afterper");
var years_uaedriving_lic1 = jQuery('#years_uaedrivinglicence option:selected').val();

////console.log("Before Licence cal_rate.."+cal_rate);

//Years UAE Driv Licence age
if(years_uaedriving_lic1 == "66"){
year_uae_driving_lic = valuecomp[18].replace ( /[^\d.]/g, '' );

if(jQuery.isNumeric(year_uae_driving_lic)){
year_uae_driving_licence_var = (cal_rate/100)*year_uae_driving_lic;
cal_rate = parseInt(rate_afterpercentage) + parseInt(year_uae_driving_licence_var);
}else{
cal_rate = cal_rate;
}
}

////console.log("After Licence cal_rate.."+cal_rate);

//If age less then 25 years
if(new Date(age_less_25) > new Date(ideal_date)){
age_less_then25 = valuecomp[19].replace ( /[^\d.]/g, '' );

if(jQuery.isNumeric(age_less_then25)){
age_less_then25_var = (cal_rate/100) * parseFloat(age_less_then25);
var cal_rate2 = parseFloat(rate_afterpercentage) + parseFloat(age_less_then25_var);
cal_rate = cal_rate2;
}
else{
cal_rate = cal_rate;
}
}

////console.log("After age  cal_rate.."+cal_rate);

//Per Acc Cover Driver
cal_personal_accident_c_driver = valuecomp[13];
if(need_per_acc_cover_driver == "Yes"){
cal_rate = cal_rate + parseFloat(cal_personal_accident_c_driver);
localStorage.setItem("pacover_driver_checkout","120 AED");
}else{
cal_rate = cal_rate;
localStorage.setItem("pacover_driver_checkout","None");
}

//Rent A Car
cal_rent_a_car = valuecomp[15];
if(rent_car_check == "Yes"){
cal_rate = cal_rate + parseFloat(cal_rent_a_car);
////////console.log("rent_a_car_final..."+cal_rate);
}else{
cal_rate = cal_rate;
}

//Outside UAE Coverage
out_cov_val = valuecomp[17];

////console.log("out_cov_val.."+out_cov_val);
////console.log("outside_uae_cov_inner.."+outside_uae_cov_inner);

if(outside_uae_cov_inner == "TRUE"){
//////console.log("outside_uae_cov_Option_Before..."+outside_uae_cov_inner);
//////console.log("out_cov_value_Before..."+out_cov_val);
////console.log("cal_rate_Before..."+cal_rate);

	
	// if(out_cov_val == "TRUE"){
	// 	cal_rate = parseFloat(cal_rate) + 0.00;
	// }
	// else if(out_cov_val == "FALSE"){
	// 	cal_rate = parseFloat(cal_rate);
	// }
}else{
cal_rate = cal_rate;
}

////console.log("outside_uae_cov_After..."+cal_rate);

//Per Acc Cover Passengers
if(pa_cover_passangers >= 1){

		if(pa_cover_passangers == 1){
		cal_rate = cal_rate + parseFloat(valuecomp[14]);
		//////////////console.log("cal_rate_passanger1..."+cal_rate);
		}else if(pa_cover_passangers == 2){

		var passangers2 = parseFloat(valuecomp[14]) + parseFloat(valuecomp[14]);

		cal_rate = parseInt(cal_rate) + passangers2;

		//////////////console.log("cal_rate_passanger2..."+cal_rate);

		}else if(pa_cover_passangers == 3){
		var passangers2 = parseFloat(valuecomp[14]) + parseFloat(valuecomp[14]) + parseFloat(valuecomp[14]);
		cal_rate = parseInt(cal_rate) + passangers2;
		//////////////console.log("cal_rate_passanger3..."+cal_rate);

		}else if(pa_cover_passangers == 4){
		var passangers2 = parseFloat(valuecomp[14]) + parseFloat(valuecomp[14]) + parseFloat(valuecomp[14]) + parseFloat(valuecomp[14]);
		cal_rate = parseInt(cal_rate) + passangers2;

		//////////////console.log("cal_rate_passanger4..."+cal_rate);

		}else if(pa_cover_passangers == 5){
		var passangers2 = parseFloat(valuecomp[14]) + parseFloat(valuecomp[14]) + parseFloat(valuecomp[14]) + parseFloat(valuecomp[14]) + parseFloat(valuecomp[14]);
		cal_rate = parseInt(cal_rate) + passangers2;

		//////////////console.log("cal_rate_passanger5..."+cal_rate);

		}else if(pa_cover_passangers == 6){
		var passangers2 = parseFloat(valuecomp[14]) + parseFloat(valuecomp[14]) + parseFloat(valuecomp[14]) + parseFloat(valuecomp[14]) + parseFloat(valuecomp[14]) + parseFloat(valuecomp[14]);

		cal_rate = parseInt(cal_rate) + passangers2;

		//////////////console.log("cal_rate_passanger6..."+cal_rate);

		}else if(pa_cover_passangers == 7){
		var passangers2 = parseFloat(valuecomp[14]) + parseFloat(valuecomp[14]) + parseFloat(valuecomp[14]) + parseFloat(valuecomp[14]) + parseFloat(valuecomp[14]) + parseFloat(valuecomp[14]) + parseFloat(valuecomp[14]);

		cal_rate = parseInt(cal_rate) + passangers2;

		//console.log("cal_rate_passanger7..."+cal_rate);

		}else{
		cal_rate = cal_rate;
		}

		// localStorage.setItem("pa_coverpassangersChkout",passangers2);
		//cal_rate = parseFloat(cal_rate) * parseFloat(valuecomp[14]);
}
////console.log("Before ncd_added.."+cal_rate);

var cal_rate1;
var cal_rate2;
//NCD 10% add
ncd_get_val = localStorage.getItem("ncd_check");
//////console.log("ncd_get_val..",ncd_get_val);
if(ncd_get_val == "true"){
var ammount_toadd = 10/100;
cal_rate1 = cal_rate * parseFloat(ammount_toadd);
cal_rate2 = parseFloat(cal_rate1) + cal_rate;
//////console.log("cal_rate2 ncd_added..",cal_rate2);
}
else{
cal_rate = cal_rate;
}
//cal_rate = cal_rate2;
//////console.log("After ncd_added..",cal_rate);
//////////console.log("cal_rateBeforeDiscount.."+cal_rate);
var discount_apply = valuecomp[2].replace(/\%/g, "");
/////////console.log("discount_apply..."+discount_apply);
var discount_apply2 = discount_apply/100;
var discount_amount = discount_apply2 * cal_rate;
var price_afterdeduct = cal_rate - discount_amount;

////////console.log("cal_rate bef disc.."+cal_rate);
////////console.log("price_afterdeduct.."+price_afterdeduct);

var len = valuecomp[6].length;

/* 	var apply_vatAmmount1 = valuecomp[3].replace(/\%/g, "");
var percentage_toadd = (parseFloat(price_afterdeductB) / 100) * parseFloat(apply_vatAmmount1);
var price_afterdeductB = price_afterdeductB + parseFloat(percentage_toadd);
var price_afterdeduct = price_afterdeductB.toFixed(2);

//////console.log("price_after VAT Deduct.."+price_afterdeduct);
*/



all_companies.push({
compname: valuecomp[0],
discounts: valuecomp[2],
ondamage: valuecomp[16],
personal_accident_CFD: valuecomp[13],
loss_of_pb: valuecomp[28],
third_partylimit: valuecomp[21],
accident_cforpassangers: valuecomp[14],
offroad_desrec: valuecomp[24],
windscreen_dmage: valuecomp[22],
em_med_expence: valuecomp[25],
amb_cover: valuecomp[26],
rep_of_locks: valuecomp[31],
v_parking_theft: valuecomp[30],
car_reg_renewal: valuecomp[33],
third_p_limit: valuecomp[21],
death_bodyinjury: valuecomp[27],
natural_clamity_cover: valuecomp[32],
fire_and_theft: valuecomp[29],
repair_dent: valuecomp[23],
compclass: valuecomp[1],
price_afterdisc: price_afterdeduct,
price_beforedisc: cal_rate,
gcc_spec: valuecomp[34],
vehicle_cat: valuecomp[5],
repair_cond: valuecomp[12],
minimum_val: valuecomp[6],
maximum_val: valuecomp[7],
rate: valuecomp[8],
minimum: valuecomp[9],
dl_less_than_year: valuecomp[18],
less_than_year: valuecomp[19],
outside_UAE_coverage: valuecomp[17],
pa_driver: valuecomp[13],
rs_assistance: valuecomp[20],
rent_car: valuecomp[15],

cart_btn: '<a value="'+price_afterdeduct+'" data1="'+price_afterdeduct+'#'+valuecomp[0]+'#'+valuecomp[1]+'#'+valuecomp[2]+'#'+valuecomp[3]+'#'+valuecomp[4]+'#'+valuecomp[5]+'#'+valuecomp[6]+'#'+valuecomp[7]+'#'+valuecomp[8]+'#'+valuecomp[9]+'#'+valuecomp[10]+'#'+valuecomp[11]+'#'+valuecomp[12]+'#'+valuecomp[13]+'#'+valuecomp[14]+'#'+valuecomp[15]+'#'+valuecomp[16]+'#'+valuecomp[17]+'#'+valuecomp[18]+'#'+valuecomp[19]+'#'+valuecomp[20]+'#'+valuecomp[21]+'#'+valuecomp[22]+'#'+valuecomp[23]+'#'+valuecomp[24]+'#'+valuecomp[25]+'#'+valuecomp[26]+'#'+valuecomp[27]+'#'+valuecomp[28]+'#'+valuecomp[29]+'#'+valuecomp[30]+'#'+valuecomp[31]+'#'+valuecomp[32]+'#'+valuecomp[33]+'#'+valuecomp[34]+'" class="pricing-tables-widget-purchase-button add-tocartbtn button_1" href="#">Buy Now</a>'
});
//}
}
}
});

return all_companies;
}

jQuery(".percentage").change(function() {
jQuery('#priceingtable').html('');
all_companies = get_companies();

////console.log(all_companies);
var veh_fullname_filled = jQuery('#firstname').val();
jQuery('#user_filled').html(veh_fullname_filled);

if(all_companies){
var pa_cover_driver_check = localStorage.getItem("pa_cover_driver_check");
var pa_cover_driver_show;
var pa_cover_passangers_selected = localStorage.getItem("pa_cover_pass_check");
var companies_count = all_companies.length;
localStorage.setItem("comp_count",companies_count);
jQuery('#cars_count').html(companies_count);
////////console.log("companies_count..."+companies_count);

jQuery.each( all_companies, function(g, companies1) {

//Checking PA Cover for Passangers
// if(pa_cover_passangers_selected == 1){
// pa_cover_pass_amount = 30;
// }if(pa_cover_passangers_selected == 2){
// pa_cover_pass_amount = 60;
// }if(pa_cover_passangers_selected == 3){
// pa_cover_pass_amount = 90;
// }if(pa_cover_passangers_selected == 4){
// pa_cover_pass_amount = 120;
// }if(pa_cover_passangers_selected == 5){
// pa_cover_pass_amount = 150;
// }if(pa_cover_passangers_selected == 6){
// pa_cover_pass_amount = 180;
// }if(pa_cover_passangers_selected == 7){
// pa_cover_pass_amount = 210;
// }else if(pa_cover_passangers_selected == 0){
// pa_cover_pass_amount = "None";
// }

var pa_cover_pass_amount = parseFloat(companies1.accident_cforpassangers) * parseFloat(pa_cover_passangers_selected);
////console.log(pa_cover_pass_amount);
////console.log(companies1.accident_cforpassangers);


jQuery('#pa_cover_passangers').html(pa_cover_pass_amount);

//Policy Email Template
jQuery('#driv_acc_coverpassanger_e').html(pa_cover_pass_amount);

var rent_car_check1 = localStorage.getItem("rent_car_check_val");
if(rent_car_check1 == 'Yes'){
rent_car_check2 = 'True';
}else{
rent_car_check2 = 'false';
}

var outside_uae_cov_comp = companies1.outside_UAE_coverage;
////console.log("outside_uae_cov_comp.."+outside_uae_cov_comp);
var outside_uae_set;

if(outside_uae_cov_comp == "TRUE"){
	var outside_uae_set = "FREE";
}
else {
	var outside_uae_set = "None";
}

//Checking PA Cover for Driver
if(pa_cover_driver_check == "Yes"){
pa_cover_driver_show = companies1.personal_accident_CFD;
}else{
pa_cover_driver_show = "None";
}

var selected_model_year_filter = localStorage.getItem("selected_model_year");

if(selected_model_year_filter == '2018' || selected_model_year_filter == '2019'){
	console.log("Sel year is 2018 2019");
	console.log(companies1.repair_cond);
	var rep_cnd_ins_agency = "true"; 
}
else{
	var rep_cnd_ins_agency = "false";
}
if (rep_cnd_ins_agency == "true" && companies1.repair_cond == "Inside Agency") {
	console.log("Rep Cond inside Agency setted");


jQuery('#priceingtable').append('<div class="pricing-widget-wrapper mainrow"><div class="pricing-tables-widget pricing-tables-widget-default pricing-tables-widget-blue pricing-tables-widget-dark"><div class="grid-container"><div class="grid-row"><div class="grid-item item-lg-12 item-sm-6 item-xs-marginb30"><div class="grid-row grid-row-collapse"><div class="grid-item item-lg-3 item-xs-12 leftbar"><div class="pricing-tables-widget-header tablerighthead"><div id="companylogo"><p  id="companylogoinner"><span  class="'+companies1.compname.toString().toLowerCase()+'"></span></p></div><div class="pricing-tables-widget-purchase">'+companies1.compname+'</div><div id="compclass"><div class="pricing-tables-widget-purchase compclassname">'+companies1.compclass+'</div></div></div></div><div class="grid-item item-lg-6 item-xs-12"><div class="pricing-tables-widget-section"><div class="grid-row"><div class="grid-item item-lg-6 item-xs-12"><ul class="pricing-tables-widget-divider-res-section"><li><div class="pricing-tables-widget-tooltip pricing-tables-widget-top-left pricing-tables-widget-fixed" data-tooltip="This is the amount shown in your policy schedule which you must pay when you make an at fault claim and cannot be recovered from a third party"></div><i class="icomoon-circle2 iconcircle"></i>Excess/Own-Damage Cover <span class="dataval"> '+companies1.ondamage+'</span></li><li id="chkeckthis11"><div class="pricing-tables-widget-tooltip pricing-tables-widget-top-left pricing-tables-widget-fixed" data-tooltip="Personal Accident for driver is AED-120 Standard Charges"></div><i class="icomoon-circle2 iconcircle"></i><span class="switchlabel">Personal Accident Cover to Driver</span> <span class="dataval">'+pa_cover_driver_show+'</span></li><li><div class="pricing-tables-widget-tooltip pricing-tables-widget-top-left pricing-tables-widget-fixed" data-tooltip="Your Auto Repair Options are depand on Vehicle Model Year. for-example(Inside Agency, Standard Wrokshop)"></div><i class="icomoon-circle2 iconcircle"></i>Car Repair in <span class="dataval"> '+companies1.repair_cond+'</span></li><li><div class="pricing-tables-widget-tooltip pricing-tables-widget-top-left pricing-tables-widget-fixed" data-tooltip="Car rental reimbursement available for an extra 200 Dhs."></div><i class="icomoon-circle2 iconcircle"></i>Replacement Car Rental <span class="dataval"><i class="icomoon-checkmark-circle '+rent_car_check2.toString().toLowerCase()+'"></i></span></li></ul></div><div class="grid-item item-lg-6 item-xs-12"><ul><li><div class="pricing-tables-widget-tooltip pricing-tables-widget-top-left pricing-tables-widget-fixed" data-tooltip="This will cover property damages when you are legally at fault in an accident"></div><i class="icomoon-circle2 iconcircle"></i>Third Party Limit <span class="dataval">'+companies1.third_partylimit+'</span></li><li><div class="pricing-tables-widget-tooltip pricing-tables-widget-top-left pricing-tables-widget-fixed" data-tooltip="Personal Accident for Passengers Coverage Other than the Family Members is AED-30 Per Person"></div><i class="icomoon-circle2 iconcircle"></i><span class="switchlabel">Accident Cover for Passengers</span><span class="dataval"> '+pa_cover_pass_amount+'</span></li><li><div class="pricing-tables-widget-tooltip pricing-tables-widget-top-left pricing-tables-widget-fixed" data-tooltip="Out-Side UAE Coverage infrmation"></div><i class="icomoon-circle2 iconcircle"></i>OutSide UAE Coverage <span class="dataval">'+outside_uae_set+'</span></li><li><div class="pricing-tables-widget-tooltip pricing-tables-widget-top-left pricing-tables-widget-fixed" data-tooltip="This will provide you off road assistance on condition that the car is on certain metres away from the paved road"></div><i class="icomoon-circle2 iconcircle"></i>Off-Road & Desert Recovery <span class="dataval"><i class="icomoon-checkmark-circle '+companies1.offroad_desrec.toString().toLowerCase()+'"></i></span></li></ul></div></div><p class="pricing-tables-widget-purchase seemoredetails"><a href="#demo'+g+'" class="pricing-tables-widget-purchase seemoretoggle">see more details</a><i class="icomoon-menu-open2 seemoredetailsicon"></i></p> </div></div><div class="grid-item item-lg-3 item-xs-12"><div class="pricing-tables-widget-footer ptablefooter"><p class="pricing-tables-widget-top-left rightdiscount">Save '+companies1.discounts+'</p><h3 class="item-xs-marginb0"><strike>'+companies1.price_beforedisc+'</strike></h3><p>Total Price Are</p><h1 class="item-xs-marginb0"><span class="dataval1">'+companies1.price_afterdisc+'</span> AED</h1><div class="pricing-tables-widget-price">'+companies1.price_afterdisc+' AED</div>'+companies1.cart_btn+'</div></div></div></div></div><div id="demo'+g+'" class="" style="display:none;"><div class="pricing-tables-widget-section moredetail"><div class="grid-row"><div class="grid-item item-sm-4"><ul class="pricing-tables-widget-divider-res-section"><li><div class="pricing-tables-widget-tooltip pricing-tables-widget-top-left pricing-tables-widget-fixed" data-tooltip="Road Side Assistance either free or either there will be charge according to the company we will add the package since most of the companies are charging the road side Assistance"></div><i class="icomoon-circle2 iconcircle"></i>Road Side Assistance <span class="dataval"><i class="icomoon-checkmark-circle '+companies1.rs_assistance.toString().toLowerCase()+'"></i></span></li><li><div class="pricing-tables-widget-tooltip pricing-tables-widget-top-left pricing-tables-widget-fixed" data-tooltip="WWindscreen Damage information"></div><i class="icomoon-circle2 iconcircle"></i>Windscreen Damage <span class="dataval">'+companies1.windscreen_dmage+'</span></li><li><div class="pricing-tables-widget-tooltip pricing-tables-widget-top-left pricing-tables-widget-fixed" data-tooltip="This will cover your vehicle for removing dings, creases and dents caused by accidental damage or any perils"></div><i class="icomoon-circle2 iconcircle"></i>Dent Repair <span class="dataval"><i class="icomoon-checkmark-circle '+companies1.repair_dent.toString().toLowerCase()+'"></i></span></li><li><div class="pricing-tables-widget-tooltip pricing-tables-widget-top-left pricing-tables-widget-fixed" data-tooltip="tooltip info"></div><i class="icomoon-circle2 iconcircle"></i>Loss of Personal Belongings <span class="dataval"><i class="icomoon-checkmark-circle '+companies1.loss_of_pb.toString().toLowerCase()+'"></i></span></li></ul></div><div class="grid-item item-sm-4"><ul><li><div class="pricing-tables-widget-tooltip pricing-tables-widget-top-left pricing-tables-widget-fixed" data-tooltip="This will cover you for the cost of emergency medical treatment following an accident per person cost one Accident One Claim per year"></div><i class="icomoon-circle2 iconcircle"></i>Emergency Medical Expenses <span class="dataval">'+companies1.em_med_expence+'</span></li><li><div class="pricing-tables-widget-tooltip pricing-tables-widget-top-left pricing-tables-widget-fixed" data-tooltip="This will cover you for the cost of any service or transport by a recognize ambulance following an accident"></div><i class="icomoon-circle2 iconcircle"></i>Ambulance Cover <span class="dataval"><i class="icomoon-checkmark-circle '+companies1.amb_cover.toString().toLowerCase()+'"></i></span></li><li><div class="pricing-tables-widget-tooltip pricing-tables-widget-top-left pricing-tables-widget-fixed" data-tooltip="Personal Accident benefit cover for driver and passengers up to Dhs. 200,000 each in case of death or total permanent disability of person. (Coverage details available upon request)"></div><i class="icomoon-circle2 iconcircle"></i>Death or Bodily Injury <span class="dataval"><i class="icomoon-checkmark-circle '+companies1.death_bodyinjury.toString().toLowerCase()+'"></i></span></li><li><div class="pricing-tables-widget-tooltip pricing-tables-widget-top-left pricing-tables-widget-fixed" data-tooltip="Car Renewal infrmation"></div><i class="icomoon-circle2 iconcircle"></i>Car Registration / Renewal <span class="dataval"><i class="icomoon-checkmark-circle '+companies1.car_reg_renewal.toString().toLowerCase()+'"></i></span></li></ul></div><div class="grid-item item-sm-4"><ul><li><div class="pricing-tables-widget-tooltip pricing-tables-widget-top-left pricing-tables-widget-fixed" data-tooltip="This will cover your car to any damages caused by a natural peril (i.e. storm, flood, hailstorm etc.)"></div><i class="icomoon-circle2 iconcircle"></i>Natural Calamity Cover <span class="dataval"><i class="icomoon-checkmark-circle '+companies1.natural_clamity_cover.toString().toLowerCase()+'"></i></span></li><li><div class="pricing-tables-widget-tooltip pricing-tables-widget-top-left pricing-tables-widget-fixed" data-tooltip="This will cover your vehicle for accidental fire damage and for damages resulting from attempted theft (i.e. broken windows or stolen radio). This also covers replacement of your car if stolen."></div><i class="icomoon-circle2 iconcircle"></i>Fire and Theft Cover <span class="dataval"><i class="icomoon-checkmark-circle '+companies1.fire_and_theft.toString().toLowerCase()+'"></i></span></li><li><div class="pricing-tables-widget-tooltip pricing-tables-widget-top-left pricing-tables-widget-fixed" data-tooltip="This will cover your vehicle for theft while your car is under the care of a valet service (i.e hotels, restaurants and other businesses)"></div><i class="icomoon-circle2 iconcircle"></i>Valet Parking Theft <span class="dataval"><i class="icomoon-checkmark-circle '+companies1.v_parking_theft.toString().toLowerCase()+'"></i></span> </li><li><div class="pricing-tables-widget-tooltip pricing-tables-widget-top-left pricing-tables-widget-fixed" data-tooltip="This will cover the cost of replacing stolen or broken locks subject to a limited amount"></div><i class="icomoon-circle2 iconcircle"></i>Replacment of locks <span class="dataval"><i class="icomoon-checkmark-circle '+companies1.rep_of_locks.toString().toLowerCase()+'"></i></span> </li></ul></div></div> </div></div></div></div></div>');

}else if(rep_cnd_ins_agency == "false" && companies1.repair_cond == "Standard Wrokshop"){
	console.log("Rep Cond Standard Workshop setted");

	jQuery('#priceingtable').append('<div class="pricing-widget-wrapper mainrow"><div class="pricing-tables-widget pricing-tables-widget-default pricing-tables-widget-blue pricing-tables-widget-dark"><div class="grid-container"><div class="grid-row"><div class="grid-item item-lg-12 item-sm-6 item-xs-marginb30"><div class="grid-row grid-row-collapse"><div class="grid-item item-lg-3 item-xs-12 leftbar"><div class="pricing-tables-widget-header tablerighthead"><div id="companylogo"><p  id="companylogoinner"><span  class="'+companies1.compname.toString().toLowerCase()+'"></span></p></div><div class="pricing-tables-widget-purchase">'+companies1.compname+'</div><div id="compclass"><div class="pricing-tables-widget-purchase compclassname">'+companies1.compclass+'</div></div></div></div><div class="grid-item item-lg-6 item-xs-12"><div class="pricing-tables-widget-section"><div class="grid-row"><div class="grid-item item-lg-6 item-xs-12"><ul class="pricing-tables-widget-divider-res-section"><li><div class="pricing-tables-widget-tooltip pricing-tables-widget-top-left pricing-tables-widget-fixed" data-tooltip="This is the amount shown in your policy schedule which you must pay when you make an at fault claim and cannot be recovered from a third party"></div><i class="icomoon-circle2 iconcircle"></i>Excess/Own-Damage Cover <span class="dataval"> '+companies1.ondamage+'</span></li><li id="chkeckthis11"><div class="pricing-tables-widget-tooltip pricing-tables-widget-top-left pricing-tables-widget-fixed" data-tooltip="Personal Accident for driver is AED-120 Standard Charges"></div><i class="icomoon-circle2 iconcircle"></i><span class="switchlabel">Personal Accident Cover to Driver</span> <span class="dataval">'+pa_cover_driver_show+'</span></li><li><div class="pricing-tables-widget-tooltip pricing-tables-widget-top-left pricing-tables-widget-fixed" data-tooltip="Your Auto Repair Options are depand on Vehicle Model Year. for-example(Inside Agency, Standard Wrokshop)"></div><i class="icomoon-circle2 iconcircle"></i>Car Repair in <span class="dataval"> '+companies1.repair_cond+'</span></li><li><div class="pricing-tables-widget-tooltip pricing-tables-widget-top-left pricing-tables-widget-fixed" data-tooltip="Car rental reimbursement available for an extra 200 Dhs."></div><i class="icomoon-circle2 iconcircle"></i>Replacement Car Rental <span class="dataval"><i class="icomoon-checkmark-circle '+rent_car_check2.toString().toLowerCase()+'"></i></span></li></ul></div><div class="grid-item item-lg-6 item-xs-12"><ul><li><div class="pricing-tables-widget-tooltip pricing-tables-widget-top-left pricing-tables-widget-fixed" data-tooltip="This will cover property damages when you are legally at fault in an accident"></div><i class="icomoon-circle2 iconcircle"></i>Third Party Limit <span class="dataval">'+companies1.third_partylimit+'</span></li><li><div class="pricing-tables-widget-tooltip pricing-tables-widget-top-left pricing-tables-widget-fixed" data-tooltip="Personal Accident for Passengers Coverage Other than the Family Members is AED-30 Per Person"></div><i class="icomoon-circle2 iconcircle"></i><span class="switchlabel">Accident Cover for Passengers</span><span class="dataval"> '+pa_cover_pass_amount+'</span></li><li><div class="pricing-tables-widget-tooltip pricing-tables-widget-top-left pricing-tables-widget-fixed" data-tooltip="Out-Side UAE Coverage infrmation"></div><i class="icomoon-circle2 iconcircle"></i>OutSide UAE Coverage <span class="dataval">'+outside_uae_set+'</span></li><li><div class="pricing-tables-widget-tooltip pricing-tables-widget-top-left pricing-tables-widget-fixed" data-tooltip="This will provide you off road assistance on condition that the car is on certain metres away from the paved road"></div><i class="icomoon-circle2 iconcircle"></i>Off-Road & Desert Recovery <span class="dataval"><i class="icomoon-checkmark-circle '+companies1.offroad_desrec.toString().toLowerCase()+'"></i></span></li></ul></div></div><p class="pricing-tables-widget-purchase seemoredetails"><a href="#demo'+g+'" class="pricing-tables-widget-purchase seemoretoggle">see more details</a><i class="icomoon-menu-open2 seemoredetailsicon"></i></p> </div></div><div class="grid-item item-lg-3 item-xs-12"><div class="pricing-tables-widget-footer ptablefooter"><p class="pricing-tables-widget-top-left rightdiscount">Save '+companies1.discounts+'</p><h3 class="item-xs-marginb0"><strike>'+companies1.price_beforedisc+'</strike></h3><p>Total Price Are</p><h1 class="item-xs-marginb0"><span class="dataval1">'+companies1.price_afterdisc+'</span> AED</h1><div class="pricing-tables-widget-price">'+companies1.price_afterdisc+' AED</div>'+companies1.cart_btn+'</div></div></div></div></div><div id="demo'+g+'" class="" style="display:none;"><div class="pricing-tables-widget-section moredetail"><div class="grid-row"><div class="grid-item item-sm-4"><ul class="pricing-tables-widget-divider-res-section"><li><div class="pricing-tables-widget-tooltip pricing-tables-widget-top-left pricing-tables-widget-fixed" data-tooltip="Road Side Assistance either free or either there will be charge according to the company we will add the package since most of the companies are charging the road side Assistance"></div><i class="icomoon-circle2 iconcircle"></i>Road Side Assistance <span class="dataval"><i class="icomoon-checkmark-circle '+companies1.rs_assistance.toString().toLowerCase()+'"></i></span></li><li><div class="pricing-tables-widget-tooltip pricing-tables-widget-top-left pricing-tables-widget-fixed" data-tooltip="WWindscreen Damage information"></div><i class="icomoon-circle2 iconcircle"></i>Windscreen Damage <span class="dataval">'+companies1.windscreen_dmage+'</span></li><li><div class="pricing-tables-widget-tooltip pricing-tables-widget-top-left pricing-tables-widget-fixed" data-tooltip="This will cover your vehicle for removing dings, creases and dents caused by accidental damage or any perils"></div><i class="icomoon-circle2 iconcircle"></i>Dent Repair <span class="dataval"><i class="icomoon-checkmark-circle '+companies1.repair_dent.toString().toLowerCase()+'"></i></span></li><li><div class="pricing-tables-widget-tooltip pricing-tables-widget-top-left pricing-tables-widget-fixed" data-tooltip="tooltip info"></div><i class="icomoon-circle2 iconcircle"></i>Loss of Personal Belongings <span class="dataval"><i class="icomoon-checkmark-circle '+companies1.loss_of_pb.toString().toLowerCase()+'"></i></span></li></ul></div><div class="grid-item item-sm-4"><ul><li><div class="pricing-tables-widget-tooltip pricing-tables-widget-top-left pricing-tables-widget-fixed" data-tooltip="This will cover you for the cost of emergency medical treatment following an accident per person cost one Accident One Claim per year"></div><i class="icomoon-circle2 iconcircle"></i>Emergency Medical Expenses <span class="dataval">'+companies1.em_med_expence+'</span></li><li><div class="pricing-tables-widget-tooltip pricing-tables-widget-top-left pricing-tables-widget-fixed" data-tooltip="This will cover you for the cost of any service or transport by a recognize ambulance following an accident"></div><i class="icomoon-circle2 iconcircle"></i>Ambulance Cover <span class="dataval"><i class="icomoon-checkmark-circle '+companies1.amb_cover.toString().toLowerCase()+'"></i></span></li><li><div class="pricing-tables-widget-tooltip pricing-tables-widget-top-left pricing-tables-widget-fixed" data-tooltip="Personal Accident benefit cover for driver and passengers up to Dhs. 200,000 each in case of death or total permanent disability of person. (Coverage details available upon request)"></div><i class="icomoon-circle2 iconcircle"></i>Death or Bodily Injury <span class="dataval"><i class="icomoon-checkmark-circle '+companies1.death_bodyinjury.toString().toLowerCase()+'"></i></span></li><li><div class="pricing-tables-widget-tooltip pricing-tables-widget-top-left pricing-tables-widget-fixed" data-tooltip="Car Renewal infrmation"></div><i class="icomoon-circle2 iconcircle"></i>Car Registration / Renewal <span class="dataval"><i class="icomoon-checkmark-circle '+companies1.car_reg_renewal.toString().toLowerCase()+'"></i></span></li></ul></div><div class="grid-item item-sm-4"><ul><li><div class="pricing-tables-widget-tooltip pricing-tables-widget-top-left pricing-tables-widget-fixed" data-tooltip="This will cover your car to any damages caused by a natural peril (i.e. storm, flood, hailstorm etc.)"></div><i class="icomoon-circle2 iconcircle"></i>Natural Calamity Cover <span class="dataval"><i class="icomoon-checkmark-circle '+companies1.natural_clamity_cover.toString().toLowerCase()+'"></i></span></li><li><div class="pricing-tables-widget-tooltip pricing-tables-widget-top-left pricing-tables-widget-fixed" data-tooltip="This will cover your vehicle for accidental fire damage and for damages resulting from attempted theft (i.e. broken windows or stolen radio). This also covers replacement of your car if stolen."></div><i class="icomoon-circle2 iconcircle"></i>Fire and Theft Cover <span class="dataval"><i class="icomoon-checkmark-circle '+companies1.fire_and_theft.toString().toLowerCase()+'"></i></span></li><li><div class="pricing-tables-widget-tooltip pricing-tables-widget-top-left pricing-tables-widget-fixed" data-tooltip="This will cover your vehicle for theft while your car is under the care of a valet service (i.e hotels, restaurants and other businesses)"></div><i class="icomoon-circle2 iconcircle"></i>Valet Parking Theft <span class="dataval"><i class="icomoon-checkmark-circle '+companies1.v_parking_theft.toString().toLowerCase()+'"></i></span> </li><li><div class="pricing-tables-widget-tooltip pricing-tables-widget-top-left pricing-tables-widget-fixed" data-tooltip="This will cover the cost of replacing stolen or broken locks subject to a limited amount"></div><i class="icomoon-circle2 iconcircle"></i>Replacment of locks <span class="dataval"><i class="icomoon-checkmark-circle '+companies1.rep_of_locks.toString().toLowerCase()+'"></i></span> </li></ul></div></div> </div></div></div></div></div>');
}

});
}
});
});
</script>


<script>
//jQuery(document).ready(function() {
jQuery(document).on('click', '.seemoretoggle', function () {
var content_toggle= jQuery(this).attr('href');
//jQuery(window).scrollTop(0);
jQuery(content_toggle).slideToggle("slow");
});


jQuery(document).on('click', '.add-tocartbtn', function () {
//Set form name TPL
//localStorage.setItem("form_type",'ins');
//your function here
var alldataclicked = jQuery(this).attr('data1');
//////console.log("alldatacheck.."+alldataclicked);

var dataSelected_full = alldataclicked.split('#');
var price_final = dataSelected_full[0];
localStorage.setItem("dataSelected_full",JSON.stringify(dataSelected_full));

//console.log("dataSelected_full[2].."+dataSelected_full[2]);
//Getting Selected Company and let it go

localStorage.setItem("final_price_cart",dataSelected_full[0]);

jQuery('#comp_class').html(dataSelected_full[2]);

var selected_compny_data = dataSelected_full;
var price = 	dataSelected_full[0];
var compname =  dataSelected_full[1];
var gcc_spec =  dataSelected_full[2];
var vehicle_cat = dataSelected_full[3];
var discount_applied = dataSelected_full[3];
var repair_cond = dataSelected_full[12];
var minimum_val = dataSelected_full[5];
var maximum_val = dataSelected_full[6];
var rate = dataSelected_full[7];
var minimum = dataSelected_full[8];
var dl_less_than_year = dataSelected_full[9];
var less_than_year = dataSelected_full[10];
var os_coverage = dataSelected_full[18];
var pa_cover_driver = dataSelected_full[13];
var pa_cover_passanger = dataSelected_full[14];
var rs_assistance = dataSelected_full[14];
var rent_car = dataSelected_full[16];
var excess_amount = dataSelected_full[17];
var third_prty_limit = dataSelected_full[22];
var em_med_expence2 = dataSelected_full[26];
var em_loss_of_per_belongings = dataSelected_full[26];

//console.log("excess_amount..."+dataSelected_full[16]);
//console.log("excess_amount..."+dataSelected_full[17]);
//console.log("excess_amount..."+dataSelected_full[18]);

//User Details
var veh_email_address = jQuery('#email_address').val();
var veh_vehicle_brand = jQuery('#vehicle_brand option:selected').val();
var veh_model_year = jQuery('#model_year').val();
var veh_fullname = jQuery('#firstname').val();
var veh_dobirth = jQuery('#datepicker1').val();
var veh_nationality = jQuery('#nationality option:selected').val();
var veh_country_first_driving_lic = jQuery('#country_firstdl').val();
var veh_brandnew = jQuery('#fr3').val();
var veh_manyear1 = jQuery('#dor_date').val();
var veh_manyear2 = jQuery('#dor_month').val();
var veh_manyear3 = jQuery('#dor_year').val();
var veh_manufactured_year = veh_manyear1+'/'+veh_manyear2+'/'+veh_manyear3;
var veh_contact_num1 = jQuery('#contact_num1').val();
var veh_contact_num2 = jQuery('#contact_num2').val();
var veh_contact_numF = veh_contact_num1+veh_contact_num2;
var veh_model_detail = jQuery('#vehicle_model_detail').val();
var veh_reg_date1 = jQuery('#vehicle_reg_date1').val();
var veh_reg_date2 = jQuery('#vehicle_reg_date2').val();
var veh_reg_date3 = jQuery('#vehicle_reg_date3').val();
var veh_reg_dateF = veh_reg_date1+'/'+veh_reg_date2+'/'+veh_reg_date3;
var veh_city_ofreg = jQuery('#cityof_reg').val();
var veh_car_val = jQuery('#vehicle_val').val();
//var veh_policystart_date1 = jQuery('#vec_policy_date1').val();
//var veh_policystart_date2 = jQuery('#vec_policy_date2').val();
//var veh_policystart_date3 = jQuery('#vec_policy_date3').val();
//var veh_policy_sdateF = veh_policystart_date1+'/'+veh_policystart_date2+'/'+veh_policystart_date3;
var veh_country_first_dl = jQuery('#country_firstdl').val();
// var years_exp_dl_home = jQuery('#home_country_drivingexp option:selected').val();
// var years_exp_dl_home_set;

// if(years_exp_dl_home == "66" ){
// years_exp_dl_home_set = "6 Months";
// }
// if(years_exp_dl_home == "1" ){
// years_exp_dl_home_set = "1 Year";
// }
// else if(years_exp_dl_home == "2" ){
// years_exp_dl_home_set = "2 Years";
// }
// else if(years_exp_dl_home == "3" ){
// years_exp_dl_home_set = "3 Years";
// }
// else if(years_exp_dl_home == "44" ){
// years_exp_dl_home_set = "More then 4 Years";
// }
//Set data to stap 5

jQuery('#comp_name').html(compname);
jQuery('#ins_value').html(veh_car_val);
jQuery('#sel_vehicleModel').html(veh_model_year);
jQuery('#sel_vehicleBrand').html(veh_vehicle_brand);
jQuery('#sel_vehicleModel_detail').html(veh_model_detail);
jQuery('#driv_name').html(veh_fullname);
jQuery('#driv_nationality').html(veh_nationality);
//jQuery('#driv_drivingExp').html(veh_nationality);
//jQuery('#driv_homecountry_exp').html(years_exp_dl_home_set);
jQuery('#driv_DOB').html(veh_dobirth);
jQuery('#driv_email').html(veh_email_address);
jQuery('#car_tpl').html(third_prty_limit);
jQuery('#car_ExcessAmount').html(excess_amount);
jQuery('#car_EmergencyMedical').html(em_med_expence2);
//jQuery('#rent_aCar_chkout').html(rent_car);
jQuery('#loss_per_belong_chkout').html(em_loss_of_per_belongings);
//jQuery('#pa_cover_passangers').html(pa_cover_passanger);
//jQuery('#outside_uae_cover').html(os_coverage);
jQuery('#ins_price').html(price);
jQuery('#ins_price1').html(price);
compname2 = compname.toLowerCase();
localStorage.setItem("compname2",compname2);
//jQuery('#company_logoname').addClass(compname2);
//jQuery('#policy_startFrom').html(veh_policy_sdateF);
//jQuery('#policy_end1').html(veh_policystart_date1);
//jQuery('#policy_end2').html(veh_policystart_date2);
//var next_year = parseInt(veh_policystart_date3) + 1;
//jQuery('#policy_end3').html(next_year);
jQuery('#ins_discount').html(discount_applied);

//Apply Vat on Price
var disc_price = parseFloat(price) * 0.05;
var vat_added_amount = parseFloat(price) + disc_price;
////console.log("vat_added_amount"+vat_added_amount);
jQuery('#ins_total_after_vat').html(vat_added_amount.toFixed(2));

what_included();
var years_uaedriving_lic = jQuery('#years_uaedrivinglicence option:selected').val();


var total_Drivingexperience;
//var exp_total_added = parseInt(years_uaedriving_lic) + parseInt(years_exp_dl_home);
var exp_total_added = parseInt(years_uaedriving_lic);

if(exp_total_added == "66" ){
total_Drivingexperience = "6 Months";
}
if(exp_total_added == "1" ){
total_Drivingexperience = "1 Year";
}
else if(exp_total_added == "2" ){
total_Drivingexperience = "2 Years";
}
else if(exp_total_added == "3" ){
total_Drivingexperience = "3 Years";
}
else if(exp_total_added == "44" ){
total_Drivingexperience = "More then 4 Years";
}
jQuery('#driv_drivingExp').html(total_Drivingexperience);
/* else if(exp_total_added == "6" ){
total_Drivingexperience = "6 Years";
}
else if(exp_total_added == "88" ){
total_Drivingexperience = "More then 4 Years";
}
else if(exp_total_added == "69" ){
total_Drivingexperience = "3 Years and 6 Months";
}
else if(exp_total_added == "67" ){
total_Drivingexperience = "1 Year and 6 Months";
}
else if(exp_total_added == "68" ){
total_Drivingexperience = "2 Years and 1 Month";
} */

var repair_cond_set = dataSelected_full[13];
jQuery("#car_repairin").html(repair_cond_set);

//Email Template
jQuery('#driv_carRepairIn_e').html(repair_cond_set);

var rent_a_carCheck = jQuery('input[name=veh_replacement_chk]:checked').val();

if(rent_a_carCheck == "Yes"){
rent_car = rent_car;
}
else if(rent_a_carCheck == "No"){
rent_car = "None";
}
jQuery("#rent_aCar_chkout").html(rent_car);

//console.log("os_coverage.."+os_coverage);
//var outside_uae_cov_comp1 = localStorage.getItem("outside_uae_cov_value");
var outside_uae_cov_comp1 = os_coverage;
////console.log("outside_uae_cov_comp1.."+outside_uae_cov_comp1);
var outside_uae_set;

if(outside_uae_cov_comp1 == "TRUE"){
	var outside_uae_set = "FREE";
}
else {
	var outside_uae_set = "None";
}

jQuery('#outside_uae_cover').html(outside_uae_set);

//Personnal Accident Cover for Driver
var need_per_acc_cover_driver1 = localStorage.getItem("pa_cover_driver_check");
//console.log("need_per_acc_cover_driver1.."+need_per_acc_cover_driver1);
var pa_coverdriverset;

if(need_per_acc_cover_driver1 == "Yes" || need_per_acc_cover_driver1 == "YES"){
pa_coverdriverset = "Yes";
}
else if(need_per_acc_cover_driver1 == "No"){
pa_coverdriverset = "None";
}
jQuery('#pa_cover_driver_chkout').html(pa_coverdriverset);

//Email Template
jQuery('#driv_paCoverDriver_e').html(pa_coverdriverset);



//Set data to Email Template
var mycurrentpolicy_chk_email = localStorage.getItem("mycurrentpolicy_chk_e");
var current_ins_notexp_val_email = localStorage.getItem("current_ins_notexp_val_e");
var veh_not_forcommercial_email = localStorage.getItem("veh_not_forcommercial_e");
var ncd_cert_e = localStorage.getItem("ncd_check_years");

jQuery('#ncd_cert_e').html(ncd_cert_e);
jQuery('#my_veh_not_commercial_e').html(veh_not_forcommercial_email);
jQuery('#my_curr_ins_notexp_e').html(current_ins_notexp_val_email);
jQuery('#my_curr_pol_comprehensive_e').html(mycurrentpolicy_chk_email);
jQuery('#comp_name_e').html(compname);
jQuery('#driv_name_e').html(veh_fullname);
jQuery('#sel_vehicleModel_e').html(veh_model_detail);
jQuery('#contact_num_email').html(veh_contact_numF);
jQuery('#veh_value_e').html(veh_car_val);
jQuery('#driv_drivingExp_e').html(total_Drivingexperience);
//jQuery('#my_veh_is_gccspec_e').html(gcc_spec);
jQuery('#driv_rentcar_e').html(rent_car);
//jQuery('#driv_acc_coverpassanger_e').html(pa_cover_passanger);
jQuery('#driv_OutsideUaeCover_e').html(os_coverage);
//jQuery('#driv_carRepairIn_e').html(repair_cond);
jQuery('#driv_thirdPLimit_e').html(third_prty_limit);
//jQuery('#driv_paCoverDriver_e').html(pa_cover_driver);
jQuery('#driv_ownDamageCover_e').html(excess_amount);
jQuery('#driv_EmMedicalExp_e').html(em_med_expence2);



var data = {

'action': 			'my_ajax_rt',
'ins_price' : 	price,
'ins_compname' :  compname,
'ins_discount' :  gcc_spec,
'ins_vehicle_cat' : vehicle_cat,
'ins_repair_cond' : repair_cond,
'ins_minimum_val' : minimum_val,
'ins_maximum_val' : maximum_val,
'ins_rate' : rate,
'ins_minimum' : minimum,
'ins_dl_less_than_year' : dl_less_than_year,
'ins_less_than_year' : less_than_year,
'ins_os_coverage' : os_coverage,
'ins_pa_cover_driver' : pa_cover_driver,
'ins_cover_passanger' : pa_cover_passanger,
'ins_rs_assistance' : rs_assistance,
'ins_rent_car' : rent_car,
'ins_excess_amount' : excess_amount,
'ins_third_prty_limit' : third_prty_limit,
'ins_emergency_med_exp' : em_med_expence2,
'ins_loss_of_per_belongings' : em_loss_of_per_belongings,

'ins_total_drivexp' : total_Drivingexperience,
'ins_fullname' : veh_fullname,
'ins_vehicle_brand' : veh_vehicle_brand,
'ins_veh_model_year' : veh_model_year,
'ins_contact_num' : veh_contact_numF,
'ins_veh_man_year' : veh_manufactured_year,
'ins_emailaddress' : veh_email_address,
'ins_nationality_sel' : veh_nationality,
'ins_brand_new' : veh_brandnew,
'ins_model_detail' : veh_model_detail,
'ins_cityof_reg' : veh_city_ofreg,
'ins_car_value' : veh_car_val,
'ins_country_first_dl' : veh_country_first_dl,
'ins_cntry_fir_driv_lic' : veh_country_first_driving_lic,
'ins_dob_date' : veh_dobirth,
'ins_selected_compny_data' : selected_compny_data

};

var ajax_url_custom = "http://carinsuranceuae.net/wp-admin/admin-ajax.php";

jQuery.post(ajax_url_custom, data,   function(response) {
////console.log(response);
});

showEstimatedFareHtml(price_final);
document.getElementById("stern_taxi_fare_estimated_fare").value =  price_final;
localStorage.setItem("finalPrice", price_final);

//Show pre checkot
jQuery( "#step5" ).show();
jQuery( "#bread_crums_custom" ).show();
jQuery( "#step4" ).hide();
jQuery( "#form_main_div" ).hide();
jQuery( ".form_desc" ).hide();
});
//Capture Emai Template
function capture_precheckout(){
var html_email = jQuery( "#email_container" ).html();

var data = {
'action': 			'my_ajax_rt_precheckout',
'ins_pdf_image' : 	html_email };

var ajax_url_custom = "https://insurancetopup.com/wp-admin/admin-ajax.php";

jQuery.post(ajax_url_custom, data, function(response) {
//console.log(response);
});

}


jQuery(document).on('click', '#proceed_checkout', function () {
capture_precheckout();
checkout_url_function();
});

</script>

<!-- Old Calculater -->

<form  id="stern_taxi_fare_div" method="post">
<div  style="@display:none;">
<input type="hidden"  name="stern_taxi_fare_estimated_fare" id="stern_taxi_fare_estimated_fare" value=""/><div class="row">
</div>
</div>
</form>
</div>

<div class="col-lg-3 form_desc">
<div class="desc_icon ">
<img class="form_descicon" src="http://carinsuranceuae.net/xio_outww2/insurance-quote-icon.png">
</div>
<p class="form_descright">Can't find what you need? Not sure about something? Please Contact us at <a href="mailto:hello@insurancetopup.com">hello@insurancetopup.com</a>, or Call Us at<a href="#">+97165661505</a> We'll be able to pull up your quote and help you get the coverage that's right for you</p>

<div class="progress_indicator" style="display:none;">
<div class="big c100 " id="progress">
<span id="percentage-text">0%</span>
<span id="complete">Info Complete</span>
<div class="slice">
<div class="bar"></div>
<div class="fill"></div>
</div>
<div class="whitecircle"></div>
</div>
</div>

</div>
</div>

<!-- <script type="text/javascript" src="http://doptiq.com/smart-forms/demos/samples/elegant/js/jquery.formShowHide.min.js"></script> -->

<script type="text/javascript">
jQuery(document).ready(function(jQuery){
jQuery(function(){
//jQuery('.smartfm-ctrl').formShowHide();
});

/* @normal masking rules */

jQuery.mask.definitions['f'] = "[A-Fa-f0-9]";

jQuery("#contact_num2").mask('9999999', {placeholder:'X'});
//jQuery("#datepicker1").mask('99-99-9999', {placeholder:'_'});
jQuery('#firstname').bind('keyup blur',function(){
var node = jQuery(this);
node.val(node.val().replace(/^[0-9]*$/,'') );
}
);


//Showpopups on Select.

jQuery('#rent_a_car_id input').on('change', function() {
var rent_a_car_val = jQuery('input[name=veh_replacement_chk]:checked').val();
if(rent_a_car_val == 'Yes' ){
jQuery( "#popup_infomain" ).show();
//alert(" You Have choosed rental car option. So... 200 AED will be added ");
//////////console.log("Rent Car Yes");
}
});

//Show Brand New Popup
jQuery('#veh_brand_new_id input').on('change', function() {
var brand_newchk = jQuery('input[name=new_check]:checked').val();
if(brand_newchk == 'Yes' ){
//////console.log("Brand New Yes");
jQuery( "#popup_infomain_brand_new" ).show();
}
});

//Current Insurence Not Expired alert
jQuery('#my_current_ins_not_exp input').on('change', function() {
var current_ins_notexp_val = jQuery('input[name=mycurrentins_chk]:checked').val();
localStorage.setItem("current_ins_notexp_val_e",current_ins_notexp_val);
if(current_ins_notexp_val == 'No' ){
jQuery( "#popup_current_ins_expired" ).show();
}
});



	

	//Current Insurence GCC Spec
	jQuery('#my_veh_is_gccspec_e').html("None");
	jQuery('#my_veh_is_gcc input').on('change', function() {
		var cur_veh_gccspec = jQuery('input[name=vehicle_isgcc_chk]:checked').val();
		
		if(cur_veh_gccspec == 'Yes' ){
			jQuery('#my_veh_is_gccspec_e').html("Yes");
		}else{
			jQuery('#my_veh_is_gccspec_e').html("None");
		}

		//Check to redirect if Gcc Specification is No
		var cur_veh_gccspec_redirect = jQuery('input[name=vehicle_isgcc_chk]:checked').val();
		
		//console.log("cur_veh_gccspec_redirect.."+cur_veh_gccspec_redirect);
		
		if(cur_veh_gccspec_redirect == 'No' ){
			jQuery( "#qoute_custom_gcc" ).show();
			jQuery( "#qoute_custom" ).hide();
			var website_url = "http://"+window.location.host+"/contact-insurance-agent";
			jQuery("#qoute_custom_gcc").attr("href", website_url);
		}
	});

localStorage.setItem("veh_not_forcommercial_e","No");

	//My Vehicle is not for Commercial Purpose
	jQuery('#veh_not_forcommercial input').on('change', function() {
		var veh_not_forcommercial = jQuery('input[name=vehiclecommercial_chk]:checked').val();
		localStorage.setItem("veh_not_forcommercial_e",veh_not_forcommercial);
		if(veh_not_forcommercial == 'No' ){
			jQuery( "#vehicle_notfor_commercialpurpose" ).show();

			//Check to redirect if Gcc Specification is No
			jQuery( "#qoute_custom_gcc" ).show();
			jQuery( "#qoute_custom" ).hide();
			var website_url = "http://"+window.location.host+"/contact-insurance-agent";
			//console.log(website_url);
			jQuery("#qoute_custom_gcc").attr("href", website_url);
		}
	});

//Outside UAE Coverage
jQuery('#outside_uae_coverage_id input').on('change', function() {
	var outside_uae_coverage = jQuery('input[name=outsideuaeCover_chk]:checked').val();
	// if(outside_uae_coverage == 'TRUE' ){
	// jQuery( "#popup_outsideuae_cover" ).show();
	// }
});


//Model
/*
jQuery('[data-smartmodal-close]').on('click', function(e)  {
e.preventDefault();
var smartInactiveModal = jQuery(this).attr('data-smartmodal-close');
jQuery(smartInactiveModal).removeClass('smartforms-modal-visible');
jQuery('body').removeClass('smartforms-modal-scroll');
});
});
*/

// get the mPopup
/* 	var mpopup = document.getElementById('mpopupBox');
var close = document.getElementById('close-popup');
*/

// close the mPopup when user clicks outside of the box
/* 	window.onclick = function(event) {

if (event.target == mpopup) {
mpopup.style.display = "none";
}
} */

//Personnal Account Cover Driver Price Table Check
//jQuery('#pa_cover_driver_chekcbox0').on('change', 'input', function(){

//alert('hello');
//});



jQuery('#pa_cover_driver_chekcbox0 input').on('change', function() {
var pa_cover_driver_chekcbox_val = jQuery('input[name=pac_driver0]:checked').val();
//////////////console.log("pa_cover_driver_chekcbox_val.."+pa_cover_driver_chekcbox_val);
});
});
</script>


<style>
/* mPopup box style */
.mpopup {
display: none;
position: fixed;
z-index: 1;
padding-top: 100px;
left: 0;
top: 0;
width: 100%;
height: 100%;
overflow: auto;
background-color: rgb(0,0,0);
background-color: rgba(0,0,0,0.4);
}

.mpopup-content {
position: relative;
background-color: #fefefe;
margin: auto;
padding: 0;
width: 60%;
box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19);
-webkit-animation-name: animatetop;
-webkit-animation-duration: 0.4s;
animation-name: animatetop;
animation-duration: 0.4s
}

.mpopup-head {
padding: 2px 16px;
background-color: #ff0000;
color: white;
}
.mpopup-main {padding: 2px 16px;}

.mpopup-foot {
padding: 2px 16px;
background-color: #ff0000;
color: #ffffff;
}

/* add animation effects */
@-webkit-keyframes animatetop {
from {top:-300px; opacity:0}
to {top:0; opacity:1}
}

@keyframes animatetop {
from {top:-300px; opacity:0}
to {top:0; opacity:1}
}

/* close button style */
.close {
color: white;
float: right;
font-size: 28px;
font-weight: bold;
}

.close:hover, .close:focus {
color: #000;
text-decoration: none;
cursor: pointer;
}
</style>

<!-- Model Start -->

<div id="popup_infomain_brand_new" style="display:none;">
<input class="bunta-switcher" id="bunta-success" type="checkbox">
<div class="bunta" data-bunta-style="dos" data-bunta-theme="success">
<label class="bunta-close" for="bunta-success">&times;</label>
<label class="bunta-overlay" for="bunta-success" data-bunta-opacity="40"></label>
<div class="bunta-container">
<div class="bunta-center">
<h3 class="bunta-heading-icon"><i class="fa fa-info-circle"></i></h3>
<h2 class="bunta-heading text1">Wow! Brand New Car<i class="fa fa-smile-o"></i></h2>
<h4 class="bunta-heading text1"><p></p>
Your Car is Brand New..So No Need to First Registration Date.</h4>
<p></p>
<label class="bunta-button bunta-circle" id="info_ok_brand_new" for="bunta-success" data-bunta-theme="white"> - OK - </label>
</div>
</div>
</div>
</div>


<div id="popup_dangerPolicyStartDate" style="display:none;">
<input class="bunta-switcher" id="bunta-danger" type="checkbox">
<div id="mpopupBox" class="bunta" data-bunta-style="dos" data-bunta-theme="danger">
<label class="bunta-close" for="bunta-danger">&times;</label>
<label class="bunta-overlay" for="bunta-danger" data-bunta-opacity="40"></label>
<div class="bunta-container" id="banta_cont">
<div class="bunta-center">
<h3 class="bunta-heading-icon"><i class="fa fa-calendar"></i></h3>
<h1 class="bunta-heading text1">Oops!</h1>
<h2 class="bunta-heading text1">You have Selected Policy Start Date more than 60 Days.</h2>
<p class="text1 text2">Please! choose date range within under 60 days...</p>
<label class="bunta-button bunta-circle" id="dangerPolicy_ok" for="bunta-danger" data-bunta-theme="white"> - OK - </label>
</div>
</div>
</div>
</div>


<div id="popup_current_ins_expired" style="display:none;">
<input class="bunta-switcher" id="bunta-danger" type="checkbox">
<div id="mpopupBox" class="bunta" data-bunta-style="dos" data-bunta-theme="danger">
<label class="bunta-close" for="bunta-danger">&times;</label>
<label class="bunta-overlay" for="bunta-danger" data-bunta-opacity="40"></label>
<div class="bunta-container" id="banta_cont">
<div class="bunta-center">
<h3 class="bunta-heading-icon"><i class="fa fa-info-circle"></i></h3>
<h1 class="bunta-heading text1">Oops!</h1>
<h2 class="bunta-heading text1">Your Current Insurance is Already Expired</h2>
<p class="text1 text2">Contact Our Sales Team, Our Insurance Agent is Ready to Talk to You! Want to chat with someone right now? Try our live chat System or Call Us on this Number</p>
<h3 class="bunta-heading text1"><i class="fa fa-phone"></i> Call Now @ +971-65661505</h3>
<label class="bunta-button bunta-circle" id="ins_expired_danger_ok" for="bunta-danger" data-bunta-theme="white"> - OK - </label>
</div>
</div>
</div>
</div>


<div id="popup_dangermain1111" style="display:none;">
<input class="bunta-switcher" id="bunta-danger" type="checkbox">
<div id="mpopupBox" class="bunta" data-bunta-style="dos" data-bunta-theme="danger">
<label class="bunta-close" for="bunta-danger">&times;</label>
<label class="bunta-overlay" for="bunta-danger" data-bunta-opacity="40"></label>
<div class="bunta-container" id="banta_cont">
<div class="bunta-center">
<h3 class="bunta-heading-icon"><i class="fa fa-id-card"></i></h3>
<h1 class="bunta-heading text1">Oops!</h1>
<h2 class="bunta-heading text1">Your Current Policy is TPL Insurance</h2>
<p class="text1 text2">Dear Valued Customer, Your Current Policy is Third Party Liability Insurance, so please call us and talk to one of our Agents. We'll be able to pull up your quote and help you get the coverage that's right for you.</p>
<h3 class="bunta-heading text1"><i class="fa fa-phone"></i> Call Now @ +971 65661505</h3>
<label class="bunta-button bunta-circle" id="danger_ok" for="bunta-danger" data-bunta-theme="white"> - OK - </label>
</div>
</div>
</div>
</div>


<div id="vehicle_notfor_commercialpurpose" style="display:none;">
<input class="bunta-switcher" id="bunta-danger" type="checkbox">
<div id="mpopupBox" class="bunta" data-bunta-style="dos" data-bunta-theme="danger">
<label class="bunta-close" for="bunta-danger">&times;</label>
<label class="bunta-overlay" for="bunta-danger" data-bunta-opacity="40"></label>
<div class="bunta-container" id="banta_cont">
<div class="bunta-center">
<h3 class="bunta-heading-icon"><i class="fa fa-car"></i></h3>
<h1 class="bunta-heading text1">Oops!</h1>
<h2 class="bunta-heading text1">Your Vehicle is Use as Commercial Purpose</h2>
<p class="text1 text2">Contact Our Sales Team, Our Insurance Agent is Ready to Talk to You! Via Chat or on Phone - Also Check Our Third Party Insurance</p>
<h3 class="bunta-heading text1"><i class="fa fa-phone"></i> Call Now @ +971-65661505</h3>
<label class="bunta-button bunta-circle" id="veh_notcommercial_ok" for="bunta-danger" data-bunta-theme="white"> - OK - </label>
</div>
</div>
</div>
</div>


<div id="popup_infomain" style="display:none;">
<input class="bunta-switcher" id="bunta-success" type="checkbox">
<div class="bunta" data-bunta-style="dos" data-bunta-theme="success">
<label class="bunta-close" for="bunta-success">&times;</label>
<label class="bunta-overlay" for="bunta-success" data-bunta-opacity="40"></label>
<div class="bunta-container">
<div class="bunta-center">
<h3 class="bunta-heading-icon"><i class="fa fa-info-circle"></i></h3>
<h2 class="bunta-heading text1">That's Good job!<i class="fa fa-thumbs-up"></i></h2>
<h4 class="bunta-heading text1"><p></p>
You have chosen Rental Car Option. So Amount of 200 AED will be added in Quote</h4>
<p></p>
<label class="bunta-button bunta-circle" id="info_ok" for="bunta-success" data-bunta-theme="white"> - OK - </label>
</div>
</div>
</div>
</div>


<div id="popup_outsideuae_cover" style="display:none;">
<input class="bunta-switcher" id="bunta-success" type="checkbox">
<div class="bunta" data-bunta-style="dos" data-bunta-theme="success">
<label class="bunta-close" for="bunta-success">&times;</label>
<label class="bunta-overlay" for="bunta-success" data-bunta-opacity="40"></label>
<div class="bunta-container">
<div class="bunta-center">
<h3 class="bunta-heading-icon"><i class="fa fa-info-circle"></i></h3>
<h2 class="bunta-heading text1">That's Good job!<i class="fa fa-thumbs-up"></i></h2>
<h4 class="bunta-heading text1"><p></p>
You have chosen Outside UAE Coverage Option. So Amount  will be added in Quote</h4>
<p></p>
<label class="bunta-button bunta-circle" id="info_outsideuae_ok" for="bunta-success" data-bunta-theme="white"> - OK - </label>
</div>
</div>
</div>
</div>


<div id="popup_dangerdob" style="display:none;">
<input class="bunta-switcher" id="bunta-danger" type="checkbox">
<div id="mpopupBox" class="bunta" data-bunta-style="dos" data-bunta-theme="danger">
<label class="bunta-close" for="bunta-danger">&times;</label>
<label class="bunta-overlay" for="bunta-danger" data-bunta-opacity="40"></label>
<div class="bunta-container" id="banta_cont">
<div class="bunta-center">
<h3 class="bunta-heading-icon"><i class="fa fa-id-card"></i></h3>
<h1 class="bunta-heading text1">Oops!</h1>
<h2 class="bunta-heading text1">Your Age is Less then 25 Years</h2>
<p class="text1 text2">Dear Valued Customer, Your age is less then 21 years, so please call us and talk to one of our Agents.
We'll be able to pull up your quote and help you get the coverage that's right for you.<br>
It doesn’t matter if you are 18 or 80, there are always ways for you to save on auto insurance.</p>
<h3 class="bunta-heading text1"><i class="fa fa-phone"></i> Call Now @ +971-65661505</h3>
<label class="bunta-button bunta-circle" id="dangerDOB_ok" for="bunta-danger" data-bunta-theme="white"> - OK - </label>
</div>
</div>
</div>
</div>
<!-- Model End -->


<!-- Pricing Table -->
<!-- FONT LINK -->
<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,600,600italic,700,700italic,800,800italic|Source+Sans+Pro:400,300,300italic,400italic,600,600italic,700,700italic,900,900italic|Alegreya+Sans:400,300,300italic,400italic,500,500italic,700,700italic,800,800italic,900,900italic|Poiret+One|Dosis:400,500,300,600,700,800|Lobster+Two:400,400italic,700,700italic|Raleway:400,300,300italic,400italic,500,500italic,600,600italic,700,700italic,800,800italic,900,900italic|Roboto+Condensed:400,300italic,300,400italic,700,700italic|Roboto:400,300,300italic,400italic,500,500italic,700,700italic,900,900italic|Courgette">


<div id="step4">
<div id="user_inforow">
<!-- <h2 class="user_info"><?php //global $current_user; get_currentuserinfo(); echo 'Dear, ' . $current_user->user_login . ""; ?></h2> -->
<h2 class="user_info">Dear: <span id="user_filled"></span></h2>
<h2 class="user_details">We have Found <span id="cars_count"></span> car isurance qoutes for your <span id="cars_brand"></span> <span id="cars_model"></span> Worth is <span id="cars_worth"></span> AED</h2>
<p class="user_tooltip"><i class="icomoon-info2"></i> Tool tip appear on all title (for detailed info) and for more info about policy expand the table please.</p>
</div>

<div id="priceingtable"> </div>
</div>

<div id="step5">
<?
//Get data from Ajax
$ins_price_sel = $_REQUEST['ins_price'];
$ins_compname_sel = $_REQUEST['ins_compname'];
$ins_discount_sel = $_REQUEST['ins_gcc_spec'];
$ins_vehicle_cat = $_REQUEST['ins_vehicle_cat'];
$ins_repair_cond_sel = $_REQUEST['ins_repair_cond'];
$ins_minimum_val = $_REQUEST['ins_minimum_val'];
$ins_maximum_val = $_REQUEST['ins_maximum_val'];
$ins_rate = $_REQUEST['ins_rate'];
$ins_minimum = $_REQUEST['ins_minimum'];
$ins_dl_less_than_year_sel = $_REQUEST['ins_dl_less_than_year'];
$ins_less_than_year_sel = $_REQUEST['ins_less_than_year'];
$ins_os_coverage_sel = $_REQUEST['ins_os_coverage'];
$ins_pa_cover_driver1 = $_REQUEST['ins_pa_cover_driver'];
$ins_papp = $_REQUEST['ins_papp'];
$ins_rs_assistance = $_REQUEST['ins_rs_assistance'];
$ins_rent_car = $_REQUEST['ins_rent_car'];
$ins_excess_amount = $_REQUEST['ins_excess_amount'];
$ins_third_prty_limit1 = $_REQUEST['ins_third_prty_limit'];
$ins_em_med_expense = $_REQUEST['ins_emergency_med_exp'];
$ins_loss_of_per_belongings_set = $_REQUEST['ins_loss_of_per_belongings'];
$ins_selected_compny_data = $_REQUEST['ins_selected_compny_data'];

//User Data
$ins_fullname = $_REQUEST['ins_fullname'];
$ins_veh_model_year = $_REQUEST['ins_veh_model_year'];
$ins_model_detail = $_REQUEST['ins_model_detail'];
$ins_contact_num = $_REQUEST['ins_contact_num'];
$ins_veh_man_year = $_REQUEST['ins_veh_man_year'];
$veh_email_address = $_REQUEST['ins_emailaddress'];
$veh_modelno = $_REQUEST['ins_vehicle_brand'];
$veh_reg_dateF = $_REQUEST['ins_vehiclereg_date'];
$veh_city_ofreg = $_REQUEST['ins_cityof_reg'];
$ins_car_val = $_REQUEST['ins_car_value'];
$veh_country_first_dl = $_REQUEST['ins_country_first_dl'];
$veh_dob_date = $_REQUEST['ins_dob_date'];
$ins_total_drivexp = $_REQUEST['ins_total_drivexp'];

//Selected Data
$veh_brandnewcheck = $_REQUEST['veh_brandnewcheck'];
$current_ins_exp = $_REQUEST['current_ins_exp'];
$current_policy_comp_ins = $_REQUEST['current_policy_comp_ins'];
$cur_policy_agency_repair = $_REQUEST['cur_policy_agency_repair'];
$vehicle_is_gcc_spec = $_REQUEST['vehicle_is_gcc_spec'];
$not_for_commercial_purpose = $_REQUEST['not_for_commercial_purpose'];
$reg_under_myname = $_REQUEST['reg_under_myname'];
$claim_in_last12months = $_REQUEST['claim_in_last12months'];
$excess_amountpay = $_REQUEST['excess_amountpay'];
$need_veh_replacement = $_REQUEST['need_veh_replacement'];
$need_per_acc_cover = $_REQUEST['need_per_acc_cover'];
$need_out_uae_cover = $_REQUEST['need_out_uae_cover'];
$ins_cntry_fir_driv_lic = $_REQUEST['ins_cntry_fir_driv_lic'];
$ins_nationality_aj1 = $_REQUEST['ins_nationality_sel'];
$ins_form_type = $_REQUEST['form_type'];
$tpl_ambulanceCover_sess = $_REQUEST['tpl_ambulanceCover'];
$tpl_roadSide_assistance_sess = $_REQUEST['tpl_roadSide_assistance'];
$tpl_third_party_death_sess = $_REQUEST['tpl_third_party_death'];
?>

<div class="smart-wrap">
<div class="smart-forms smart-container wrap-4">

<form method="post" action="" id="form-ui22">
<div class="form-body">

<div class="frm-row main" >

<!-- main Content left-->
<div class="section colm colm8 main-colm7">

<div class="frm-row">
<div class="qp_box01 section colm colm3">
<div id="companylogo">
<p  id="companylogoinner">
<span id="company_logoname"></span>
</p>
</div>
</div>

<?php $current_date = date("d/m/Y"); ?>
<div class="qp_box09 section colm colm6">
<div class="qp_box09_inner091 boxtext02 section">Insurance Premium By <br><span id="comp_name"></span> - <span id="comp_class"></span></div>
<div class="qp_box09_inner091 boxtext02">
<?php
/* $sel_date_array = explode("/", $veh_policy_sdateF_sel);
$sel_date_array[0];
$sel_date_array[1] = $sel_date_array[1] + 1;
$sel_date_array[2] = $sel_date_array[2] + 1;
echo "$sel_date_array[0]/$sel_date_array[1]/$sel_date_array[2]"; */
?></div>

<div class="qp_box09_inner091 boxtext02">Your Premium is: <span id="ins_price"></span> AED - Excluding VAT</div>
</div>

<div class="qp_box03 section colm colm2">
<div class="qp_box09_inner091 boxtext03 section">Quote Ref. No:<br><?php $ins_compname_sel1 = mt_rand(562222,1000000);
echo "Topup-".$ins_compname_sel1; ?></div>
<div class="qp_box09_inner091 boxtext03">Date:<br><?php echo $current_date; ?></div>
</div>
</div>


<div class="frm-row">
<div class="qp_box05 section">
<div class="frm-row">
<div class="qp_box05_inner051 boxtext01 section colm colm3">Your Vehicle Model:</div>
<div class="qp_box05_inner051 boxtext02 section colm colm8"><span id="sel_vehicleModel"></span> <span id="sel_vehicleYear"></span> <span id="sel_vehicleBrand"></span> <span id="sel_vehicleModel_detail"></span> <span id="tpl_insuredVal"> - Insured Value is <span id="veh_model_price"><span id="ins_value"></span> <span> AED</span></span></span>
</div>
</div>
</div>

<div class="frm-row-x">
<div class="qp_H2Box section">--- Driver info as per Policy Requirements ---</div>
</div>
<?php //$ins_nationality_aj_show ?>
<script>
//jQuery(document).ready(function() {
//var veh_nationality = localStorage.getItem("veh_nationality");
////////console.log("veh_nationality.Chkout.."+veh_nationality);
//jQuery( "#chkout_nationality" ).html(veh_nationality);
//});
</script>

<div class="frm-row">
<div class="qp_box08 section">
<div class="frm-row">
<div class="qp_box08_inner081 boxtext01 section colm colm3">Your Name:</div>
<div class="qp_box08_inner081 boxtext02x section colm colm8"><span id="driv_name"></span></div>
<div class="qp_box08_inner081 boxtext01 section colm colm3">Your Nationality:</div>
<div class="qp_box08_inner081 boxtext02x section colm colm8"><span id="driv_nationality"></span></div>
<div class="qp_box08_inner081 boxtext01 section colm colm3">UAE Driving Experience:</div>
<div class="qp_box08_inner081 boxtext02 section colm colm8"><span id="driv_drivingExp"></span></div>

<div class="qp_box08_inner081 boxtext01 section colm colm3">Driver Date of Birth:</div>
<div class="qp_box08_inner081 boxtext02 section colm colm8"><span id="driv_DOB"></span></div>
<div class="qp_box08_inner081 boxtext01 section colm colm3">Your Email:</div>
<div class="qp_box08_inner081 boxtext02 section colm colm8"><span id="driv_email"></span></div>
</div>
</div>
</div>

<script>
jQuery(document).ready(function(jQuery){

var rent_car_val_get_chkout = localStorage.getItem("rent_car_check_val");
var pa_cover_driver_checkout = localStorage.getItem("pa_cover_driver_check");
var pa_cover_pass_checkout = localStorage.getItem("pa_cover_pass_check");
var pa_cover_driver_chk = localStorage.getItem("pacover_driver_checkout");

var car_repair_chk1 = localStorage.getItem("garrage_repair");
var car_repair_chk2 = localStorage.getItem("premier_workshop");
var car_repair_chk3 = localStorage.getItem("inside_agency");


/* if(car_repair_chk1 == "Garage Repair"){
jQuery("#car_repairin").append('Garrage Repair');
}
if(car_repair_chk2 == "Premier Workshop"){
jQuery("#car_repairin").append(' - Premier Workshop');
}
if(car_repair_chk3 == " - Inside Agency"){
jQuery("#car_repairin").append('Inside Agency');
}  */


jQuery("#tpl_pacoverDriverchk").html(pa_cover_driver_checkout);
jQuery("#tpl_pacoverPassangerchk").html(pa_cover_pass_checkout);

//////console.log("pa_cover_driver_checkout.."+pa_cover_driver_checkout);
//////console.log("pa_cover_pass_checkout.."+pa_cover_pass_checkout);

/* var form_type_set = localStorage.getItem("form_type");

if(form_type_set == 'tpl'){
jQuery("#ins").hide();
jQuery("#icluded").hide();
jQuery("#tpl").show();
jQuery("#tpl_name").show();
jQuery("#tpl_insuredVal").hide();
//////console.log("hide ins");
}else {
jQuery("#tpl").hide();
jQuery("#ins").show();
jQuery("#tpl_name").hide();
jQuery("#tpl_insuredVal").show();
//////console.log("hide tpl");
} */

/* 				jQuery("ul #outside_uae_cover_chkout").hide();
jQuery("ul #rent_aCar_chkout").hide();
jQuery("ul #pa_cover_driver_checkout").hide();

if(rent_car_val_get_chkout == "Yes"){
jQuery("#rent_aCar_chkout").show();
}else{
jQuery("#rent_aCar_chkout").hide();
} */

});
</script>

<?php
//if($ins_form_type_session_show != 'tpl'){ ?>
<div id="ins">
<div class="frm-row-x">
<div class="qp_H2Box section">---- Full Coverage Premium Details ----</div>
</div>

<div class="frm-row">
<div class="qp_box06 section">
<div class="frm-row">
<div class="qp_box06_inner061 boxtext05 section">Policy Feature Summary</div>
<div class="qp_box06_inner061 boxtext02 section colm colm6">- Auto Repair in: <span id="car_repairin"></span></div>
<div class="qp_box06_inner061 boxtext02 section colm colm5">- Third Party Limit: <span id="car_tpl"></span></div>
<div class="qp_box06_inner061 boxtext02 section colm colm6">- Personal Accident Cover for Driver: <span id="pa_cover_driver_chkout"></span></div>
<div class="qp_box06_inner061 boxtext02 section colm colm5">- Excess/Own Damage Cover: <span id="car_ExcessAmount"></span> AED</div>
<div class="qp_box06_inner061 boxtext02 section colm colm6">- Emergency Medical Expenses: <span id="car_EmergencyMedical"></span></div>
<div class="qp_box06_inner061 boxtext02 section colm colm5">- Replacement Car Rental: <span id="rent_aCar_chkout"></span> AED </div>
<div class="qp_box06_inner061 boxtext02 section colm colm6">- Loss of Personal Belongings: <span id="loss_per_belong_chkout"></span></div>
<div class="qp_box06_inner061 boxtext02 section colm colm5">- Additional Cover for Passangers: <span id="pa_cover_passangers"></span> AED</div>
<div class="qp_box06_inner061 boxtext02 section colm colm6">- Outside UAE Coverage: <span id="outside_uae_cover"></span> </div>
</div>
</div>
</div>
</div>


<?php// }
// else if($ins_form_type_session_show == 'tpl'){ ?>
<!-- <div id="tpl">
<div class="frm-row">
<div class="qp_H2Box section">------ TPL Policy Cover Details ------</div>
</div>

<div class="frm-row">
<div class="qp_box06 section">
<div class="frm-row">
<div class="qp_box06_inner061 boxtext05 section">Third Party Only Feature Summary</div>
<div class="qp_box06_inner061 boxtext02 section colm colm6">Third Party Limit: <?php //echo $ins_repair_cond_sel; ?></div>
<div class="qp_box06_inner061 boxtext02 section colm colm5">Third Party Death: <?php //echo $tpl_third_party_death_show; ?></div>
<div class="qp_box06_inner061 boxtext02 section colm colm6">Ambulance Cover: <span class="dataval "><i class="icomoon-checkmark-circle <?php //echo strtolower($tpl_ambulanceCover_show); ?>"></i></span></div>
<div class="qp_box06_inner061 boxtext02 section colm colm6">Road Side Assistance: <span class="dataval "><i class="icomoon-checkmark-circle <?php //echo strtolower($tpl_roadSide_assistance_show); ?>"></i></span></div>
<div class="qp_box06_inner061 boxtext02 section colm colm6">Personal Accident Cover for Driver: <span id="tpl_pacoverDriverchk"></span></div>
<div class="qp_box06_inner061 boxtext02 section colm colm5">Accident Cover for Passangers: <span id="tpl_pacoverPassangerchk"></span> Passangers</div>
</div>
</div>
</div>
</div> -->

<?php //} ?>


<script>
var label_array =
["Price",
"Company Name",
"Class",
"Discounts",
"Empty",
"Excluded Vehicles List",
"Vehicle Category",
"Min. Value",
"Max. Value",
"Rate",
"Minimum",
"No Claim Certificate",
"Self Declaration",
"Repair Condition",
"Personal Accident Cover for Driver",
"Accident Cover for Passengers",
"Rant a Car",
"Own Damage Cover",
"Out Side UAE Coverage",
"IF UAE License Age less than 1 Year",
"iF Age less than 25 year",
"Road-side Assistance",
"Third Party Limit",
"Windscreen Damage",
"Dent Repair",
"Off-Road & Desert Recovery",
"Emergency Medical Expenses",
"Ambulance Cover",
"Death or Bodily Injury",
"Loss of Personal Belongings",
"Fire and Theft Cover",
"Valet Parking Theft",
"Replacment of locks",
"Natural Calamity Cover",
"Car Registration / Renewal",
"Modified / Non-GCC Support"];
</script>


<div class="frm-row-x" id="icluded">
<div class="qp_box04 section">
<div class="frm-row">
<div class="qp_box04_inner041 boxtext05 section colm colm6">What's Included</div>
<div class="qp_box04_inner041x boxtext02 included section colm colm6">
<?php $all_sel_data = $ins_selected_compny_data_sel?>
<ul id="is_icludedul">
</ul>
<script>
function what_included(){
var compname3 = localStorage.getItem("compname2");
jQuery('#company_logoname').attr('class','');
jQuery('#company_logoname').addClass(compname3);

jQuery('#is_icludedul').html('');
var all_sel_data = JSON.parse(localStorage.getItem("dataSelected_full"));

//console.log("all_sel_data.."+all_sel_data);

var final_array =[];
var included =[];

jQuery.each( all_sel_data, function( i, val ) {
if(all_sel_data[i] == "TRUE"){
	final_array[i] = label_array[i];
}
});
////console.log(final_array);
var final_array1 = final_array.filter(function(e){ return e.replace(/(\r\n|\n|\r)/gm,"")});


////console.log("final_array1.."+final_array1);

jQuery.each( final_array1, function( i, traverse ) {
////console.log(traverse);
jQuery('#is_icludedul').append('<li class="included">'+traverse+'</li>');
});
what_included1();
}
</script>
</div>
<div class="qp_box04_inner041 boxtext05 section">What's Not Included</div>
<div class="qp_box04_inner041x boxtext02 not_included section colm colm5">
<ul id="not_icludedul">
</ul>
<script>

function what_included1(){

	jQuery('#not_icludedul').html('');
	var all_sel_data = JSON.parse(localStorage.getItem("dataSelected_full"));
	//////console.log("all_sel_data.."+all_sel_data);
	var final_array2 =[];
	var included =[];

	jQuery.each( all_sel_data, function( i, val ) {

	if( all_sel_data[i] == "FALSE"){
		final_array2[i] = label_array[i];
	}

	});
	////console.log(final_array);
	var final_array1 = final_array2.filter(function(e){ return e.replace(/(\r\n|\n|\r)/gm,"")});

	jQuery.each( final_array1, function( i, traverse ) {
	//console.log(traverse);
	jQuery('#not_icludedul').append('<li class="not-included">'+traverse+'</li>');
	});
}


</script>
</div>
</div>
</div>
</div>


<?php
$pdf_companyName = strtolower($ins_compname_sel);
//echo $pdf_companyName;
if($pdf_companyName == "qatar general insurance"){
$pdf_fileLink = 'https://insurancetopup.com/xio_outww2/Policy_PDF/Qatar_General_Insurance_Policy.pdf';
}
else if($pdf_companyName == "watania insurance"){
$pdf_fileLink = 'https://insurancetopup.com/xio_outww2/Policy_PDF/Watania_Insurance_Policy.pdf';
}
else if($pdf_companyName == "watania"){
$pdf_fileLink = 'https://insurancetopup.com/xio_outww2/Policy_PDF/Watania_Insurance_Policy.pdf';
}
else if($pdf_companyName == "insurance house"){
$pdf_fileLink = 'https://insurancetopup.com/xio_outww2/Policy_PDF/Insurance_House_Policy.pdf';
}
else if($pdf_companyName == "adamjee insurance"){
$pdf_fileLink = 'https://insurancetopup.com/xio_outww2/Policy_PDF/Adamjee_Insurance_Policy.pdf';
}
else if($pdf_companyName == "the oriental insurance"){
$pdf_fileLink = 'https://insurancetopup.com/xio_outww2/Policy_PDF/The_Oriental_Insurance_Policy.pdf';
}
else if($pdf_companyName == "tokio marine"){
$pdf_fileLink = 'https://insurancetopup.com/xio_outww2/Policy_PDF/Tokio_Marine_Policy.pdf';
}
else if($pdf_companyName == "dubai national insurance"){
$pdf_fileLink = 'https://insurancetopup.com/xio_outww2/Policy_PDF/Dubai_National_Insurance_Policy.pdf';
}
?>

<div class="frm-row">
<div class="qp_box02 boxtext02 section">View Policy Documents <a href="<?php echo $pdf_fileLink;?>">Insurance Company PDF File</a></div>
</div>

<div class="frm-row">
<div class="qp_box02x boxtext06 section">
<div class="boxtext01 section">Disclaimer</div>
Dear Customer! You must tell us immediately if any of the information stated in your quotation is in-correct. Failure to do may invalidate your policy. Because no list of questions can be exhaustive, so please consider carefully whether there is any other material information known to you which could influence our acceptance and assessment of the risk.<br>
Please check whether the value indicated represents the correct market value of your vehicle. Proof may be requested. Rule of thumb is that the car depreciates 15% in value each year after purchase.</div>
</div>
</div></div><!-- end colm7 section -->
<!-- main Content Left -->

<!-- Sidebar right -->
<!-- <div class="frm-row">
<div class="section colm colm3 rightsidebar">
<input type="button" value="Proceed to Checkout" id="proceed_checkout" class="btn btn-primary" >
</div>
</div> -->
<!-- end Sidebar right -->

<!-- Sidebar right -->
<div class="frm-row">
<div class="section colm colm4 main-colmX">
<h3>Secure Checkout</h3>
<h5>Cart Totals</h5>
<!--<h2><span id="ins_price"></span> AED</h2>-->
<div class="base_price_block colm colm6">
<h5 class="main-colmX-txt">Subtotal:</h5>
<h5 class="main-colmX-txt">5% VAT:</h5>
<h5 class="main-colmX-txt">Total:</h5>

</div>
<div class="base_price_block colm colm6">
<h5 class="main-colmX-txt2"><span id="ins_price1"></span> AED</h5>
<h5 class="main-colmX-txt2"><span id="vat_applied">5%</span></h5>
<h5 class="main-colmX-txt2"><span id="ins_total_after_vat"></span> AED</h5>
</div>

<div id="proceed_to_checkout">
<input type="button" value="Proceed to Checkout" id="proceed_checkout" class="btn btn-primary" >
</div>
</div>
</div>


</div>
</div>
</form>

</div>
</div>
</div>



<!-- Policy Email Template -->
<div id="email_container">

<div class="p_txt">
<p>Dear Valuable Customer,<br />
First of all, we would like to Thank you, for choosing your auto policy from our platform. Your order is on hold, because the payment for your car insurance policy is still pending. To make sure you’re covered as soon as possible, we will be in touch to arrange an address to pick up the cost (you can pay by Cash/Credit-Card to our courier guy) of your policy. You should expect a call within 20 mints time. in our business hours (Saturday to Thursday, 8:30AM – 8.00PM).<br />
Following documents are required for process your auto policy:
<ul>
<li>Your Vehicle Registration Copy</li>
<li>Your Driving Licence Copy</li>
<li>Your Emirates ID Copy</li>
<li>No Claims Certificate (if you have - optional)</li>
</ul>
Please send your all documents as soon as possible via email, and its mentioned below.</p>

<p class="small_txt">Please Note:<br />
Once we have collected all the relevant documents and payment confirmation on the above details (invoice), we will issue your chosen insurance policy cover, kindly acknowledge that sometimes vehicle value needs the approval. in some cases, issuance of the policy requires a vehicle survey, in that case you will be asked to provide the current dated photograph of your vehicle. Once all the requirements are completed we’ll issue your policy. Please note that some policies can only be approved during working hours.<br />
Also! The cover will only start and become effective once the original policy document has been issued.</p>
</div>


<div class="section group">
<div class="col span_2_of_2 ml_bgmail"><p>doc@insurancetopup.com</p></div>
</div>

<div style="text-align: center"> <h3>Comprehensive Policy Cover Information</h3> </div>

<div class="section group">
<div class="col span_2_of_2 ml_txt">
<ul>
<li>Insurance Premium By: <strong><span id="comp_name_e"></span></strong></li>
<li>Your Name: <strong><span id="driv_name_e"></span></strong></li>
<li>Your Current Vehicle Value: <strong><span id="veh_value_e"></span></strong></li>
<li>Your Vehicle Model: <strong><span id="sel_vehicleModel_e"></span></strong></li>
<li>Contact Number: <strong><span id="contact_num_email"></span></strong></li>
<li>Driving Experience: <strong><span id="driv_drivingExp_e"></span></strong></li>

</ul></div>
</div>

<div class="section group"> <div class="col span_2_of_2 hr">.</div> </div>


<div class="section group">
<div class="col span_1_of_2 ml_txt">
<ul>
<li>Replacement Car Rental: <strong><span id="driv_rentcar_e"></span></strong></li>
<li>OutSide UAE Coverage: <strong><span id="driv_OutsideUaeCover_e"></span></strong></li>
<li>Third Party Limit: <strong><span id="driv_thirdPLimit_e"></span></strong></li>
<li>Excess / Own-Damage Cover: <strong><span id="driv_ownDamageCover_e"></span></strong></li>

</ul></div>

<div class="col span_1_of_2 ml_txt">
<ul>
<li>Accident Cover for Passengers: <strong><span id="driv_acc_coverpassanger_e"></span></strong></li>
<li>I choosed Auto Repair from: <strong><span id="driv_carRepairIn_e"></span></strong></li>
<li>Personal Accident Cover to Driver: <strong><span id="driv_paCoverDriver_e"></span></strong></li>
<li>Emergency Medical Expenses: <strong><span id="driv_EmMedicalExp_e"></span></strong></li>
<li>-</li>

</ul></div>
</div>

<p class="p_txt">And more policy features you will see in origanl policy cover when we realse you after all basic requirements by insurance company</p>


<div class="section group">
<div class="col span_2_of_2 ml_txt">Cover Information
<ul>
<li>My Current Policy is Comprehensive Insurance: <strong><span id="my_curr_pol_comprehensive_e"></span></strong></li>
<li>My Vehicle is GCC Specification / Not Modified: <strong><span id="my_veh_is_gccspec_e"></span></strong></li>
<li>My Current Insurance is Not Expired: <strong><span id="my_curr_ins_notexp_e"></span></strong></li>
<li>My Vehicle is Not for Commercial Purpose: <strong><span id="my_veh_not_commercial_e"></span></strong></li>
<li>Your Policy Start Date is: <strong>-</strong></li>
</ul></div>
</div>


<div class="_">
<p class="small_txt2">Please confirm that all the information you’ve provided above is correct. You can do this by simply responding to this email saying ‘Yes i confirm’. And If the information above is incorrect, Please! talk to our insurance agent on this number +971-4-2485886</p>
<p class="p_txt">If you have any questions feel free to Contact Us.<br /><br />
Thank you Again,<br />
Insurance TopUp Team.</p>
</div>


</div>
<!-- Policy Email Template end -->

<?php
}
