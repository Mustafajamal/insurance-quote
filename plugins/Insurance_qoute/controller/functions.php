<?php

if ( ! defined( 'ABSPATH' ) )
	exit;


function isProductTaxiIsInCart($order_id) {
	global $wpdb;
	$order = new WC_Order( $order_id );
	$items = $order->get_items();
	$checkIfInCart=0;
	foreach ($items as $key => $product ) {
		$product_id = $product['product_id'];
		if($product_id == get_option('stern_taxi_fare_product_id_wc')) {
			$checkIfInCart = $checkIfInCart+1;
		}
	}
	if($checkIfInCart>0) {
		return true;
	} else {
		return false;
	}
}

function createProductAndSaveId() {
	$post_id = create_product();
	update_option('stern_taxi_fare_product_id_wc',$post_id);
}

function create_product(){
    $userID = 1;
    if(get_current_user_id()){
        $userID = get_current_user_id();
    }
    $post = array(
        'post_author' => $userID,
        'post_content' => 'Used For Insurance Quote System',
        'post_status' => 'publish',
        'post_title' => 'Insurance Policy Cover',
        'post_type' => 'product',
    );

    $post_id = wp_insert_post($post);
    update_post_meta($post_id, '_stock_status', 'instock');
    update_post_meta($post_id, '_tax_status', 'none');
    update_post_meta($post_id, '_tax_class',  'zero-rate');
    update_post_meta($post_id, '_visibility', 'hidden');
    update_post_meta($post_id, '_stock', '');
    update_post_meta($post_id, '_virtual', 'yes');
	
    update_post_meta($post_id, '_featured', 'no');
    update_post_meta($post_id, '_manage_stock', "no" );
    update_post_meta($post_id, '_sold_individually', "yes" );
    //update_post_meta($post_id, '_sku', 'checkout-taxi-fare');
	update_post_meta( $post_id, '_regular_price', "0" );
	update_post_meta($post_id, '_price', '0');
	update_option('post_id',$post_id);
    return $post_id;
}

function updatePost(){
	
    $post_id1 = get_option('stern_taxi_fare_product_id_wc');
	$custom_PriceFinal = WC()->session->get( 'estimated_fare' );
	update_post_meta( $post_id1, '_regular_price', $custom_PriceFinal);
	update_post_meta($post_id1, '_price', $custom_PriceFinal);	
}

function isBootstrapSelectEnabale() {
	if (get_option('stern_taxi_fare_Bootstrap_select') == "" or get_option('stern_taxi_fare_Bootstrap_select') == "false") {
		return "true";
	} else  {
		return "true";
	}
}

function launchForm($atts) {
	if ( isWooCommerceActive() ) {
		if (true) {			 //if (isFopenOk()) {
			showForm1($atts);			
		} else {
			echo __('Insurance Quote System: Option allow_url_fopen is not allowed on your host', 'stern_taxi_fare');
		}
	} else {
		echo __('Insurance Quote System: Please install plugin WooCommerce', 'stern_taxi_fare');
	}
}//Form TPLfunction launchFormTpl($atts) {	if ( isWooCommerceActive() ) {		if (true) {			 //if (isFopenOk()) {			showForm2($atts);		} else {			echo __('Insurance Qoute System: Option allow_url_fopen is not allowed on your host', 'stern_taxi_fare');		}	} else {		echo __('Insurance Qoute System: Please install plugin WooCommerce', 'stern_taxi_fare');	}}

function isProductCreated() {
	if(substr(esc_url( get_permalink(get_option('stern_taxi_fare_product_id_wc')) ), -10) == "insu-qote/") {
		return true;
	} else {
		if(substr(esc_url( get_permalink(get_option('stern_taxi_fare_product_id_wc')) ), -9) == "insu-qote") {
			return true;
		} else {
			return false;
		}
	}
}