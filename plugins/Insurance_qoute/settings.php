<?php

/*
 Plugin Name: Insurance Qoute System
 Description: This plugin calculates fare, distance and duration. It uses Woocommerce to pay online. Use [stern-taxi-fare] shortcode to display fare calculator.
 Version: 2.2.7
 Plugin URI: http://example.com/
 Author: Example.com
*/


if ( ! defined( 'ABSPATH' ) )
	exit;

if ( isWooCommerceActive()) {

	register_activation_hook( __FILE__,  'install' );
	
	//if( is_page( array( 'auto-insurance-quote', 'third-party-liability-insurance-quote' ) )){
		add_action('wp_enqueue_scripts', 'stern_taxi_fares_script_front_css');
		add_action('wp_enqueue_scripts', 'stern_taxi_fares_script_front_js');
	//}
	//add_action('admin_enqueue_scripts', 'stern_taxi_fares_script_back_js');

	//add_action('admin_enqueue_scripts', 'stern_taxi_fares_script_back_css');
	//add_action( 'plugins_loaded', 'myplugin_load_textdomain' );

	add_filter( 'plugin_action_links_' . plugin_basename( __FILE__ ), 'ts_add_plugin_action_links' );
	
	
	add_action('init', 'stern_taxi_fare_register_shortcodes1');
	add_action('woocommerce_checkout_update_order_meta',function( $order_id, $posted ) {
	$ins_pdf_imageset = get_option( 'ins_pdf_image');

    $order = wc_get_order( $order_id );
    $order->update_meta_data( 'policy_data', $ins_pdf_imageset);
    $order->save();
	} , 10, 2);   

	include 'controller/stern_taxi_fare_admin.php';
	include 'templates/form/showForm1.php';
	include 'templates/form/showForm2.php';
	//include 'templates/checkout/checkout.php';
	include 'controller/functions.php';
	include 'templates/form/showResults.php';
	include 'controller/stern_taxi_fare_ajax.php';
	include 'controller/stern_taxi_fare_ajax_admin.php';
	include 'templates/form/showButtonCheckOut.php';
	include 'templates/admin/settings.php';
	include 'templates/admin/woocommerceStatus.php';


} else {

    function stern_ticketing_events_admin_notice() { ?>
        <div class="error">
            <p><?php _e( 'Insurance Qoute: Please Install WooCommerce First before activating this Plugin. You can download WooCommerce from <a href="http://wordpress.org/plugins/woocommerce/">here</a>.', 'stern-ticketing-events' ); ?></p>
        </div>
    <?php }
    add_action( 'admin_notices', 'stern_ticketing_events_admin_notice' );
	//deactivate_plugins( plugin_basename( __FILE__ ) );

	//wp_die( _e( 'Please Install WooCommerce First before activating this Plugin. You can download WooCommerce from <a href="http://wordpress.org/plugins/woocommerce/">here</a>.', 'stern-ticketing-events' ) );

}

function stern_taxi_fare_register_shortcodes1() {

   //wp_register_script('jquery_enqueue_default', plugins_url('js/jquery.min.js', __FILE__ ),array('jquery'));
   //wp_enqueue_script('jquery_enqueue_default');

   add_shortcode('insurance-qoute-system-tpl', 'stern_taxi_fare2');
   add_shortcode('insurance-qoute-system', 'stern_taxi_fare1');
   add_action( 'user_register', 'myplugin_registration_save', 10, 1 );
}

/* 
function stern_taxi_fare_register_shortcodes2() {
   add_shortcode('insurance-qoute-system-tpl', 'stern_taxi_fare2');
} */

function myplugin_registration_save( $user_id ) {
    $full_name_key = "full_name";
	$current_order_id = get_option("c_id");	
	update_field($full_name_key, $full_name_key, [$current_order_id]); 	
}


function stern_taxi_fare1($atts) {

	ob_start();
	launchForm($atts);
	return ob_get_clean();
}

function stern_taxi_fare2($atts) {

	ob_start();
	//launchFormTpl($atts);
	showForm2($atts);
	return ob_get_clean();
}


function isWooCommerceActive() {
	if(is_multisite() ) {
		return true;
	} else {
		if(
			//is_plugin_active( 'woocommerce/woocommerce.php') or
			in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) )
		//	or is_plugin_active_for_network( 'woocommerce/woocommerce.php')
		//	or is_network_only_plugin('woocommerce/woocommerce.php')
		) {
			return true;
		} else {
			return false;
		}
	}
}

 function install() {
	$exist = get_option('stern_taxi_fare_product_id_wc');
	//saveVersion();
	//sendInfosDebug();
	if($exist){
		if ( get_post_status ( $exist ) ) {
			return true;
		} else {
			createProductAndSaveId();
		}
	} else {
		createProductAndSaveId();
	}
}

//Open Checkout in new tab

//Add back button in Checkout
/* add_action( 'woocommerce_after_checkout_form', 'my_second_function_sample', 10 );
function my_second_function_sample() {
  global $product;
  echo ' <button type="button" id="checkout_backbtn" onclick="history.back();"> Go back </button> '; 
} */

function ts_add_plugin_action_links( $links ) {

	return array_merge(
		array(
			'settings' => '<a href="' . get_bloginfo( 'wpurl' ) . '/wp-admin/admin.php?page=SternTaxiPage">Settings</a>'
		),
		$links
	);
}

function stern_taxi_fares_script_back_css() {

		//wp_register_style( 'stern_jquery_ui_css', plugins_url('css/jquery-ui.css', __FILE__ ));
		//wp_enqueue_style( 'stern_jquery_ui_css');

		//wp_register_style( 'stern_fullCalendar_MIN_css', plugins_url('css/fullcalendar.min.css', __FILE__ ));
		//wp_enqueue_style( 'stern_fullCalendar_MIN_css');
}

function stern_taxi_fares_script_front_css() {
		// /* CSS */
if( is_page( array( 'auto-insurance-quote', 'tpl-insurance-quote' ) )){
		wp_register_style('stern_bootstrapValidatorMinCSS', plugins_url('css/bootstrapValidator.min.css',__FILE__));
		wp_register_style('smartForms', plugins_url('css/smart-forms.css',__FILE__));

        wp_enqueue_style('smartForms');
        wp_enqueue_style('stern_bootstrapValidatorMinCSS');

		wp_register_style( 'ins_steps_css', plugins_url('css/jquery.steps.css', __FILE__ ));
		wp_enqueue_style( 'ins_steps_css');

		wp_register_style( 'ins_stepper', plugins_url('css/jquery.stepper.css', __FILE__ ));
		wp_enqueue_style( 'ins_stepper');

		wp_register_style( 'ins_sfcustom', plugins_url('css/smartforms-custom.css', __FILE__ ));
		wp_enqueue_style( 'ins_sfcustom');

		wp_register_style( 'ins_cssptable1', plugins_url('cssptable/pricing-widget-structure.css', __FILE__ ));
		wp_enqueue_style( 'ins_cssptable1');

		wp_register_style( 'ins_cssptable2', plugins_url('cssptable/pricing-widget-settings.css', __FILE__ ));
		wp_enqueue_style( 'ins_cssptable2');

		wp_register_style( 'ins_cssptable3', plugins_url('cssptable/pricing-widget-theme.css', __FILE__ ));
		wp_enqueue_style( 'ins_cssptable3');

		wp_register_style( 'ins_cssptable4', plugins_url('cssptable/pricing-widget-responsive.css', __FILE__ ));
		wp_enqueue_style( 'ins_cssptable4');
		
		wp_register_style( 'ins_smartaddon', plugins_url('css/smart-addons.css', __FILE__ ));
		wp_enqueue_style( 'ins_smartaddon');

		wp_register_style( 'ins_smartforms_modal', plugins_url('css/smartforms-modal.css', __FILE__ ));
		wp_enqueue_style( 'ins_smartforms_modal');
			
		wp_register_style( 'ins_popupn', plugins_url('css/popup.min.css', __FILE__ ));
		wp_enqueue_style( 'ins_popupn');

        wp_register_style('stern_taxi_fare_datetimepicker', plugins_url('css/bootstrap-datetimepicker.css',__FILE__));
        wp_enqueue_style('stern_taxi_fare_datetimepicker');

        wp_register_style('ins_style', plugins_url('css/ins_style.css',__FILE__));
        wp_enqueue_style('ins_style');
		wp_register_style('tabel_style', plugins_url('css/tabel_style.css',__FILE__));
        wp_enqueue_style('tabel_style');
}
}



/**
 * Add the field to the checkout page
 */

//add_action('woocommerce_after_order_notes', 'customise_checkout_field');

function customise_checkout_field($checkout){
	//Get data from PHP Sessions
	session_start();  	
	$ins_price_sel = $_SESSION["ins_price"];
	$ins_compname_sel = $_SESSION["ins_compname"];
	$ins_discount_sel = $_SESSION["ins_discount"];
	$ins_repair_cond_sel = $_SESSION["ins_repair_cond"];
	$ins_dl_less_than_year_sel = $_SESSION["ins_dl_less_than_year"];
	$ins_less_than_year_sel = $_SESSION["ins_less_than_year"];
	$ins_os_coverage_sel = $_SESSION["ins_os_coverage"];
	$ins_pa_driver_sel = $_SESSION["ins_pa_cvr_driver"];
	$ins_papp_sel = $_SESSION["ins_papp"];
	$ins_rs_assistance_sel = $_SESSION["ins_rs_assistance"];
	$ins_rent_car_sel = $_SESSION["ins_rent_car"];
	$ins_excess_amount_sel = $_SESSION["ins_excess_amount"];
	$ins_em_med_expense_sel = $_SESSION["ins_em_med_expense_set"];
	$ins_loss_of_per_belongings_sel = $_SESSION["ins_loss_of_per_belongings_session"];
	$ins_selected_compny_data_sel = $_SESSION["ins_selected_compny_data_ses"];

	//User Data
	$ins_fullname = $_SESSION["ins_fullname"];
	$ins_vehicle_brand_sel = $_SESSION["ins_vehicle_brand"];
	$ins_contact_num_sel = $_SESSION["ins_contact_num"];
	$ins_veh_man_year_sel = $_SESSION["ins_veh_man_year"];
	$veh_email_address_sel = $_SESSION["veh_email_address"];
	$veh_modelno_sel = $_SESSION["veh_modelno"];
	$veh_reg_dateF_sel = $_SESSION["veh_reg_dateF"];
	$veh_city_ofreg = $_SESSION["veh_city_ofreg"];
	$veh_car_val_sel = $_SESSION["veh_car_val"];
	$veh_policy_sdateF_sel = $_SESSION["veh_policy_sdateF"];
	$veh_model_detail = $_SESSION["veh_model_detail"];
	$veh_country_first_dl = $_SESSION["veh_country_first_dl"];
	$veh_dob_date = $_SESSION["ins_dob_date"];
	$veh_model_year = $_SESSION["ins_model_year"];
	$veh_cntry_first_dl = $_SESSION["ins_cntry_fir_driv_lic"];
	$veh_third_prty_limit = $_SESSION["ins_third_prty_limits"];
	
	//Selected Data
	$veh_brandnewcheck_sel = $_SESSION["veh_brandnewcheck"];	
	$current_ins_exp_sel = $_SESSION["current_ins_exp"];	
	$current_policy_comp_ins_sel = $_SESSION["current_policy_comp_ins"];	
	$cur_policy_agency_repair_sel = $_SESSION["cur_policy_agency_repair"];	
	$vehicle_is_gcc_spec_sel = $_SESSION["vehicle_is_gcc_spec"];	
	$not_for_commercial_purpose_sel = $_SESSION["not_for_commercial_purpose"];	
	$reg_under_myname_sel = $_SESSION["reg_under_myname"];	
	$claim_in_last12months_sel = $_SESSION["claim_in_last12months"];	
	$excess_amountpay_sel = $_SESSION["excess_amountpay"];	
	$need_veh_replacement_sel = $_SESSION["need_veh_replacement"];	
	$need_per_acc_cover_sel = $_SESSION["need_per_acc_cover"];	
	$need_out_uae_cover_sel = $_SESSION["need_out_uae_cover"];	
	$ins_total_drivexp_set = $_SESSION["ins_total_drivexp_session"];	
	$ins_nationality_aj_show = $_SESSION["ins_nationality_aj"];
	$ins_form_type_session_show = $_SESSION["ins_form_type_session"];
	$tpl_ambulanceCover_show = $_SESSION["tpl_ambulanceCover_set"];
	$tpl_roadSide_assistance_show = $_SESSION["tpl_roadSide_assistance_set"];
	$tpl_third_party_death_show = $_SESSION["tpl_third_party_death_set"]; 
	
	//setting data to options.
	update_option( 'ins_fullname', $ins_fullname );
	?>
		<div class="smart-wrap">
    	<div class="smart-forms smart-container wrap-0">
		
		<form method="post" action="" id="form-ui22">
			<div class="form-body">
		
			<div class="frm-row main" >

			<!-- main Content left--> 
  			<div class="section colm colm7 main-colm7">
			
			<div class="frm-row">
			<div class="qp_box01 section colm colm3">
			<div id="companylogo">
				<p  id="companylogoinner">
				<span  class="<?php echo strtolower($ins_compname_sel); ?>"></span>
				</p>
			</div>
			</div>
			<?php $current_date = date("d/m/Y"); ?>
			<div class="qp_box09 section colm colm6">
			<div class="qp_box09_inner091 boxtext02 section"><span id="tpl_name">TPL</span> insurance premium by <br><?php echo $ins_compname_sel; ?></div>
			<div class="qp_box09_inner091 boxtext02">Policy Start From: <?php echo $veh_policy_sdateF_sel; ?> -to- <?php 
			
			 $sel_date_array = explode("/", $veh_policy_sdateF_sel);
			 $sel_date_array[0];
			 $sel_date_array[1] = $sel_date_array[1] + 1;
			 $sel_date_array[2] = $sel_date_array[2] + 1;
			 echo "$sel_date_array[0]/$sel_date_array[1]/$sel_date_array[2]";
			
			?></div>
			<div class="qp_box09_inner091 boxtext02">Your Premium is: <?php echo $ins_price_sel; ?> AED</div>
			</div>

			<div class="qp_box03 section colm colm2">
			<div class="qp_box09_inner091 boxtext03 section">Quote Ref. No.<br><?php $ins_compname_sel1 = mt_rand(562222,1000000);
			echo "Topup-".$ins_compname_sel1; ?></div>
			<div class="qp_box09_inner091 boxtext03">Date:<br><?php echo $current_date; ?></div>
			</div>
			</div>


			<div class="frm-row">
			<div class="qp_box05 section">
			<div class="frm-row">
			<div class="qp_box05_inner051 boxtext01 section colm colm3">Vehicle Model:</div>
			<div class="qp_box05_inner051 boxtext02 section colm colm8"><?php echo $veh_modelno_sel; ?> <?php echo $veh_model_year." ".$ins_vehicle_brand_sel." ".$veh_model_detail;?><span id="tpl_insuredVal"> - insured value <span id="veh_model_price"><?php echo $veh_car_val_sel; ?><span> AED</span></span></span>		
			</div>
			</div>
			</div>

			<div class="frm-row">
			<div class="qp_H2Box section">------ Driver info as per Policy Requirements ------</div>
			</div>
			<?php //$ins_nationality_aj_show ?>
			<script>
				//jQuery(document).ready(function() {
					//var veh_nationality = localStorage.getItem("veh_nationality");
					//console.log("veh_nationality.Chkout.."+veh_nationality);
					//jQuery( "#chkout_nationality" ).html(veh_nationality);
				//});
			</script>			
			
			<div class="frm-row">
			<div class="qp_box08 section">
			<div class="frm-row">
			<div class="qp_box08_inner081 boxtext01 section colm colm3">Name:</div>
			<div class="qp_box08_inner081 boxtext02 section colm colm8"><?php echo $ins_fullname; ?></div>
			<div class="qp_box08_inner081 boxtext01 section colm colm3">Nationality:</div>
			<div class="qp_box08_inner081 boxtext02 section colm colm8"><?php echo $ins_nationality_aj_show; ?></div>
			<div class="qp_box08_inner081 boxtext01 section colm colm3">Driving Experience:</div>
			<div class="qp_box08_inner081 boxtext02 section colm colm8"><?php echo $ins_total_drivexp_set; ?></div>
			<div class="qp_box08_inner081 boxtext01 section colm colm3">First Licence Country:</div>
			<div class="qp_box08_inner081 boxtext02 section colm colm8"><?php echo $veh_cntry_first_dl; ?></div>						
			<div class="qp_box08_inner081 boxtext01 section colm colm3">Date of Birth:</div>
			<div class="qp_box08_inner081 boxtext02 section colm colm8"><?php echo $veh_dob_date; ?></div>
			<div class="qp_box08_inner081 boxtext01 section colm colm3">Email:</div>
			<div class="qp_box08_inner081 boxtext02 section colm colm8"><?php echo $veh_email_address_sel; ?></div>
			</div>
			</div>
			</div>
			
			<script>
				jQuery(document).ready(function(jQuery){
				
				var rent_car_val_get_chkout = localStorage.getItem("rent_car_check_val");			
				var pa_cover_driver_checkout = localStorage.getItem("pa_cover_driver_check");
				var pa_cover_pass_checkout = localStorage.getItem("pa_cover_pass_check");
				var pa_cover_driver_chk = localStorage.getItem("pacover_driver_checkout");
				var pa_coverpassangersChkout = localStorage.getItem("pa_coverpassangersChkout");
				
				var car_repair_chk1 = localStorage.getItem("garrage_repair");
				var car_repair_chk2 = localStorage.getItem("premier_workshop");
				var car_repair_chk3 = localStorage.getItem("inside_agency");
				
				if(car_repair_chk1 == "Garage Repair"){
					jQuery("#car_repairin").append('Garrage Repair');
				}
				if(car_repair_chk2 == "Premier Workshop"){
					jQuery("#car_repairin").append(' - Premier Workshop');
				}
				if(car_repair_chk3 == " - Inside Agency"){
					jQuery("#car_repairin").append('Inside Agency');
				}
				
				if(pa_cover_driver_chk != "NULL"){
					jQuery("#pa_cover_driver_chkout").append('120 AED');
				}
				if(pa_cover_driver_chk == "NULL"){
					jQuery("#pa_cover_driver_chkout").append('NULL');
				}
				
				if(pa_coverpassangersChkout >= 1){
					jQuery("#pa_cover_passangers").html(pa_coverpassangersChkout);
				}else{
					jQuery("#pa_cover_passangers").html("NULL");
				}	
				
				
				jQuery("#tpl_pacoverDriverchk").html(pa_cover_driver_checkout);
				jQuery("#tpl_pacoverPassangerchk").html(pa_cover_pass_checkout);
				
				console.log("pa_cover_driver_checkout.."+pa_cover_driver_checkout);
				console.log("pa_cover_pass_checkout.."+pa_cover_pass_checkout);
				
				var form_type_set = localStorage.getItem("form_type");
				
				if(form_type_set == 'tpl'){
					jQuery("#ins").hide();
					jQuery("#icluded").hide();
					jQuery("#tpl").show();
					jQuery("#tpl_name").show();
					jQuery("#tpl_insuredVal").hide();
					console.log("hide ins");
				}else {
					jQuery("#tpl").hide();
					jQuery("#ins").show();
					jQuery("#tpl_name").hide();
					jQuery("#tpl_insuredVal").show();
					console.log("hide tpl");
				}
				
/* 				jQuery("ul #outside_uae_cover_chkout").hide();
				jQuery("ul #rent_aCar_chkout").hide();
				jQuery("ul #pa_cover_driver_checkout").hide();
				
				if(rent_car_val_get_chkout == "Yes"){
					jQuery("#rent_aCar_chkout").show();
				}else{
					jQuery("#rent_aCar_chkout").hide();
				} */
				
				
				});
				</script>
			<?php 			
			//if($ins_form_type_session_show != 'tpl'){ ?>
			<div id="ins">
			<div class="frm-row" >
			<div class="qp_H2Box section">------ Premium Cover Details ------</div>
			</div>

			<div class="frm-row">
			<div class="qp_box06 section">
			<div class="frm-row">
			<div class="qp_box06_inner061 boxtext05 section">Policy Feature Summary</div>
			<div class="qp_box06_inner061 boxtext02 section colm colm6">Car Repair in <span id="car_repairin"></span></div>
			<div class="qp_box06_inner061 boxtext02 section colm colm5">Third Party Limit <?php echo $veh_third_prty_limit; ?></div>
			<div class="qp_box06_inner061 boxtext02 section colm colm6">Personal Accident Cover for Driver <span id="pa_cover_driver_chkout"></span></div>
			<div class="qp_box06_inner061 boxtext02 section colm colm5">Excess/Own Damage Cover <?php echo $ins_excess_amount_sel; ?></div>
			<div class="qp_box06_inner061 boxtext02 section colm colm6">Emergency Medical Expenses <?php echo $ins_em_med_expense_sel; ?></div>
			<div class="qp_box06_inner061 boxtext02 section colm colm5">Rant a Car <span id="rent_aCar_chkout"><?php echo $ins_rent_car_sel; ?></span> </div>
			<div class="qp_box06_inner061 boxtext02 section colm colm6">Loss of Personal Belongings <span id="loss_per_belong_chkout"><?php echo $ins_loss_of_per_belongings_sel; ?></span></div>
			<div class="qp_box06_inner061 boxtext02 section colm colm5">Additional Cover for Passangers <span id="pa_cover_passangers"></span></div>			
			<div class="qp_box06_inner061 boxtext02 section colm colm6">Outside UAE Coverage <?php echo $ins_os_coverage_sel; ?></div>
			</div>
			</div>
			</div>
			</div>
			
			<?php// } 
			// else if($ins_form_type_session_show == 'tpl'){ ?>
			
			<div id="tpl">
			<div class="frm-row">
			<div class="qp_H2Box section">------ TPL Policy Cover Details ------</div>
			</div>

			<div class="frm-row">
				<div class="qp_box06 section">
				<div class="frm-row">
				<div class="qp_box06_inner061 boxtext05 section">Third Party Only Feature Summary</div>
				<div class="qp_box06_inner061 boxtext02 section colm colm6">Third Party Limit: <?php echo $ins_repair_cond_sel; ?></div>
				<div class="qp_box06_inner061 boxtext02 section colm colm5">Third Party Death: <?php echo $tpl_third_party_death_show; ?></div>
				<div class="qp_box06_inner061 boxtext02 section colm colm6">Ambulance Cover: <span class="dataval "><i class="icomoon-checkmark-circle <?php echo strtolower($tpl_ambulanceCover_show); ?>"></i></span></div>
				<div class="qp_box06_inner061 boxtext02 section colm colm6">Road Side Assistance: <span class="dataval "><i class="icomoon-checkmark-circle <?php echo strtolower($tpl_roadSide_assistance_show); ?>"></i></span></div>				
				<div class="qp_box06_inner061 boxtext02 section colm colm6">Personal Accident Cover for Driver: <span id="tpl_pacoverDriverchk"></span></div>
				<div class="qp_box06_inner061 boxtext02 section colm colm5">Accident Cover for Passangers: <span id="tpl_pacoverPassangerchk"></span> Passangers</div>			
				</div>
				</div>
			</div>
			</div>
			
			<?php //} ?>

<?php $label_array = array(
"Price",
"Company Name",	
"Class",	
"Discounts",
"Empty",	
"Excluded Vehicles List",	
"Vehicle Category",	
"Min. Value",
"Max. Value",
"Rate",
"Minimum",
"No Claim Certificate", 	
"Self Declaration",	
"Repair Condition",	
"Personal Accident Cover for Driver",	
"Accident Cover for Passengers",	
"Rant a Car",	
"Own Damage Cover",	
"Out Side UAE Coverage", 
"IF UAE License Age less than 1 Year",
"iF Age less than 25 year",	
"Road-side Assistance",	
"Third Party Limit",	
"Windscreen Damage",	
"Dent Repair",
"Off-Road & Desert Recovery",	
"Emergency Medical Expenses", 
"Ambulance Cover",	
"Death or Bodily Injury",	
"Loss of Personal Belongings",	
"Fire and Theft Cover",	
"Valet Parking Theft",	
"Replacment of locks",
"Natural Calamity Cover",	
"Car Registration / Renewal",	
"Modified / Non-GCC Support"	
);?>

			<div class="frm-row" id="icluded">
			<div class="qp_box04 section">
			<div class="frm-row">
			<div class="qp_box04_inner041 boxtext02 included section colm colm6">
			<div class="qp_box04_inner041 boxtext05 section">What's Included</div>
			<?php $all_sel_data = $ins_selected_compny_data_sel?>
			<?php $final_array;
				for($i=0; $i<=35; $i++){								
						$final_array[$i] = array ('name' => $label_array[$i] ,'data' => $all_sel_data[$i]);
				}
				?><ul><?php						 
				foreach($final_array as $traverse){					
					if($traverse['data'] == "TRUE"){
					?><li> <?php 											 
					 echo $traverse['name'];  											 
					?> </li><?php 
					}
				}
				?>
				</ul>
			
			</div>
			<div class="qp_box04_inner041 boxtext02 not_included section colm colm5">
			<div class="qp_box04_inner041 boxtext05 section">What's Not Included</div>
			
			<?php $all_sel_data = $ins_selected_compny_data_sel?>
			<?php $final_array;
				for($i=0; $i<=35; $i++){								
						$final_array[$i] = array ('name' => $label_array[$i] ,'data' => $all_sel_data[$i]);
				}
				?><ul><?php						 
				foreach($final_array as $traverse){
					
					if($traverse['data'] == "FALSE"){
					?><li> <?php 											 
					 echo $traverse['name'];  											 
					?> </li><?php 
					}
				}
			 ?></ul><?php
			?>
			</div>
			</div>
			</div>
			</div>


			<div class="frm-row">
			<div class="qp_box02 boxtext02 section"><input type="checkbox" id="chkout_agrement"> 
			i agree to the website terms and conditions and the insurance terms and conditions</div>
			</div>
			<?php 
			$pdf_companyName = strtolower($ins_compname_sel); 
			//echo $pdf_companyName;
			if($pdf_companyName == "qatar general insurance"){
				$pdf_fileLink = 'http://insurancetopup.com/xio_outww2/Policy_PDF/Qatar_General_Insurance_Policy.pdf';
			}
			else if($pdf_companyName == "watania insurance"){
				$pdf_fileLink = 'http://insurancetopup.com/xio_outww2/Policy_PDF/Watania_Insurance_Policy.pdf';
			}
			else if($pdf_companyName == "watania"){
				$pdf_fileLink = 'http://insurancetopup.com/xio_outww2/Policy_PDF/Watania_Insurance_Policy.pdf';
			}
			else if($pdf_companyName == "insurance house"){
				$pdf_fileLink = 'http://insurancetopup.com/xio_outww2/Policy_PDF/Insurance_House_Policy.pdf';
			}
			else if($pdf_companyName == "adamjee insurance"){
				$pdf_fileLink = 'http://insurancetopup.com/xio_outww2/Policy_PDF/Adamjee_Insurance_Policy.pdf';
			}
			else if($pdf_companyName == "the oriental insurance"){
				$pdf_fileLink = 'http://insurancetopup.com/xio_outww2/Policy_PDF/The_Oriental_Insurance_Policy.pdf';
			}
			else if($pdf_companyName == "tokio marine"){
				$pdf_fileLink = 'http://insurancetopup.com/xio_outww2/Policy_PDF/Tokio_Marine_Policy.pdf';
			}
			else if($pdf_companyName == "dubai national insurance"){
				$pdf_fileLink = 'http://insurancetopup.com/xio_outww2/Policy_PDF/Dubai_National_Insurance_Policy.pdf';
			}			
			?>
						
			<div class="frm-row">
			<div class="qp_box02 boxtext02 section">View Policy Dicuments <a href="<?php echo $pdf_fileLink;?>">Link to selected Company.</a></div>
			</div>

			<div class="frm-row">
			<div class="qp_box02 boxtext06 section">
			<div class="boxtext01 section">Disclaimer</div>
			1. NON DISCLOSURE OR MISREPRESENTATION:<br>
			You must tell us immediately if any of the information stated in your quotation is incorrect. Failure to do may invalidate your policy.  Because no list of questions can be exhaustive, please consider carefully whether there is any other material information known to you which could influence our acceptance and assessment of the risk. Material information would include any special feature of the car, use, driver’s history, or its location which makes losses or accidents more likely to happen, or more serious if they do.<br><br>
			2. ADEQUACY OF VALUE OF THE VEHICLE:<br>
			Please check whether the value indicated represents the correct market value of your vehicle. Proof may be requested. Rule of thumb is that the car depreciates 15% in value each year after purchase.
			</div>
			</div>


			</div></div><!-- end colm7 section -->
			<!-- main Content Left -->                     
			
			<!-- Sidebar right -->
			<div class="section colm colm4 rightsidebar">
				<h3>Secure Checkout</h3>
				<p>Payment Amount</p>
				<h2><?php 
					setlocale(LC_MONETARY, 'ar_AE');
					//setlocale(LC_ALL,NULL);
					echo money_format('%i', $ins_price_sel);
					//echo $ins_price_sel;?> </h2>
				
				<div class="base_price_block colm colm6">								
				<p>Base Price:</p>
				<p>Voucher Code:</p>
				<p>Discount Applied:</p>				
				</div>
				<div class="base_price_block colm colm6">								
				<p><?php echo $ins_price_sel;?></p>
				<p><?php echo "Ramdan Extended";?></p>
				<p><?php echo $ins_discount_sel;?></p>						
				</div>
			
			<?php 
			wc_get_template( 'checkout/review-order.php', array( 'checkout' => WC()->checkout() ) ); 
			wc_get_template( 'checkout/payment.php', array( 'checkout2' => WC()->checkout() ) ); ?>
			
			</div>
			<!-- end Sidebar right -->
		
			</div>
		
			</div>
		</form>

		</div>
		
		
		</div>
		
	<?php
}

add_action( 'wp_ajax_my_ajax_rt', 'my_ajax_rt' );
add_action( 'wp_ajax_nopriv_my_ajax_rt', 'my_ajax_rt' );

add_filter('woocommerce_thankyou_order_received_text', 'woo_change_order_received_text', 10, 2 );


function woo_change_order_received_text( $str, $order ) {

	//Get data from PHP Sessions
	session_start();  	

	$ins_price_sel = $_SESSION["ins_price"];
	$ins_compname_sel = $_SESSION["ins_compname"];
	$ins_gcc_spec_sel = $_SESSION["ins_gcc_spec"];
	$ins_vehicle_cat_sel = $_SESSION["ins_vehicle_cat"];
	$ins_repair_cond_sel = $_SESSION["ins_repair_cond"];
	$ins_minimum_val_sel = $_SESSION["ins_minimum_val"];
	$ins_maximum_val_sel = $_SESSION["ins_maximum_val"];
	$ins_rate_sel = $_SESSION["ins_rate"];
	$ins_minimum_sel = $_SESSION["ins_minimum"];
	$ins_dl_less_than_year_sel = $_SESSION["ins_dl_less_than_year"];
	$ins_less_than_year_sel = $_SESSION["ins_less_than_year"];
	$ins_os_coverage_sel = $_SESSION["ins_os_coverage"];
	$ins_pa_driver_sel = $_SESSION["ins_pa_driver"];
	$ins_papp_sel = $_SESSION["ins_papp"];
	$ins_rs_assistance_sel = $_SESSION["ins_rs_assistance"];
	$ins_rent_car_sel = $_SESSION["ins_rent_car"];
	$ins_excess_amount_sel = $_SESSION["ins_excess_amount"];



	//User Data

	$ins_reg_date_aj = $_SESSION["ins_reg_date_aj"];
	$ins_polstart_date_aj = $_SESSION["ins_polstart_date_aj"];
	$ins_email_address_aj = $_SESSION["ins_email_address_aj"];
	$ins_firstName_aj = $_SESSION["ins_firstName_aj"];
	$ins_lastname_aj = $_SESSION["ins_lastname_aj"];
	$ins_dob_aj = $_SESSION["ins_dob_aj"];
	$ins_nationality_aj_show = $_SESSION["ins_nationality_aj"];
	$ins_contact_num_aj = $_SESSION["ins_contact_num_aj"];
	$ins_country_firstdl_aj = $_SESSION["ins_country_firstdl_aj"];
	$ins_long_uaedrivinglicence_aj = $_SESSION["ins_long_uaedrivinglicence_aj"];
	$ins_years_dltotal_aj = $_SESSION["ins_years_dltotal_aj"];
	$ins_imported_aj = $_SESSION["ins_imported_aj"];

	

	ob_start();

	?>

	<div id="my_custom_checkout_field">
		<div class="hidethisall" style="display:none;">


		<h3><?php _e('Company Details', 'stern_taxi_fare' ); ?></h3>

		<br>

		<p class="form-row form-row form-row-wide" >
			<label><?php _e( 'Company Name', 'stern_taxi_fare' ); ?></label>
			<input name="dateEstimatedPickedUp" type="text" class="input-text" readonly value="<?php echo $ins_compname_sel; ?>">			
		</p>

		<p class="form-row form-row form-row-wide" >
			<label><?php _e( 'Price', 'stern_taxi_fare' ); ?></label>
			<input name="dateEstimatedPickedUp" type="text" class="input-text" readonly value="<?php echo $ins_price_sel; ?>">			
		</p>

		<p class="form-row form-row form-row-wide" >
			<label><?php _e( 'Support Modified', 'stern_taxi_fare' ); ?></label>
			<input name="dateEstimatedPickedUp" type="text" class="input-text" readonly value="<?php echo $ins_gcc_spec_sel; ?>">			
		</p>

		<p class="form-row form-row form-row-wide" >
			<label><?php _e( 'Vehicle Category', 'stern_taxi_fare' ); ?></label>
			<input name="dateEstimatedPickedUp" type="text" class="input-text" readonly value="<?php echo $ins_vehicle_cat_sel; ?>">
		</p>

		<p class="form-row form-row form-row-wide" >
			<label><?php _e( 'Repair Condition', 'stern_taxi_fare' ); ?></label>
			<input name="dateEstimatedPickedUp" type="text" class="input-text" readonly value="<?php echo $ins_repair_cond_sel; ?>">
		</p>

		<p class="form-row form-row form-row-wide" >
			<label><?php _e( 'Driving license less than 1 Year', 'stern_taxi_fare' ); ?></label>
			<input name="dateEstimatedPickedUp" type="text" class="input-text" readonly value="<?php echo $ins_dl_less_than_year_sel; ?>">
		</p>

		<p class="form-row form-row form-row-wide" >
			<label><?php _e( 'Age less than 25 year', 'stern_taxi_fare' ); ?></label>
			<input name="dateEstimatedPickedUp" type="text" class="input-text" readonly value="<?php echo $ins_less_than_year_sel; ?>">
		</p>

		<p class="form-row form-row form-row-wide" >
			<label><?php _e( 'Out Side UAE Coverage', 'stern_taxi_fare' ); ?></label>
			<input name="dateEstimatedPickedUp" type="text" class="input-text" readonly value="<?php echo $ins_os_coverage_sel; ?>">
		</p>



		<p class="form-row form-row form-row-wide" >

			<label><?php _e( 'Personal Accident (Driver)', 'stern_taxi_fare' ); ?></label>

			<input name="dateEstimatedPickedUp" type="text" class="input-text" readonly value="<?php echo $ins_pa_driver_sel; ?>">			

		</p>



		<p class="form-row form-row form-row-wide" >

			<label><?php _e( 'Personal Accident Per Passengers', 'stern_taxi_fare' ); ?></label>

			<input name="dateEstimatedPickedUp" type="text" class="input-text" readonly value="<?php echo $ins_papp_sel; ?>">			

		</p>



		<p class="form-row form-row form-row-wide" >

			<label><?php _e( 'Road Side Assistance', 'stern_taxi_fare' ); ?></label>

			<input name="dateEstimatedPickedUp" type="text" class="input-text" readonly value="<?php echo $ins_rs_assistance_sel; ?>">			

		</p>



		<p class="form-row form-row form-row-wide" >

			<label><?php _e( 'Rant a Car', 'stern_taxi_fare' ); ?></label>

			<input name="dateEstimatedPickedUp" type="text" class="input-text" readonly value="<?php echo $ins_rent_car_sel; ?>">			

		</p>



		<p class="form-row form-row form-row-wide" >

			<label><?php _e( 'Excess Amount', 'stern_taxi_fare' ); ?></label>

			<input name="dateEstimatedPickedUp" type="text" class="input-text" readonly value="<?php echo $ins_excess_amount_sel; ?>">			

		</p>



		<h3><?php _e('User Details', 'stern_taxi_fare' ); ?></h3>

		<br>

		

		<p class="form-row form-row form-row-wide" >

			<label><?php _e( 'Registration Date', 'stern_taxi_fare' ); ?></label>

			<input name="ins_regdate" type="text" class="input-text" readonly value="<?php echo $ins_reg_date_aj; ?>">			

		</p>

		

		<p class="form-row form-row form-row-wide" >

			<label><?php _e( 'Policy Start Date', 'stern_taxi_fare' ); ?></label>

			<input name="ins_polstartdate" type="text" class="input-text" readonly value="<?php echo $ins_polstart_date_aj; ?>">			

		</p>

		

		<p class="form-row form-row form-row-wide" >

			<label><?php _e( 'Email Address', 'stern_taxi_fare' ); ?></label>

			<input name="ins_email" type="text" class="input-text" readonly value="<?php echo $ins_email_address_aj; ?>">			

		</p>

		

		<p class="form-row form-row form-row-wide" >

			<label><?php _e( 'First name', 'stern_taxi_fare' ); ?></label>

			<input name="ins_firstname" type="text" class="input-text" readonly value="<?php echo $ins_firstName_aj; ?>">			

		</p>



		<p class="form-row form-row form-row-wide" >

			<label><?php _e( 'Last name', 'stern_taxi_fare' ); ?></label>

			<input name="ins_lastname" type="text" class="input-text" readonly value="<?php echo $ins_lastname_aj; ?>">			

		</p>

		<p class="form-row form-row form-row-wide" >
			<label><?php _e( 'Date of Birth', 'stern_taxi_fare' ); ?></label>
			<input name="ins_dob" type="text" class="input-text" readonly value="<?php echo $ins_dob_aj; ?>">			
		</p>

		<p class="form-row form-row form-row-wide" >
			<label><?php _e( 'Nationality', 'stern_taxi_fare' ); ?></label>
			<input name="ins_nationality" type="text" class="input-text" readonly value="<?php echo $ins_nationality_aj; ?>">			
		</p>

		<p class="form-row form-row form-row-wide" >
			<label><?php _e( 'Contact number', 'stern_taxi_fare' ); ?></label>
			<input name="ins_contact_num" type="text" class="input-text" readonly value="<?php echo $ins_contact_num_aj; ?>">			
		</p>

		<p class="form-row form-row form-row-wide" >
			<label><?php _e( 'Country of Your First Driving Licence', 'stern_taxi_fare' ); ?></label>
			<input name="ins_countryfdl" type="text" class="input-text" readonly value="<?php echo $ins_country_firstdl_aj; ?>">			
		</p>

		<p class="form-row form-row form-row-wide" >
			<label><?php _e( 'Year Driving in UAE', 'stern_taxi_fare' ); ?></label>
			<input name="ins_years_druae" type="text" class="input-text" readonly value="<?php echo $ins_long_uaedrivinglicence_aj; ?>">
		</p>

		<p class="form-row form-row form-row-wide" >
			<label><?php _e( 'Year Driving in Total', 'stern_taxi_fare' ); ?></label>
			<input name="ins_years_dltotal" type="text" class="input-text" readonly value="<?php echo $ins_years_dltotal_aj; ?>">			
		</p>

		<p class="form-row form-row form-row-wide" >
			<label><?php _e( 'My Car Imported', 'stern_taxi_fare' ); ?></label>
		    <input name="ins_imported" type="text" class="input-text" readonly value="<?php echo $ins_imported_aj; ?>">			
		</p>
		
		</div>
		
		<div class="container">
		  <h2>Thank you for ordering at Insurance Topup</h2>
		  <div class="panel panel-default">
			<div class="panel-body">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</div>
		  </div>
		</div>
	
	</div>

	<?php
	$result = ob_get_clean();
	return $result;	
}

function my_ajax_rt() {
	$front_end_check = 'yes';
	$ins_price = $_REQUEST['ins_price'];
	$ins_compname = $_REQUEST['ins_compname'];
	$ins_gcc_spec = $_REQUEST['ins_gcc_spec'];
	$ins_vehicle_cat = $_REQUEST['ins_vehicle_cat'];
	$ins_repair_cond = $_REQUEST['ins_repair_cond'];
	$ins_minimum_val = $_REQUEST['ins_minimum_val'];
	$ins_maximum_val = $_REQUEST['ins_maximum_val'];
	$ins_rate = $_REQUEST['ins_rate'];
	$ins_minimum = $_REQUEST['ins_minimum'];
	$ins_dl_less_than_year = $_REQUEST['ins_dl_less_than_year'];
	$ins_less_than_year = $_REQUEST['ins_less_than_year'];
	$ins_os_coverage = $_REQUEST['ins_os_coverage'];
	$ins_pa_cover_driver1 = $_REQUEST['ins_pa_cover_driver'];
	$ins_papp = $_REQUEST['ins_papp'];
	$ins_rs_assistance = $_REQUEST['ins_rs_assistance'];
	$ins_rent_car = $_REQUEST['ins_rent_car'];
	$ins_excess_amount = $_REQUEST['ins_excess_amount'];
	$ins_third_prty_limit1 = $_REQUEST['ins_third_prty_limit'];
	$ins_em_med_expense = $_REQUEST['ins_emergency_med_exp'];
	$ins_loss_of_per_belongings_set = $_REQUEST['ins_loss_of_per_belongings'];
	$ins_selected_compny_data = $_REQUEST['ins_selected_compny_data'];

	//User Data
	$ins_fullname = $_REQUEST['ins_fullname'];
	$ins_veh_model_year = $_REQUEST['ins_veh_model_year'];
	$ins_model_detail = $_REQUEST['ins_model_detail'];
	$ins_contact_num = $_REQUEST['ins_contact_num'];
	$ins_veh_man_year = $_REQUEST['ins_veh_man_year'];
	$veh_email_address = $_REQUEST['ins_emailaddress'];
	$veh_modelno = $_REQUEST['ins_vehicle_brand'];
	$veh_reg_dateF = $_REQUEST['ins_vehiclereg_date'];
	$veh_city_ofreg = $_REQUEST['ins_cityof_reg'];
	$ins_car_val = $_REQUEST['ins_car_value'];
	$veh_policy_sdateF = $_REQUEST['ins_policy_startdate'];
	$veh_country_first_dl = $_REQUEST['ins_country_first_dl'];
	$veh_dob_date = $_REQUEST['ins_dob_date'];
	$ins_total_drivexp = $_REQUEST['ins_total_drivexp'];

	//Selected Data
	$veh_brandnewcheck = $_REQUEST['veh_brandnewcheck'];
	$current_ins_exp = $_REQUEST['current_ins_exp'];
	$current_policy_comp_ins = $_REQUEST['current_policy_comp_ins'];
	$cur_policy_agency_repair = $_REQUEST['cur_policy_agency_repair'];
	$vehicle_is_gcc_spec = $_REQUEST['vehicle_is_gcc_spec'];
	$not_for_commercial_purpose = $_REQUEST['not_for_commercial_purpose'];
	$reg_under_myname = $_REQUEST['reg_under_myname'];
	$claim_in_last12months = $_REQUEST['claim_in_last12months'];
	$excess_amountpay = $_REQUEST['excess_amountpay'];
	$need_veh_replacement = $_REQUEST['need_veh_replacement'];
	$need_per_acc_cover = $_REQUEST['need_per_acc_cover'];
	$need_out_uae_cover = $_REQUEST['need_out_uae_cover'];
	$ins_cntry_fir_driv_lic = $_REQUEST['ins_cntry_fir_driv_lic'];
	$ins_nationality_aj1 = $_REQUEST['ins_nationality_sel'];
	$ins_form_type = $_REQUEST['form_type'];
	$tpl_ambulanceCover_sess = $_REQUEST['tpl_ambulanceCover'];
	$tpl_roadSide_assistance_sess = $_REQUEST['tpl_roadSide_assistance'];
	$tpl_third_party_death_sess = $_REQUEST['tpl_third_party_death'];
	$ins_cover_passanger = $_REQUEST['ins_cover_passanger'];
	
	update_option( 'front_end_check', $front_end_check );
	
	update_option( 'ins_fullname', $ins_fullname );
	update_option( 'ins_nationality_aj1', $ins_nationality_aj1 );
	update_option( 'ins_contact_num', $ins_contact_num );
	update_option( 'veh_dob_date', $veh_dob_date );
	update_option( 'ins_total_drivexp', $ins_total_drivexp );
	update_option( 'veh_country_first_dl', $veh_country_first_dl );
	update_option( 'ins_compname', $ins_compname );
	update_option( 'veh_brandnewcheck', $veh_brandnewcheck );
	update_option( 'ins_veh_model_year', $ins_veh_model_year );
	update_option( 'ins_model_detail', $ins_model_detail );
	update_option( 'veh_reg_dateF', $veh_reg_dateF );
	update_option( 'veh_city_ofreg', $veh_city_ofreg );
	update_option( 'ins_car_val', $ins_car_val );
	update_option( 'veh_policy_sdateF', $veh_policy_sdateF );
	update_option( 'current_ins_exp', $current_ins_exp );
	update_option( 'current_policy_comp_ins', $current_policy_comp_ins );
	update_option( 'vehicle_is_gcc_spec', $vehicle_is_gcc_spec );
	update_option( 'not_for_commercial_purpose', $not_for_commercial_purpose );
	update_option( 'ins_rent_car', $ins_rent_car );
	update_option( 'ins_pa_cover_driver1', $ins_pa_cover_driver1 );
	update_option( 'cur_policy_agency_repair', $cur_policy_agency_repair );
	update_option( 'need_out_uae_cover', $need_out_uae_cover );
	update_option( 'ins_pa_cover_pass', $ins_cover_passanger );
	
	
	// Start the session
	session_start();
	$_SESSION["ins_price"] = $ins_price;
	$_SESSION["ins_compname"] = $ins_compname;
	$_SESSION["ins_discount"] = $ins_gcc_spec;
	$_SESSION["ins_vehicle_cat"] = $ins_vehicle_cat;
	$_SESSION["ins_repair_cond"] = $ins_repair_cond;
	$_SESSION["ins_minimum_val"] = $ins_minimum_val;
	$_SESSION["ins_maximum_val"] = $ins_maximum_val;
	$_SESSION["ins_rate"] = $ins_rate;
	$_SESSION["ins_minimum"] = $ins_minimum;
	$_SESSION["ins_dl_less_than_year"] = $ins_dl_less_than_year;
	$_SESSION["ins_less_than_year"] = $ins_less_than_year;
	$_SESSION["ins_os_coverage"] = $ins_os_coverage;
	$_SESSION["ins_pa_cvr_driver"] = $ins_pa_cover_driver1;
	$_SESSION["ins_papp"] = $ins_papp;
	$_SESSION["ins_rs_assistance"] = $ins_rs_assistance;
	$_SESSION["ins_rent_car"] = $ins_rent_car;
	$_SESSION["ins_excess_amount"] = $ins_excess_amount;
	$_SESSION["ins_third_prty_limits"] = $ins_third_prty_limit1;
	$_SESSION["ins_em_med_expense_set"] = $ins_em_med_expense;
	$_SESSION["ins_loss_of_per_belongings_session"] = $ins_loss_of_per_belongings_set;
	$_SESSION["ins_selected_compny_data_ses"] = $ins_selected_compny_data;
	
	//User Data
	$_SESSION["ins_fullname"] = $ins_fullname;
	$_SESSION["ins_model_year"] = $ins_veh_model_year;
	$_SESSION["ins_contact_num"] = $ins_contact_num;
	$_SESSION["ins_veh_man_year"] = $ins_veh_man_year;
	$_SESSION["veh_email_address"] = $veh_email_address;
	$_SESSION["veh_city_registered"] = $veh_city_registered;
	$_SESSION["veh_model_detail"] = $ins_model_detail;
	$_SESSION["veh_reg_dateF"] = $veh_reg_dateF;
	$_SESSION["veh_city_ofreg"] = $veh_city_ofreg;
	$_SESSION["veh_car_val"] = $ins_car_val;
	$_SESSION["veh_policy_sdateF"] = $veh_policy_sdateF;
	$_SESSION["ins_cntry_fir_driv_lic"] = $ins_cntry_fir_driv_lic;
	$_SESSION["ins_dob_date"] = $veh_dob_date;
	$_SESSION["ins_total_drivexp_session"] = $ins_total_drivexp;
	
	//Selected Data
	$_SESSION["veh_brandnewcheck"] = $veh_brandnewcheck;
	$_SESSION["current_ins_exp"] = $current_ins_exp;
	$_SESSION["current_policy_comp_ins"] = $current_policy_comp_ins;
	$_SESSION["cur_policy_agency_repair"] = $cur_policy_agency_repair;
	$_SESSION["vehicle_is_gcc_spec"] = $vehicle_is_gcc_spec;
	$_SESSION["not_for_commercial_purpose"] = $not_for_commercial_purpose;
	$_SESSION["reg_under_myname"] = $reg_under_myname;
	$_SESSION["claim_in_last12months"] = $claim_in_last12months;
	$_SESSION["excess_amountpay"] = $excess_amountpay;
	$_SESSION["need_veh_replacement"] = $need_veh_replacement;
	$_SESSION["need_per_acc_cover"] = $need_per_acc_cover;
	$_SESSION["need_out_uae_cover"] = $need_out_uae_cover;
	$_SESSION["ins_nationality_aj"] = $ins_nationality_aj1;
	$_SESSION["tpl_ambulanceCover_set"] = $tpl_ambulanceCover_sess;
	$_SESSION["tpl_roadSide_assistance_set"] = $tpl_roadSide_assistance_sess;
	$_SESSION["tpl_third_party_death_set"] = $tpl_third_party_death_sess;
	$_SESSION["ins_vehicle_brand"] = $veh_modelno;
	
}

function stern_taxi_fares_script_back_js() {

	// $google_map_api = 'https://maps.google.com/maps/api/js?libraries=places&language='. get_option('stern_taxi_fare_country') . '&key=' .  get_option('stern_taxi_fare_apiGoogleKey');

	// wp_enqueue_script('google-places', $google_map_api);
	// wp_register_script('stern_bootstrapMoment', plugins_url('js/moment-with-locales.js', __FILE__ ),array('jquery'));
	// wp_enqueue_script('stern_bootstrapMoment');
	// wp_register_script('stern_appendGrid', plugins_url('js/jquery.appendGrid-1.6.1.js', __FILE__ ),array('jquery'));
	// wp_enqueue_script('stern_appendGrid');

	// wp_register_script('stern_jquery_ui', plugins_url('js/jquery-ui.js', __FILE__ ),array('jquery'));
	// wp_enqueue_script('stern_jquery_ui');

	// wp_register_script('stern_jquery_fullCalendarJS', plugins_url('js/fullcalendar.min.js', __FILE__ ),array('jquery'));
	// wp_enqueue_script('stern_jquery_fullCalendarJS');
	
	// wp_register_script('stern_taxi_fare_fullcalendar_js', plugins_url('js/stern_taxi_fare_fullcalendar.js', __FILE__ ),array('jquery'));
	// wp_enqueue_script('stern_taxi_fare_fullcalendar_js');
	
	// wp_register_script('stern_taxi_fare_admin_js', plugins_url('js/stern_taxi_fare_admin.js', __FILE__ ),array('jquery'));
	// wp_localize_script('stern_taxi_fare_admin_js', 'ajax_obj_type_car_admin',	array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
	// wp_localize_script('stern_taxi_fare_admin_js', 'ajax_obj_calendar',			array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
	// wp_enqueue_script('stern_taxi_fare_admin_js');
}



function stern_taxi_fares_script_front_js() {
	if( is_page( array( 'auto-insurance-quote', 'tpl-insurance-quote' ) )){
		//wp_register_script('stern_taxi_fare_fullcalendar_front_js', plugins_url('js/stern_taxi_fare_fullcalendar_front.js', __FILE__ ),array('jquery'));

		//wp_enqueue_script('stern_taxi_fare_fullcalendar_front_js');
       // wp_register_script('stern_bootstrapValidatorMin', plugins_url('js/bootstrapValidator.min.js', __FILE__ ),array('jquery'));
        //wp_enqueue_script('stern_bootstrapValidatorMin');

	    //wp_register_script('ins_additionalmethords', plugins_url('js/additional-methods.min.js', __FILE__ ),array('jquery'));
        //wp_enqueue_script('ins_additionalmethords');
		//wp_register_script('ins_jquery19', plugins_url('js/jquery-1.9.1.min.js', __FILE__ ),array('jquery'));
        //wp_enqueue_script('ins_jquery19');
		// wp_register_script('ins_jqueryuicombo', plugins_url('js/jquery-ui-combo.min.js', __FILE__ ),array('jquery'));
        //wp_enqueue_script('ins_jqueryuicombo');
		//wp_register_script('ins_touch', plugins_url('js/jquery-ui-touch-punch.min.js', __FILE__ ),array('jquery'));
       // wp_enqueue_script('ins_touch');

/* 		wp_register_script('ins_slider_pips', plugins_url('js/jquery-ui-slider-pips.min.js', __FILE__ ),array('jquery'));
        wp_enqueue_script('ins_slider_pips');

		wp_register_script('ins_ui_touch', plugins_url('js/jquery-ui-touch-punch.min.js', __FILE__ ),array('jquery'));
        wp_enqueue_script('ins_ui_touch');
 */						
		//wp_register_script('ins_jquerymin', plugins_url('js/jquery.min', __FILE__ ),array('jquery'));
       //wp_enqueue_script('ins_jquerymin');

		/*
        wp_register_script('stern_bootstrapValidatorJS', plugins_url('js/bootstrapValidator.js', __FILE__ ),array('jquery'));
        wp_enqueue_script('stern_bootstrapValidatorJS');
		*/

        //wp_register_script('stern_bootstrapselectMin', plugins_url('js/bootstrap-select.min.js', __FILE__ ),array('jquery'));
       // wp_enqueue_script('stern_bootstrapselectMin');

/*
        wp_register_script('stern_bootstrapselect', plugins_url('js/bootstrap-select.js', __FILE__ ),array('jquery'));
        wp_enqueue_script('stern_bootstrapselect');
*/
		//wp_register_script('stern_bootstrapMoment', plugins_url('js/moment-with-locales.js', __FILE__ ),array('jquery'));
		//wp_enqueue_script('stern_bootstrapMoment');

		//wp_register_script('stern_jquery_ui', plugins_url('js/jquery-ui.js', __FILE__ ),array('jquery'));
		//wp_enqueue_script('stern_jquery_ui');

		//wp_register_script('stern_bootstrapMinjs', plugins_url('js/bootstrap.min.js', __FILE__ ),array('jquery'));
		//wp_enqueue_script('stern_bootstrapMinjs');

		/* if(get_option('stern_taxi_fare_lib_bootstrap_js')=="true"){
			wp_register_script('stern_taxi_fare_lib_bootstrap_js', plugins_url('/bootstrap/js/bootstrap.js', __FILE__ ),array('jquery'));
			wp_enqueue_script('stern_taxi_fare_lib_bootstrap_js');

		} */

		//wp_register_script('stern_bootstrapdatetimepickerMin', plugins_url('js/bootstrap-datetimepicker.min.js', __FILE__ ),array('jquery'));
		//wp_enqueue_script('stern_bootstrapdatetimepickerMin');

		//wp_register_script('stern_bootstrapdatetimepicker', plugins_url('js/bootstrap-datetimepicker.js', __FILE__ ),array('jquery'));
		//wp_enqueue_script('stern_bootstrapdatetimepicker');
		/*
		wp_register_script('stern_bootstrap_datetimepickerFR', plugins_url('js/bootstrap-datetimepicker.fr.js', __FILE__ ),array('jquery'));
		wp_enqueue_script('stern_bootstrap_datetimepickerFR');
  		*/
		//wp_register_script('stern_jquery_fullCalendarJS', plugins_url('js/fullcalendar.min.js', __FILE__ ),array('jquery'));
		//wp_enqueue_script('stern_jquery_fullCalendarJS');

		//	wp_register_script( 'stern-bootstrapwp', plugins_url('/js/bootstrap-wp.js', __FILE__ ),array('jquery'));
		//	wp_enqueue_script( 'stern-bootstrapwp');

		// Working Scripts

		wp_register_script('fabric_js', plugins_url('js/fabric.js', __FILE__ ),array('jquery'));
		wp_enqueue_script('fabric_js');
		
		wp_register_script('html_to_canvas_js', plugins_url('js/html2canvas.js', __FILE__ ),array('jquery'));
		wp_enqueue_script('html_to_canvas_js');

	
        wp_register_script('ins_steps', plugins_url('js/jquery.steps.min.js', __FILE__ ),array('jquery'));
        wp_enqueue_script('ins_steps');

        wp_register_script('ins_jqueryforms', plugins_url('js/jquery.form.min.js', __FILE__ ),array('jquery'));
        wp_enqueue_script('ins_jqueryforms');

        

        wp_register_script('ins_utils', plugins_url('js/utils.js', __FILE__ ),array('jquery'));
        wp_enqueue_script('ins_utils');

       

        wp_register_script('ins_jqueryuicostom', plugins_url('js/jquery-ui-custom.min.js', __FILE__ ),array('jquery'));
        wp_enqueue_script('ins_jqueryuicostom');
        
		wp_register_script('ins_jquerystepper', plugins_url('js/jquery.stepper.min.js', __FILE__ ),array('jquery'));
        wp_enqueue_script('ins_jquerystepper');
		
		wp_register_script('ins_jqueryplaceholder', plugins_url('js/jquery.placeholder.min.js', __FILE__ ),array('jquery'));
        wp_enqueue_script('ins_jqueryplaceholder');
		
		wp_register_script('ins_jqueryvalidate', plugins_url('js/jquery.validate.min.js', __FILE__ ),array('jquery'));
        wp_enqueue_script('ins_jqueryvalidate');

		wp_register_script('ins_formsmodel', plugins_url('js/smartforms-modal.min.js', __FILE__ ),array('jquery'));
        wp_enqueue_script('ins_formsmodel');

		wp_register_script('ins_select2', plugins_url('js/select2.full.min.js', __FILE__ ),array('jquery'));
        wp_enqueue_script('ins_select2');

		
		
		wp_register_script('ins_maskedinput', plugins_url('js/jquery.maskedinput.js', __FILE__ ),array('jquery'));
        wp_enqueue_script('ins_maskedinput');

		wp_register_script('ins_smartforms', plugins_url('js/smart-form.js', __FILE__ ),array('jquery'));
        wp_enqueue_script('ins_smartforms');

		wp_register_script('ins_jqueryautotab', plugins_url('js/jquery.autotab.min.js', __FILE__ ),array('jquery'));
        wp_enqueue_script('ins_jqueryautotab');

		wp_register_script('ins_tellinput', plugins_url('js/intlTelInput.min.js', __FILE__ ),array('jquery'));
        wp_enqueue_script('ins_tellinput');

        //Working scripts Ends.
    }
 
       	//wp_register_script('stern_taxi_fare', plugins_url('js/stern_taxi_fare.js', __FILE__ ),array('jquery'));
		
		wp_localize_script('stern_taxi_fare', 'my_ajax_object',			array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
		wp_localize_script('stern_taxi_fare', 'my_ajax_object_toll',		array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
		wp_localize_script('stern_taxi_fare', 'my_ajax_object_picker',		array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
		wp_localize_script('stern_taxi_fare', 'my_ajax_object_suitcases',	array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
		wp_localize_script('stern_taxi_fare', 'my_ajax_object_carFare',	array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
		wp_localize_script('stern_taxi_fare', 'ajax_obj_calendar',			array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );

		wp_enqueue_script('stern_taxi_fare', plugins_url('js/stern_taxi_fare.js', __FILE__ ) );
 

		add_filter( 'woocommerce_checkout_fields' , 'custom_override_checkout_fields' );
		 
		function custom_override_checkout_fields( $fields ) {
			//unset($fields['billing']['billing_first_name']);
			//unset($fields['billing']['billing_last_name']);
			unset($fields['billing']['billing_company']);
			unset($fields['billing']['billing_address_1']);
			unset($fields['billing']['billing_address_2']);
			//unset($fields['billing']['billing_city']);
			unset($fields['billing']['billing_postcode']);
			unset($fields['billing']['billing_country']);
			unset($fields['billing']['billing_state']);
			//unset($fields['billing']['billing_phone']);
			//unset($fields['order']['order_comments']);
			unset($fields['billing']['billing_address_2']);
			unset($fields['billing']['billing_postcode']);
			unset($fields['billing']['billing_company']);
			//unset($fields['billing']['billing_last_name']);
			//unset($fields['billing']['billing_email']);
			//unset($fields['billing']['billing_city']);
			return $fields;
		}
		
		add_filter( 'woocommerce_currency_symbol', 'wc_change_uae_currency_symbol', 10, 2 );

		function wc_change_uae_currency_symbol( $currency_symbol, $currency ) {
			switch ( $currency ) {
				case 'AED':
					$currency_symbol = 'AED';
				break;
			}

			return $currency_symbol;
		}
	    // load bootstrap jquery
		//    wp_enqueue_script( 'stern-jquery', plugins_url('/js/jquery.min.js', __FILE__ ),array('jquery'));
	//}
}
add_filter( 'woocommerce_countries_inc_tax_or_vat', function () {
  return __( 'GST', 'woocommerce' );
});

 
/*  function myplugin_load_textdomain() {
  	$locale = apply_filters( 'plugin_locale', get_locale(), 'stern-taxi_fare' );
	$dir    = trailingslashit( WP_LANG_DIR );
	load_textdomain( 'stern-taxi-fare', $dir . 'stern-taxi_fares/stern_taxi_fare-' . $locale . '.mo' );
	load_plugin_textdomain( 'stern_taxi_fare', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );

} */

